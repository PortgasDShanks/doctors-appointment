<?php
// 0 = Pending
// 1 = Cancel
// 2 = Approve
// 3 = Finish
// 4 = Reschedule
include 'core/config.php';
$user_id = $_SESSION['user_id'];
$accessType = $_SESSION['user_access'];
$display  = ($accessType == 'admin')?"display:none":"";
$users = SELECT_DATA("*","tbl_users","user_id = '$user_id'");

checkSession($user_id);

$access = (isset($_GET['access']) && $_GET['access'] !='') ? $_GET['access'] : '';
$view = (isset($_GET['view']) && $_GET['view'] !='') ? $_GET['view'] : '';
$sub = (isset($_GET['sub']) && $_GET['sub'] !='') ? $_GET['sub'] : '';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>D A M S</title>
  <link rel="icon" type="image/png" href="assets/images/icon.png">
  <!-- All CSS -->
  <link rel="stylesheet" href="assets/css/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/css/adminlte.min.css">
  <link rel="stylesheet" href="assets/css/ionicons.min.css">
  <link rel="stylesheet" type="text/css" href="assets/css/myStyle.css">
  <link rel="stylesheet" href="assets/css/dataTables.bootstrap4.css">
  <link href='assets/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
  <link href='assets/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-datepicker.css">
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-timepicker.min.css">
  <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css">
  <link rel="stylesheet" type="text/css" href="assets/css/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="assets/css/daterangepicker-bs3.css">
  <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
  <link rel="stylesheet" type="text/css" href="assets/css/fastselect.css">
  <link rel="stylesheet" type="text/css" href="assets/css/fSelect.css">
  <link rel="stylesheet" type="text/css" href="assets/css/msg_styles.css">
  <link rel="stylesheet" type="text/css" href="assets/css/select2.min.css">
  <!-- <link rel="stylesheet" type="text/css" href="assets/css/timepicker.min.css"> -->
  <!-- <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"> -->
  <!-- All  JS -->
  <script src="assets/login_assets/js/jquery-3.3.1.min.js"></script>
  <script src="assets/js/bootstrap.bundle.min.js"></script>
  <script src="assets/js/adminlte.min.js"></script>
  <script src="assets/js/sweetalert.min.js"></script>
  <script src="assets/js/jquery.dataTables.js"></script>
  <script src="assets/js/dataTables.bootstrap4.js"></script>
  <script src='assets/fullcalendar/lib/moment.min.js'></script>
  <script src="assets/fullcalendar/fullcalendar.min.js"></script>
  <script type="text/javascript" src="assets/js/datepicker.js"></script>
  <script type="text/javascript" src="assets/js/bootstrap-timepicker.min.js"></script>
  <script type="text/javascript" src="assets/js/sweetalert.min.js"></script>
  <script type="text/javascript" src="assets/js/jquery-ui.js"></script>
  <script type="text/javascript" src="assets/js/daterangepicker.js"></script>
  <script type="text/javascript" src="assets/js/jquery.knob.js"></script>
  <!-- <script type="text/javascript" src="assets/js/fastselect.min.js"></script> -->
  <script type="text/javascript" src="assets/js/fastselect.standalone.min.js"></script>
  <script type="text/javascript" src="assets/js/fSelect.js"></script>
  <script type="text/javascript" src="assets/js/select2.full.min.js"></script>
  <!-- <script type="text/javascript" src="assets/js/timepicker.min.js"></script> -->
</head>
<style>
.select2{
  width: 80%;
}
</style>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <?php include 'core/header.php';?>
 

  <!-- Main Sidebar Container -->
  <?php include 'core/sidebar.php'; ?>

  <!-- Routes -->
  <div class="content-wrapper">
    <?php
    include 'routes/routes.php';
    ?>
  </div>
 

  <!-- Control Sidebar -->
  <?php include 'core/settings.php'; ?>
  

  <!-- Footer -->
  <?php include 'core/footer.php'; ?>
</div>


</body>
<script type="text/javascript">
  function gotoCLinic(){
  }
  function logout(){
    window.location.href = 'ajax/logout.php';
  }
  function userProfile(){
    window.location = 'index.php?access=profile';
  }
  function time_table(){
    window.location = 'index.php?page=time-table';
  }
  function announcement(){
    window.location = 'index.php?page=announcements'
  }
  function add_user(){
    window.location = 'index.php?page=secretary';
  }
  function add_member(){
    window.location = 'index.php?page=add-member';
  }
  function updateAlert(){
    swal({
        title: "All Good!",
        text: "Your data was successfully updated",
        type: "success"
      }, function(){
        location.reload();
      });
  }
  function deleteAlert(){
    swal({
        title: "All Good!",
        text: "Your data was successfully deleted.",
        type: "success"
      }, function(){
        location.reload();
      });
  }
  function insertAlert(){
    swal({
        title: "All Good!",
        text: "Your data was successfully inserted.",
        type: "success"
      }, function(){
        location.reload();
      });
  }
  function failedAlert(){
    swal({
        title: "Aw Snap!",
        text: "Unable to execute the query.",
        type: "error"
      }, function(){
        location.reload();
      });
  }
  function alertWarning(){
    swal("Aw Snap!", "There's something missing in your data.","warning");
  }
  $(document).ready( function(){
    $('[data-toggle="tooltip"]').tooltip();
    $("#datepicker_1").datepicker({
      minDate: 0
    });
    $('#daterange').daterangepicker();
    $('#multipleSelect').fastselect();

    $("#icon_hover").hover( function(){
      $("#icon_hover").removeClass("fa fa-star");
      $("#icon_hover").addClass("fa fa-star-o");
    }, function(){
      $("#icon_hover").removeClass("fa fa-star-o");
      $("#icon_hover").addClass("fa fa-star");
    }); 

    $("#users").fSelect({
      width: '100%',
      placeholder: 'Please Select Users'
    });

    $(".searchDoc").fSelect({
      width: '100%'
    });
  });

</script>
</html>
