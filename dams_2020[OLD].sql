-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 06, 2020 at 09:52 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dams_2020`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_appointments`
--

CREATE TABLE `tbl_appointments` (
  `appointment_id` int(11) UNSIGNED NOT NULL,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `appointment_date` date NOT NULL,
  `reschedule_date` date NOT NULL,
  `appointment_time_id` int(11) NOT NULL,
  `queu_no` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `diagnosis` text NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_appointments`
--

INSERT INTO `tbl_appointments` (`appointment_id`, `patient_id`, `doctor_id`, `appointment_date`, `reschedule_date`, `appointment_time_id`, `queu_no`, `status`, `diagnosis`, `remarks`) VALUES
(3, 24, 21, '2020-05-13', '0000-00-00', 6, 0, 3, 'asdasdasdas', 'dasdasdasdas'),
(5, 5, 21, '2020-05-13', '0000-00-00', 5, 0, 3, 'Wala na chansa mag ayo', 'Pati joke lng'),
(6, 23, 21, '2020-05-15', '0000-00-00', 4, 0, 1, '', ''),
(7, 25, 21, '2020-05-14', '0000-00-00', 4, 0, 3, 'Wala bulong', 'inum lng damu tubig'),
(8, 6, 21, '2020-05-18', '2020-05-19', 4, 0, 4, '', ''),
(9, 5, 8, '2020-05-27', '0000-00-00', 8, 0, 3, 'Test', 'Test'),
(10, 23, 8, '2020-05-28', '2020-05-29', 8, 0, 1, '', ''),
(11, 23, 8, '2020-05-27', '0000-00-00', 9, 0, 3, 'Test Test Test Test Test', 'TEstTest Test Test Test Test Test'),
(12, 6, 8, '2020-05-27', '0000-00-00', 10, 0, 3, 'test', 'test'),
(13, 25, 21, '2020-06-17', '0000-00-00', 4, 0, 3, 'look into my eyes and you will see', 'look into my eyes and you will see'),
(14, 23, 21, '2020-06-17', '0000-00-00', 5, 0, 3, 'TestTestTestTestTestTestTestTestTest', 'TestTestTestTestTestTestTestTestTest'),
(15, 5, 21, '2020-06-17', '0000-00-00', 6, 0, 3, 'TestTestTestTestTestTestTestTestTest TestTestTestTestTestTestTestTestTest TestTestTestTestTestTestTestTestTest', 'TestTestTestTestTestTestTestTestTest TestTestTestTestTestTestTestTestTest TestTestTestTestTestTestTestTestTest');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blood_donors`
--

CREATE TABLE `tbl_blood_donors` (
  `blood_donor_id` int(11) UNSIGNED NOT NULL,
  `blood_donor_name` varchar(100) NOT NULL DEFAULT '',
  `blood_donor_address` varchar(100) NOT NULL DEFAULT '',
  `blood_donor_contact` int(11) NOT NULL,
  `blood_type` varchar(10) NOT NULL DEFAULT '',
  `date_added` datetime NOT NULL,
  `quantity_bags` decimal(12,3) NOT NULL,
  `quantity_used` int(11) NOT NULL,
  `is_expired` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blood_donors`
--

INSERT INTO `tbl_blood_donors` (`blood_donor_id`, `blood_donor_name`, `blood_donor_address`, `blood_donor_contact`, `blood_type`, `date_added`, `quantity_bags`, `quantity_used`, `is_expired`) VALUES
(1, 'Abel Bayon', 'BAcolod City', 2147483647, '7', '2020-05-26 11:16:21', '2.000', 2, 0),
(2, 'Abel Bayon', 'Bacolod City', 2147483647, '7', '2020-05-26 11:19:52', '1.000', 1, 0),
(3, 'Jochelle Bravo', 'Bacolod City', 2147483647, '8', '2020-05-26 11:20:13', '3.000', 1, 0),
(4, 'Jochelle Bravo', 'Bacolod City', 2147483647, '8', '2020-05-26 11:20:36', '4.000', 2, 0),
(5, 'Mina Myoui', 'Seoul', 2147483647, '3', '2020-05-26 11:58:02', '2.000', 0, 0),
(6, 'Mina Myoui', 'Seoul', 2147483647, '3', '2020-05-26 11:58:16', '3.000', 3, 0),
(7, 'Abel Bayon', 'BAcolod City', 2147483647, '7', '2020-04-13 14:38:18', '5.000', 0, 0),
(8, 'Mina Myoui', 'Seoul', 2147483647, '3', '2020-06-17 13:53:12', '5.000', 0, 0),
(9, 'Mina Myoui', 'Seoul , South Korea', 1234567889, '7', '2020-09-01 08:48:12', '10.000', 0, 0),
(10, 'Abel Bayon', 'BAcolod City', 2147483647, '7', '2020-09-06 15:51:14', '10.000', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blood_types`
--

CREATE TABLE `tbl_blood_types` (
  `blood_type_id` int(11) UNSIGNED NOT NULL,
  `type_name` varchar(10) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blood_types`
--

INSERT INTO `tbl_blood_types` (`blood_type_id`, `type_name`) VALUES
(1, 'O +'),
(2, 'O -'),
(3, 'A +'),
(4, 'A -'),
(5, 'B +'),
(6, 'B -'),
(7, 'AB +'),
(8, 'AB -');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_categories`
--

CREATE TABLE `tbl_categories` (
  `category_id` int(11) UNSIGNED NOT NULL,
  `category_name` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_categories`
--

INSERT INTO `tbl_categories` (`category_id`, `category_name`) VALUES
(4, 'Corona Virus Treatment'),
(6, 'Surgical Surgery'),
(7, 'Psychiatrist'),
(8, 'Internal Medicine Physician');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_doctor_announcements`
--

CREATE TABLE `tbl_doctor_announcements` (
  `da_id` int(11) UNSIGNED NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `da_date` date NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_doctor_announcements`
--

INSERT INTO `tbl_doctor_announcements` (`da_id`, `doctor_id`, `da_date`, `remarks`) VALUES
(4, 21, '2020-05-07', 'May kadtuan ko . so wla danay konsulta'),
(5, 22, '2020-05-19', 'may panghatag bugas');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_doctor_time_table`
--

CREATE TABLE `tbl_doctor_time_table` (
  `time_id` int(11) UNSIGNED NOT NULL,
  `time_from` time NOT NULL,
  `time_to` time NOT NULL,
  `doctor_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_doctor_time_table`
--

INSERT INTO `tbl_doctor_time_table` (`time_id`, `time_from`, `time_to`, `doctor_id`) VALUES
(4, '08:10:00', '09:00:00', 21),
(5, '09:00:00', '10:00:00', 21),
(6, '10:00:00', '11:00:00', 21),
(7, '11:00:00', '12:00:00', 21),
(8, '08:00:00', '09:00:00', 8),
(9, '09:00:00', '10:00:00', 8),
(10, '13:00:00', '14:00:00', 8),
(11, '14:00:00', '15:00:00', 8);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_events`
--

CREATE TABLE `tbl_events` (
  `event_id` int(11) UNSIGNED NOT NULL,
  `event_name` text NOT NULL,
  `event_start_date` date NOT NULL,
  `event_end_date` date NOT NULL,
  `event_time` time NOT NULL,
  `event_added_by` int(11) NOT NULL,
  `date_added` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_events`
--

INSERT INTO `tbl_events` (`event_id`, `event_name`, `event_start_date`, `event_end_date`, `event_time`, `event_added_by`, `date_added`) VALUES
(2, 'Malakat sa america mabulig ubra vaccine para sa COVID-19', '2020-06-01', '2020-06-14', '17:00:00', 21, '2020-05-16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_followup_appointments`
--

CREATE TABLE `tbl_followup_appointments` (
  `fu_appointment_id` int(11) UNSIGNED NOT NULL,
  `appointment_main_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `appointment_date` date NOT NULL,
  `reschedule_date` date NOT NULL,
  `appointment_time_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `diagnosis` text NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_followup_appointments`
--

INSERT INTO `tbl_followup_appointments` (`fu_appointment_id`, `appointment_main_id`, `patient_id`, `doctor_id`, `appointment_date`, `reschedule_date`, `appointment_time_id`, `status`, `diagnosis`, `remarks`) VALUES
(1, 3, 24, 21, '2020-05-16', '0000-00-00', 4, 3, '', ''),
(2, 5, 5, 21, '2020-05-18', '0000-00-00', 6, 2, '', ''),
(3, 7, 25, 21, '2020-05-15', '0000-00-00', 5, 3, 'dasdas das das', 'd asd asd asds adsa'),
(5, 7, 25, 21, '2020-05-16', '0000-00-00', 5, 2, '', ''),
(6, 9, 5, 8, '2020-05-29', '0000-00-00', 9, 2, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_messages`
--

CREATE TABLE `tbl_messages` (
  `message_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message_content` text NOT NULL,
  `message_datetime` datetime NOT NULL,
  `message_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_messages`
--

INSERT INTO `tbl_messages` (`message_id`, `sender_id`, `receiver_id`, `message_content`, `message_datetime`, `message_status`) VALUES
(1, 23, 8, 'test', '2020-09-07 11:42:51', 0),
(2, 23, 21, 'test', '2020-09-07 11:47:26', 1),
(3, 23, 21, 'awts', '2020-09-07 11:47:31', 1),
(4, 21, 23, 'dasdas', '2020-09-07 11:47:47', 1),
(5, 21, 23, 'qweqweqweqw', '2020-09-07 11:47:56', 0),
(6, 21, 23, 'asdasdasdsa', '2020-09-07 11:48:00', 0),
(7, 21, 23, 'dasdasdasdsad', '2020-09-07 12:00:22', 0),
(8, 21, 23, 'dasdasasdas', '2020-09-07 12:01:50', 0),
(9, 23, 21, 'asdasdasdas', '2020-09-07 12:09:18', 0),
(10, 23, 21, 'test scroll', '2020-09-07 12:09:54', 0),
(11, 23, 21, 'asdasdasdas', '2020-09-07 12:10:31', 0),
(12, 23, 21, 'wqeqwe  dfdas cc dasc', '2020-09-07 12:11:09', 0),
(13, 23, 8, 'dasdasd', '2020-09-07 12:11:27', 0),
(14, 23, 8, 'asdasdasd', '2020-09-07 12:11:29', 0),
(15, 23, 8, 'asdasdasd', '2020-09-07 12:11:30', 0),
(16, 23, 8, 'asdasdas', '2020-09-07 12:11:32', 0),
(17, 23, 8, 'asdasdas', '2020-09-07 12:11:34', 0),
(18, 23, 8, 'asdasdas', '2020-09-07 12:11:36', 0),
(19, 23, 8, 'asdasdas', '2020-09-07 12:11:37', 0),
(20, 8, 23, 'luh parang tanga', '2020-09-07 12:12:32', 1),
(21, 23, 8, 'asdad', '2020-09-07 12:13:09', 0),
(22, 23, 8, 'asdasdsa', '2020-09-07 12:13:11', 0),
(23, 8, 6, 'asdasdasdas', '2020-09-07 12:17:25', 0),
(24, 23, 21, 'asdasdas', '2020-09-07 12:17:52', 0),
(25, 23, 21, 'asdasdas', '2020-09-07 12:27:23', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification`
--

CREATE TABLE `tbl_notification` (
  `notification_id` int(11) UNSIGNED NOT NULL,
  `notification_date` datetime NOT NULL,
  `notification_type` int(1) NOT NULL COMMENT '0=notif , 1=message',
  `notification_is_for` int(1) NOT NULL COMMENT '0=patient , 1=doctor/secretary',
  `user_id` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `notification_receiver` int(11) NOT NULL,
  `module` varchar(20) NOT NULL DEFAULT '',
  `action` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification`
--

INSERT INTO `tbl_notification` (`notification_id`, `notification_date`, `notification_type`, `notification_is_for`, `user_id`, `status`, `notification_receiver`, `module`, `action`) VALUES
(1, '2020-05-11 11:08:38', 0, 1, 5, 0, 21, 'PA', 'Set New Appointment'),
(2, '2020-05-11 11:09:15', 0, 1, 23, 0, 21, 'PA', 'Set New Appointment asd asdas ds das das dasd asd asd adss das da sdas das das da adas das dasd'),
(3, '2020-05-11 11:09:42', 0, 1, 24, 1, 21, 'PA', 'Set New Appointment asd asdas ds das das dasd asd asd adss das da sdas das das da adas das dasd'),
(4, '2020-05-13 11:23:05', 0, 1, 5, 1, 21, 'PA', 'Set New Appointment'),
(5, '2020-05-13 11:26:48', 0, 1, 23, 1, 21, 'PA', 'Set New Appointment asd asdas ds das das dasd asd asd adss das da sdas das das da adas das dasd'),
(6, '2020-05-13 11:32:20', 0, 0, 0, 0, 0, 'CA', 'Your appointment was cancelled'),
(7, '2020-05-13 11:38:35', 0, 0, 0, 0, 0, 'CA', 'Your appointment was cancelled'),
(8, '2020-05-13 11:39:38', 0, 0, 0, 0, 0, 'CA', 'Your appointment was cancelled'),
(9, '2020-05-13 11:40:20', 0, 0, 0, 0, 0, 'CA', 'Your appointment was cancelled'),
(10, '2020-05-13 11:53:36', 0, 0, 21, 1, 23, 'AA', 'Your appointment was approved'),
(11, '2020-05-13 11:54:34', 0, 0, 21, 1, 23, 'AA', 'Your appointment was approved'),
(12, '2020-05-13 15:01:21', 0, 0, 0, 0, 0, 'CA', 'Your appointment was cancelled'),
(13, '2020-05-13 15:03:13', 0, 0, 0, 0, 0, 'CA', 'Your appointment was cancelled'),
(14, '2020-05-13 15:03:51', 0, 0, 0, 0, 0, 'CA', 'Your appointment was cancelled'),
(15, '2020-05-13 15:04:43', 0, 0, 0, 0, 0, 'CA', 'Your appointment was rescheduled'),
(16, '2020-05-13 15:09:01', 0, 0, 0, 0, 0, 'CA', 'Your appointment was rescheduled'),
(17, '2020-05-13 15:09:37', 0, 0, 0, 0, 0, 'CA', 'Your appointment was rescheduled'),
(18, '2020-05-13 15:10:30', 0, 0, 0, 0, 0, 'CA', 'Your appointment was rescheduled'),
(19, '2020-05-13 15:10:47', 0, 0, 21, 1, 23, 'AA', 'Your appointment was approved'),
(20, '2020-05-13 15:11:01', 0, 0, 0, 0, 0, 'CA', 'Your appointment was cancelled'),
(23, '2020-05-16 13:47:03', 0, 0, 0, 0, 0, 'CA', 'Your appointment was cancelled'),
(24, '2020-05-16 13:48:05', 0, 0, 0, 0, 0, 'CA', 'Your appointment was rescheduled'),
(25, '2020-05-26 16:10:35', 0, 1, 5, 1, 8, 'PA', 'Set New Appointment'),
(26, '2020-05-26 16:21:41', 0, 0, 8, 1, 5, 'AA', 'Your appointment was approved'),
(33, '2020-05-26 16:44:34', 0, 0, 8, 1, 23, 'CA', 'Your appointment was rescheduled'),
(36, '2020-05-26 16:47:52', 0, 0, 8, 1, 23, 'CA', 'Your appointment was cancelled'),
(37, '2020-05-27 11:51:46', 0, 1, 23, 1, 8, 'PA', 'Set New Appointment'),
(38, '2020-05-27 11:53:12', 0, 1, 6, 1, 8, 'PA', 'Set New Appointment'),
(39, '2020-06-17 13:42:12', 0, 1, 25, 1, 21, 'PA', 'Set New Appointment'),
(40, '2020-06-17 13:47:46', 0, 1, 23, 1, 21, 'PA', 'Set New Appointment'),
(41, '2020-06-17 13:48:32', 0, 1, 5, 1, 21, 'PA', 'Set New Appointment');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_queuing`
--

CREATE TABLE `tbl_queuing` (
  `queu_id` int(11) NOT NULL,
  `appointment_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_queuing`
--

INSERT INTO `tbl_queuing` (`queu_id`, `appointment_id`, `status`) VALUES
(9, 9, 1),
(10, 11, 1),
(11, 12, 0),
(15, 13, 1),
(16, 14, 1),
(17, 15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `user_firstname` varchar(100) NOT NULL DEFAULT '',
  `user_middlename` varchar(100) NOT NULL,
  `user_lastname` varchar(100) NOT NULL,
  `user_address` varchar(100) NOT NULL,
  `user_conNum` int(11) NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `category_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '0',
  `access` varchar(5) NOT NULL DEFAULT '',
  `date_added` date NOT NULL,
  `user_image` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `user_firstname`, `user_middlename`, `user_lastname`, `user_address`, `user_conNum`, `username`, `password`, `category_id`, `doctor_id`, `is_active`, `access`, `date_added`, `user_image`) VALUES
(1, 'Admin', '', '', '', 0, 'admin', '12345', 0, 0, 0, 'A', '0000-00-00', ''),
(5, 'Abel', 'Tobongbanua', 'Bayon', 'tangub Bacolod City', 2147483647, 'abel', '12345', 0, 0, 0, 'P', '0000-00-00', 'shanks.jpeg'),
(6, 'asdas', 'dasd', 'dasd', 'dadasd', 123, 'a', 'a', 0, 0, 0, 'P', '0000-00-00', ''),
(7, 'Mina', 'Sharon', 'Myoui', 'Bacolod City', 123, 'mina', '12345', 4, 0, 0, 'D', '0000-00-00', ''),
(8, 'Jochelle', 'Esponilla', 'Bravo', 'Bacolod City', 123, 'jo', '12345', 6, 0, 0, 'D', '0000-00-00', ''),
(9, 'Test', 'Test', 'Test', 'Test', 1234567, 'Test', 'Test', 8, 0, 0, 'D', '0000-00-00', ''),
(11, 'Sec', 'Sec', 'Secs', 'Sec', 12312, 'Sec', 'Sec', 0, 9, 0, 'S', '0000-00-00', ''),
(14, 'Chaeyoung', 'Son', 'Son', 'Seoul', 213123, 'Chaeyoung', 'Son', 7, 0, 0, 'D', '0000-00-00', ''),
(20, 'Dahyun', 'Dahyun', 'Kim', 'Seoul', 12312, 'Dahyun', 'Kim', 0, 14, 0, 'S', '0000-00-00', ''),
(21, 'Gray', ' ', 'Fullbuster', 'Tokyo , Japan', 123456789, 'Gray', 'fairytail', 8, 0, 0, 'D', '2020-05-05', 'wp2382500.jpg'),
(22, 'Juvia', ' ', 'Lockser', 'Magnolia', 987654321, 'Juvia', 'gray', 0, 21, 0, 'S', '0000-00-00', '5591126-cover_433.png'),
(23, 'Lucy', ' ', 'Heartfilia', 'Magnolia', 123456789, 'lucy', '12345', 0, 0, 0, 'P', '0000-00-00', 'lucy.png'),
(24, 'Test', 'Test', 'Test', 'TEst', 123456789, 'testni', '12345', 0, 0, 0, 'P', '0000-00-00', ''),
(25, 'San', ' ', 'Marino', 'Pacific Ocean', 123456789, 'san', 'san', 0, 0, 0, 'P', '0000-00-00', ''),
(26, 'Jeongyeon', ' ', 'Yoo', 'Seoul', 1235621, 'Jeongyeon', 'Yoo', 0, 8, 0, 'S', '0000-00-00', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_appointments`
--
ALTER TABLE `tbl_appointments`
  ADD PRIMARY KEY (`appointment_id`);

--
-- Indexes for table `tbl_blood_donors`
--
ALTER TABLE `tbl_blood_donors`
  ADD PRIMARY KEY (`blood_donor_id`);

--
-- Indexes for table `tbl_blood_types`
--
ALTER TABLE `tbl_blood_types`
  ADD PRIMARY KEY (`blood_type_id`);

--
-- Indexes for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tbl_doctor_announcements`
--
ALTER TABLE `tbl_doctor_announcements`
  ADD PRIMARY KEY (`da_id`);

--
-- Indexes for table `tbl_doctor_time_table`
--
ALTER TABLE `tbl_doctor_time_table`
  ADD PRIMARY KEY (`time_id`);

--
-- Indexes for table `tbl_events`
--
ALTER TABLE `tbl_events`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `tbl_followup_appointments`
--
ALTER TABLE `tbl_followup_appointments`
  ADD PRIMARY KEY (`fu_appointment_id`);

--
-- Indexes for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `tbl_queuing`
--
ALTER TABLE `tbl_queuing`
  ADD PRIMARY KEY (`queu_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_appointments`
--
ALTER TABLE `tbl_appointments`
  MODIFY `appointment_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_blood_donors`
--
ALTER TABLE `tbl_blood_donors`
  MODIFY `blood_donor_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_blood_types`
--
ALTER TABLE `tbl_blood_types`
  MODIFY `blood_type_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_categories`
--
ALTER TABLE `tbl_categories`
  MODIFY `category_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_doctor_announcements`
--
ALTER TABLE `tbl_doctor_announcements`
  MODIFY `da_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_doctor_time_table`
--
ALTER TABLE `tbl_doctor_time_table`
  MODIFY `time_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_events`
--
ALTER TABLE `tbl_events`
  MODIFY `event_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_followup_appointments`
--
ALTER TABLE `tbl_followup_appointments`
  MODIFY `fu_appointment_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  MODIFY `notification_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `tbl_queuing`
--
ALTER TABLE `tbl_queuing`
  MODIFY `queu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
