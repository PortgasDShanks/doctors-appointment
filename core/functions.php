<?php

function getCurrentDate(){
	ini_set('date.timezone','UTC');
	date_default_timezone_set('UTC');
	$today = date('H:i:s');
	$date = date('Y-m-d H:i:s', strtotime($today)+28800);
	
	return $date;
}

function clean($str) {
    $str = @trim($str);
    if(get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return mysql_real_escape_string($str);
}

function select_query($type , $table , $params){

	$select_query = mysql_query("SELECT $type FROM $table WHERE $params")or die(mysql_error());
	$fetch = mysql_fetch_assoc($select_query);
	return $fetch;

}

function checkSession(){
	if(!isset($_SESSION['user_id'])){
	header("Location: login.php");
	}
}

function insert_query($table_name, $form_data , $last_id){
    $fields = array_keys($form_data);

    $sql = "INSERT INTO ".$table_name."
    (`".implode('`,`', $fields)."`)
    VALUES('".implode("','", $form_data)."')";

    $return_insert = mysql_query($sql)or die(mysql_error());
    $lastID = mysql_insert_id();

    if($last_id == 'Y'){
        if($return_insert){
            $val = $lastID;
        }else{
            $val = 0;
        }
    }else{
        if($return_insert){
            $val = 1;
        }else{
            $val = 0;
        }
    }

    return $val;
}

function delete_query($table_name, $where_clause=''){
    $whereSQL = '';
    if(!empty($where_clause)){
        if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE'){
            $whereSQL = " WHERE ".$where_clause;
        }else{
            $whereSQL = " ".trim($where_clause);
        }
    }
    $sql = "DELETE FROM ".$table_name.$whereSQL;
    
    $return_delete = mysql_query($sql);
    
    if($return_delete){
    	echo 1;
    }else{
    	echo 0;
    }
}

function update_query($table_name, $form_data, $where_clause=''){
    $whereSQL = '';
    if(!empty($where_clause)){
        if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE'){
            $whereSQL = " WHERE ".$where_clause;
        }else{
            $whereSQL = " ".trim($where_clause);
        }
    }
    $sql = "UPDATE ".$table_name." SET ";
    $sets = array();
    foreach($form_data as $column => $value)
    {
         $sets[] = "`".$column."` = '".$value."'";
    }
    $sql .= implode(', ', $sets);
    $sql .= $whereSQL;

    $return_query = mysql_query($sql);
    if($return_query){
    	echo 1;
    }else{
    	echo 0;
    }
}
function split_words($string, $nb_caracs, $separator){
    $string = strip_tags(html_entity_decode($string));
    if( strlen($string) <= $nb_caracs ){
        $final_string = $string;
    } else {
        $final_string = "";
        $words = explode(" ", $string);
        foreach( $words as $value ){
            if( strlen($final_string . " " . $value) < $nb_caracs ){
                if( !empty($final_string) ) $final_string .= " ";
                $final_string .= $value;
            } else {
                break;
            }
        }
        $final_string .= "<p data-toggle='tooltip' data-placement='top' title='".$string."'>" . $separator . "<p/>";
    }
    return $final_string;
}
function getPosition($project_id , $member_id){

    $get_memberPosition = mysql_fetch_array(mysql_query("SELECT * FROM pms_project_members as pm , pms_users as u WHERE pm.project_member_id = u.user_id AND pm.project_id = '$project_id' AND project_member_id = '$member_id'"));
        if($get_memberPosition['project_access'] == 'P'){
          $position = 'Team Leader/Project Manager';
        }else if($get_memberPosition['project_access'] == 'M'){
          $position = 'Project Member';
        }else{
          $position = '';
        }
        return $position; 
}

function getUser($user_id){
    $getUser = mysql_fetch_array(mysql_query("SELECT CONCAT(user_firstname,' ',user_lastname) as fullname FROM tbl_users WHERE user_id = '$user_id'"));

    return $getUser[0];
}

function getTime($time_id){
    $getUser = mysql_fetch_array(mysql_query("SELECT * FROM tbl_doctor_time_table WHERE time_id = '$time_id'"));

    $time = date("H:i A", strtotime($getUser['time_from']))." - ".date("H:i A", strtotime($getUser['time_to']));
    return $time;
}

function getCategoryType($catID){
    $cat = mysql_fetch_array(mysql_query("SELECT category_name FROM tbl_categories WHERE category_id = '$catID'"));

    return $cat[0];
}
function GETDOCTORCATEGORIES(){
    $content = "<option value=''>  -- Please Select -- </option>";
    $cat = mysql_query("SELECT * FROM tbl_categories");
    while($row = mysql_fetch_array($cat)){
        $name = $row['category_name'];
        $cat_id = $row['category_id'];
        $content .= "<option value='$cat_id'>$name</option>";
    }

    return $content;
}
function GETDOCTORS(){
    $content = "<option value=''>  -- Please Select -- </option>";
    $cat = mysql_query("SELECT * FROM tbl_users as u, tbl_categories as c WHERE u.category_id = c.category_id AND access = 'D'");
    while($row = mysql_fetch_array($cat)){
        $name = $row['user_firstname'].' '.$row['user_lastname'];
        $doc_id = $row['user_id'];
        $content .= "<option value='$doc_id'>$name (<span style='color: blue'>Field: ".$row['category_name']."</span>)</option>";
    }

    return $content;
}
function GETDOCTORCATEGORY($user_id){
    $data = mysql_fetch_array(mysql_query("SELECT c.category_name FROM tbl_categories as c , tbl_users as u WHERE c.category_id = u.category_id AND user_id = '$user_id'"));

    return $data[0];
}
function GETALLAPPOINTMENTS($patient_id){
    $curDate = date("Y-m-d", strtotime(getCurrentDate()));
    $data = "";
    $appointments = mysql_query("SELECT * FROM tbl_appointments WHERE patient_id = '$patient_id' ORDER BY appointment_id DESC");
    $count = mysql_num_rows($appointments);
    if($count > 0){
        while($row = mysql_fetch_array($appointments)){
            $doctor_id = $row['doctor_id'];
            $appointment_id = $row['appointment_id'];
            $appointment_date = $row['appointment_date'];

            $finalDate = ($row['reschedule_date'] != '0000-00-00')?$row['reschedule_date']:$row['appointment_date'];

            $app_status = ($row['status'] == 0 && $finalDate > $curDate)?"<span style='background-color:orange'>PENDING</span>":(($row['status'] == 1 || $finalDate < $curDate)?"<span style='background-color: red'>CANCELLED</span>":(($row['status'] == 2)?"<span style='background-color: blue'>APPROVED</span>":(($row['status'] == 3)?"<span style='background-color: green'>FINISHED</span>":"<span style='background-color: blue'>RESCHEDULED</span>")));

            $btn_stat = ($row['status'] == 0 && $finalDate > $curDate)?"btn-warning":(($row['status'] == 2 || $finalDate < $curDate)?"btn-danger":"btn-primary");

            $data .= "<div class='col-md-4'>
                        <div class='box' id='card-shadow'>
                            <div class='ribbon ribbon-top-right'>".$app_status."</div>
                                <div style='padding: 70px 0px 0px 11px;'>
                                    <h6>Doctor: ".getUser($doctor_id)."</h6>
                                    <h6>Date: ".date("F d, Y", strtotime($appointment_date))."</h6>
                                    <h6>Category: ".GETDOCTORCATEGORY($doctor_id)."</h6>
                                    <button class='btn btn-sm ".$btn_stat." pull-right' style='margin-right: 10px;' onclick='showResult(".$appointment_id.")'><span class='fa fa-eye'></span> Show Result</button>
                                </div>
                        </div>
                    </div>";
        }
    }else{
        $data = "<h4 style='color:#a5a5a5;text-align:center'>NO APPOINTMENT FOUND</h4>";
    }

    return $data;
}
function GETALLNOTIF($receiverID){
    $data = '';
    $query = mysql_query("SELECT * FROM tbl_notification WHERE notification_receiver = '$receiverID' AND notification_type = 0 AND `status` = 0 ORDER BY notification_id DESC");
    
    while($row = mysql_fetch_array($query)){
        $id = $row['user_id'];
        $action = $row['action'];
        
        $userImg = mysql_fetch_array(mysql_query("SELECT user_image FROM tbl_users WHERE user_id = '$id'"));
        

        $img = ($userImg[0] != '')?$userImg[0]:"avatar.png";
           
         $status = ($row['status'] == 0)?"<i class='fa fa-star'></i>":"";

         $data .= "<a href='#' onclick='viewNotif(".$row['notification_id'].")' class='dropdown-item'>
                    <div class='media'>
                        <img src='assets/images/".$img."' style='width: 25px;height: 25px;object-fit: cover' alt='User Avatar' class='img-size-50 mr-3 img-circle'>
                        <div class='media-body'>
                            <p class='text-sm'>".$row['action']." ".getUser($id)."</p>
                        </div>
                    </div>
                    </a>
                    <div class='dropdown-divider'></div>";
            
        
        
    }
    return $data;
}
function GETALLMESSAGES($receiverID){
    $curtime = date("H", strtotime(getCurrentDate()));
    $data = '';
    $query = mysql_query("SELECT * FROM tbl_messages WHERE receiver_id = '$receiverID'  GROUP BY sender_id ORDER BY message_datetime DESC");
    
    while($row = mysql_fetch_array($query)){
        $sender = $row['sender_id'];
        $userImg = mysql_fetch_array(mysql_query("SELECT user_image FROM tbl_users WHERE user_id = '$sender'"));
        $msg_time = date("H", strtotime($row['message_datetime']));

        $msg_time = $msg_time - $curtime;

        $img = ($userImg[0] != '')?$userImg[0]:"avatar.png";

        $status = ($row['message_status'] == 0)?"<i class='fa fa-star'></i>":"";
            

        $data .= "<a href='#' onclick='viewMessage(".$row['message_id'].",".$sender.")' class='dropdown-item'>
                    <div class='media'>
                        <img src='assets/images/".$img."' style='width: 25px;height: 25px;object-fit: cover' alt='User Avatar' class='img-size-50 mr-3 img-circle'>
                        <div class='media-body'>
                            <h3 class='dropdown-item-title'>
                                    ".getUser($sender)."
                                <span class='float-right text-sm text-danger'>".$status."</span>
                            </h3>
                                <p class='text-sm'>New Message From ".getUser($sender)."</p>
                        </div>
                    </div>
                    </a>
                    <div class='dropdown-divider'></div>";
    }
    return $data;
}
function appointments($doctor_id){
    $query = mysql_query("SELECT appointment_id as app_id, 
                                appointment_date as appDate, 
                                '0' as fu_app_id,
                                appointment_id as main_id, 
                                patient_id as patient, 
                                reschedule_date as resched, 
                                appointment_time_id as timeID, 
                                `status` as stat,
                                'main_check' as cat,
                                `description` as descr
                                FROM tbl_appointments 
                                WHERE doctor_id = '$doctor_id'
                                UNION
                                SELECT fa.appointment_main_id as app_id, 
                                fa.appointment_date as appDate, 
                                fa.fu_appointment_id as fu_app_id, 
                                '0' as main_id,
                                fa.patient_id as patient, 
                                fa.reschedule_date as reschedule, 
                                fa.appointment_time_id as timeID,
                                fa.status as stat, 
                                'followup_check' as cat,
                                '' as descr
                                FROM tbl_followup_appointments as fa , tbl_appointments as a 
                                WHERE fa.doctor_id = '$doctor_id' 
                                AND fa.appointment_main_id = a.appointment_id 
                                ORDER BY app_id DESC , appDate DESC");
    $count = 1;
    $data = array();
    $curDate = date("Y-m-d", strtotime(getCurrentDate()));
    while($row = mysql_fetch_array($query)){
        $finalDate = ($row['resched'] != '0000-00-00')?$row['resched']:$row['appDate'];
        $startBtn_status = ($finalDate > $curDate)?"disabled":"";
        $main_id = $row['main_id'];
        $trans_id = ($row['cat'] == 'followup_check')?$row['fu_app_id']:$row['main_id'];
        // $appID = $row['trans_id'];
        switch($row['stat']){
            case 0:
                $status = ($finalDate > $curDate)?"<span style='color: #d27d00b3;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>PENDING</span>":(($finalDate >= $curDate)?"<span style='color: #f5010194;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>NOT APPROVED BY DOCTOR</span>":"<span style='color: #f5010194;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>EXCEEDED TO APPOINTMENT DATE</span>");

                $action = "<div id='btn_cont' class='tooltip-div'><button class='btn btn-sm btn-danger' onclick='cancelAppointment(\"".$trans_id."\",\"".$row['cat']."\")' id='cancel_btn'><span class='fa fa-close'></span> Cancel</button><button style='margin-top: 3px;' class='btn btn-sm btn-info' onclick='reschedAppointment(\"".$trans_id."\",\"".$row['cat']."\",\"".$doctor_id."\")'><span class='fa fa-calendar'></span> Reschedule</button><button style='margin-top: 3px;' class='btn btn-sm btn-primary' id='approve_btn'  onclick='approveAppointment(\"".$trans_id."\",\"".$row['cat']."\")'><span class='fa fa-thumbs-up'></span> Approve</button></div>";
            break;
            case 1:
                $status = "<span style='color: #f5010194;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>CANCELLED BY THE DOCTOR</span>";
                $action = "";
            break;
            case 2:
                $status = ($finalDate >= $curDate)?"<span style='color: #0110d4a6;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>APPROVED</span>":"<span style='color: #f5010194;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>EXCEEDED TO APPOINTMENT DATE</span> ";

                $action = ($finalDate >= $curDate)?"<button id='start_btn' class='btn btn-sm btn-primary '".$startBtn_status." onclick='startAppointment(\"".$trans_id."\",\"".$row['cat']."\")'><span class='fa fa-hand-o-right'></span> Start Appointment</span>":"";
            break;
            case 3:
                $status = "<span style='color: #008000a6;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>FINISHED</span>";
                $action = "<button id='start_btn' class='btn btn-sm btn-success'  onclick='showResults(\"".$trans_id."\",\"".$row['cat']."\")'><span class='fa fa-eye'></span> View Result</span>";
            break;
            case 4:
                $status = ($finalDate >= $curDate)?"<span style='color: #0110d4a6;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>RESCHEDULED</span>":"<span style='color: #f5010194;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>EXCEEDED TO APPOINTMENT DATE</span>";
    
                $action = ($finalDate >= $curDate)?"<button class='btn btn-sm btn-primary '".$startBtn_status." onclick='startAppointment(\"".$trans_id."\",\"".$row['cat']."\")'><span class='fa fa-hand-o-right'></span> Start Appointment</span>":"";
            break;
        }

        $data[] = array(
                "count"             => $count++,
                "main_id"           => $row['main_id'],
                "appointment_date"  => date("F d, Y", strtotime($row['appDate'])),
                "followup_id"       => $row['fu_app_id'],
                "patient_id"        => $row['patient'],
                "patient"           => getUser($row['patient']),
                "reschedule_date"   => ($row['resched'] != '0000-00-00')?date("F d, Y", strtotime($row['resched'])):"<span style='color:red'>N/A</span>",
                "time_id"           => getTime($row['timeID']),
                "description"       => $row['descr'],
                "status"            => $status,
                "action"            => $action,
                "category"          => $row['cat'],
                "app_id"            => $row['app_id']
                );
    }

    return $data;
}
function GETALLPATIENTAPPOINTMENTS($patient_id){
    $query = mysql_query("SELECT appointment_id as app_id, 
                                appointment_date as appDate, 
                                '0' as fu_app_id,
                                appointment_id as main_id, 
                                doctor_id as doctor, 
                                reschedule_date as resched, 
                                appointment_time_id as timeID, 
                                `status` as stat,
                                'main_check' as cat,
                                `description` as descr
                                FROM tbl_appointments 
                                WHERE patient_id = '$patient_id'
                                UNION
                                SELECT fa.appointment_main_id as app_id, 
                                fa.appointment_date as appDate, 
                                fa.fu_appointment_id as fu_app_id, 
                                '0' as main_id,
                                fa.doctor_id as doctor, 
                                fa.reschedule_date as reschedule, 
                                fa.appointment_time_id as timeID,
                                fa.status as stat, 
                                'followup_check' as cat,
                                '' as descr
                                FROM tbl_followup_appointments as fa , tbl_appointments as a 
                                WHERE fa.patient_id = '$patient_id' 
                                AND fa.appointment_main_id = a.appointment_id 
                                ORDER BY app_id DESC , appDate DESC");
    $count = 1;
    $curDate = date("Y-m-d", strtotime(getCurrentDate()));
    $data = array();
    while($row = mysql_fetch_array($query)){
        $finalDate = ($row['resched'] != '0000-00-00')?$row['resched']:$row['appDate'];
        $startBtn_status = ($finalDate > $curDate)?"disabled":"";
        $main_id = $row['main_id'];
        $trans_id = ($row['cat'] == 'followup_check')?$row['fu_app_id']:$row['main_id'];
        switch($row['stat']){
            case 0:
                $status = ($finalDate > $curDate)?"<span style='color: #d27d00b3;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>PENDING</span>":(($finalDate >= $curDate)?"<span style='color: #f5010194;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>NOT APPROVED BY DOCTOR</span>":"<span style='color: #f5010194;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>EXCEEDED TO APPOINTMENT DATE</span>");
                $action = "";
            break;
            case 1:
                $status = "<span style='color: #f5010194;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>CANCELLED BY THE DOCTOR</span>";
                $action = "";
            break;
            case 2:
                $status = ($finalDate >= $curDate)?"<span style='color: #0110d4a6;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>APPROVED</span>":"<span style='color: #f5010194;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>EXCEEDED TO APPOINTMENT DATE</span> ";
                $action = "";
            break;
            case 3:
                $status = "<span style='color: #008000a6;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>FINISHED</span>";
                $action = "<button id='start_btn' class='btn btn-sm btn-success'  onclick='showResults(\"".$trans_id."\",\"".$row['cat']."\")'><span class='fa fa-eye'></span> View Result</span>";
                
            break;
            case 4:
                $status = ($finalDate >= $curDate)?"<span style='color: #0110d4a6;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>RESCHEDULED</span>":"<span style='color: #f5010194;border: 4px solid #b1b1b1;display: block;text-align: center;transform: rotateZ(-10deg);border-radius: 8px;font-weight: 800;'>EXCEEDED TO APPOINTMENT DATE</span>";
                $action = "";
            break;
        }
        $data[] = array(
            "count"             => $count++,
            "main_id"           => $row['main_id'],
            "appointment_date"  => date("F d, Y", strtotime($row['appDate'])),
            "followup_id"       => $row['fu_app_id'],
            "doctor_id"         => $row['doctor'],
            "patient"           => getUser($row['doctor']),
            "reschedule_date"   => ($row['resched'] != '0000-00-00')?date("F d, Y", strtotime($row['resched'])):"<span style='color:red'>N/A</span>",
            "time_id"           => getTime($row['timeID']),
            "description"       => $row['descr'],
            "status"            => $status,
            "category"          => $row['cat'],
            "app_id"            => $row['app_id'],
            "action"            => $action
            );
    }

    return $data;
}
function countFollowUpcheck($main_id){
    $query = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_followup_appointments WHERE appointment_main_id = '$main_id'"));

    return $query[0];
}
function getBloodType_name($bloodID){
    $query = mysql_fetch_array(mysql_query("SELECT * FROM tbl_blood_types WHERE blood_type_id = '$bloodID'"));

    return $query['type_name'];
}

function getQuantityUsed($bloodID){
    $query = mysql_fetch_array(mysql_query("SELECT quantity_used FROM tbl_blood_donors WHERE blood_donor_id = '$bloodID'"));

    return $query[0];
}
function getRemainingQuantity($donor_name){
    $curdate = date("Y-m-d h:i", strtotime(getCurrentDate()));
    $query = mysql_query("SELECT * FROM tbl_blood_donors WHERE blood_donor_name = '$donor_name' ORDER BY date_added DESC")or die(mysql_error());
    $remaining_quantity = 0;
    while($row = mysql_fetch_array($query)){
        $dateAdded = date("Y-m-d h:i", strtotime($row['date_added']));
        $expireDate = date("Y-m-d h:i", strtotime("+42 days",strtotime($dateAdded)));
        if($expireDate <= $curdate){
            $quantity = 0;
        }else{
            $quantity = $row['quantity_bags'] - $row['quantity_used'];
        }

        $remaining_quantity += $quantity;
    }

    return $remaining_quantity;
}
function countMessages($receiverID){
    $count = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_messages WHERE receiver_id = '$receiverID' AND message_status = 0"));

    return $count[0];
}
function getAppointmentForToday($doctor_id){
    $curdate = date("Y-m-d", strtotime(getCurrentDate()));
    $data = "<div class='row'>";
    $query = mysql_query("SELECT patient_id as patient, appointment_time_id as appTime FROM `tbl_appointments` WHERE (status = 2 OR status = 4) AND (appointment_date = '$curdate' OR reschedule_date = '$curdate') AND doctor_id = '$doctor_id'
        UNION
        SELECT patient_id as patient, appointment_time_id as appTime FROM tbl_followup_appointments WHERE (status = 2 OR status = 4) AND (appointment_date = '$curdate' OR reschedule_date = '$curdate') AND doctor_id = '$doctor_id'");
    $count = mysql_num_rows($query);
    if($count > 0){
        while($row = mysql_fetch_array($query)){
            $patient = $row['patient'];
            $getImg = mysql_fetch_array(mysql_query("SELECT user_image FROM tbl_users WHERE user_id = '$patient'"));
            $img = ($getImg['user_image'] != "")?$getImg['user_image']:"avatar.png";
            $patient_name = getUser($row['patient']);
            $appointment_time = getTime($row['appTime']);
            $data .= "
                        <div class='col-md-6'>
                        <div class='card'>
                        <div class='card-body'>
                            <div class='row'>
                            <div class='col-md-2'>
                                <img src='assets/images/$img' class='img-circle' style='height: 50px; width: 50px; border-radius: 50%; object-fit: cover;' alt='User Image'>
                            </div>
                            <div class='col-md-10'>
                                <h6>Name: $patient_name</h6>
                                <h6>Appointment Time: $appointment_time</h6>
                            </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    ";
        }
       $data .= "</div>";
    }else{
        $data = "<h5>No Appointments for Today</h5>";
    }
    return $data;
}
?>