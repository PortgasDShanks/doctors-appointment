<?php 
function checkSession($user_id){
	if(!isset($user_id)){
	header("Location: login.php");
	}
}
function getCurrentDate(){
	ini_set('date.timezone','UTC');
	date_default_timezone_set('UTC');
	$today = date('H:i:s');
	$date = date('Y-m-d H:i:s', strtotime($today)+28800);
	
	return $date;
}

function clean($str) {
    $str = @trim($str);
    if(get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return mysql_real_escape_string($str);
}
function SELECT_DATA($type , $table , $params=''){
    $inject = ($params == '')?"":" WHERE $params";
	$select_query = mysql_query("SELECT $type FROM $table $inject")or die(mysql_error());
	$fetch = mysql_fetch_array($select_query);
	return $fetch;

}
function SELECT_LOOP_DATA($type , $table , $params = ''){
    $data = "";
    $inject = ($params=='')?"":"WHERE $params";
    $fetch = mysql_query("SELECT $type FROM $table $inject")or die(mysql_error());
    while ($row = mysql_fetch_array($fetch)) {
        $data[] = $row;
    }
    return $data;
}
function PMS_INSERT_DATA($table_name, $form_data , $last_id){
    $fields = array_keys($form_data);

    $sql = "INSERT INTO ".$table_name."
    (`".implode('`,`', $fields)."`)
    VALUES('".implode("','", $form_data)."')";

    $return_insert = mysql_query($sql)or die(mysql_error());
    $lastID = mysql_insert_id();

    if($last_id == 'Y'){
        if($return_insert){
            $val = $lastID;
        }else{
            $val = 0;
        }
    }else{
        if($return_insert){
            $val = 1;
        }else{
            $val = 0;
        }
    }

    return $val;
}

function PMS_DELETE_QUERY($table_name, $where_clause=''){
    $whereSQL = '';
    if(!empty($where_clause)){
        if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE'){
            $whereSQL = " WHERE ".$where_clause;
        }else{
            $whereSQL = " ".trim($where_clause);
        }
    }
    $sql = "DELETE FROM ".$table_name.$whereSQL;
    
    $return_delete = mysql_query($sql);
    
    if($return_delete){
    	echo 1;
    }else{
    	echo 0;
    }
}

function PMS_UPDATE_QUERY($table_name, $form_data, $where_clause=''){
    $whereSQL = '';
    if(!empty($where_clause)){
        if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE'){
            $whereSQL = " WHERE ".$where_clause;
        }else{
            $whereSQL = " ".trim($where_clause);
        }
    }
    $sql = "UPDATE ".$table_name." SET ";
    $sets = array();
    foreach($form_data as $column => $value)
    {
         $sets[] = "`".$column."` = '".$value."'";
    }
    $sql .= implode(', ', $sets);
    $sql .= $whereSQL;

    $return_query = mysql_query($sql);
    if($return_query){
    	echo 1;
    }else{
    	echo 0;
    }
}
function getUser($user_id){
    $getUser = SELECT_DATA("CONCAT(firstname,' ',lastname) as fullname","tbl_users" ,"user_id = '$user_id'");

    return $getUser[0];
}
function GETALL_CLINICS_STATUS($status){
    $count = SELECT_DATA("count(*) as counter","tbl_clinic","clinic_status = '$status'");

    return $count['counter'];
}

function getTime($time_id){
    $getUser = SELECT_DATA("*","tbl_doctor_time_table","time_id = '$time_id'");

    $time = date("H:i A", strtotime($getUser['time_from']))." - ".date("H:i A", strtotime($getUser['time_to']));
    return $time;
}
function GETALL_PENDING_STATUS($user_id){
    $getAppointment = SELECT_DATA("count(*) as counter","tbl_appointments"," status = 0 AND doctor_id = '$user_id'");


    return $getAppointment['counter'];
}
function GETALL_TODAYS_APPOINTMENT($user_id){
    $curdate = date("Y-m-d", strtotime(getCurrentDate()));

    $getToday = SELECT_DATA("IF(main.total IS NULL,0,main.total) + IF(followup.total IS NULL,0,followup.total) as all_total", "(SELECT count(*) as total FROM tbl_appointments WHERE doctor_id = '$user_id' AND (appointment_date = '$curdate' OR reschedule_date = '$curdate')) as main,
        (SELECT count(*) as total FROM tbl_followup_appointments WHERE doctor_id = '$user_id' AND (appointment_date = '$curdate' OR reschedule_date = '$curdate')) as followup");

    return $getToday['all_total'];
}
function getAllpatientsAllTime($user_id){
    $getAppointment = SELECT_DATA("count(*) as counter","tbl_appointments"," status = 2 AND doctor_id = '$user_id'");


    return $getAppointment['counter'];
}
?>