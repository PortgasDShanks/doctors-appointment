<?php
// switch($accessType){
//   case 'S':
//     $getDoc_sec = mysql_fetch_array(mysql_query("SELECT * FROM tbl_users WHERE user_id = '$user_id'"));
//     $receiverID = $getDoc_sec['doctor_id'];
//   break;
//   case 'D' || 'P':
//     $receiverID = $user_id;
// }
// $display = ($accessType == 'S')?"display:none":"";
// // header('Content-Type: text/event-stream');
// // header('Cache-Control: no-cache');
// $countAll = mysql_fetch_array(mysql_query("SELECT n.n_t as notif , m.m_t as msg FROM
// (SELECT count(*) as n_t FROM tbl_notification WHERE 
// notification_receiver = '$receiverID' 
// AND notification_type = 0 
// AND `status` = 0) as n,

// (SELECT count(*) m_t FROM tbl_messages WHERE
// receiver_id = '$receiverID'
// AND message_status = 0) as m"));
// "data: {$countAll}\n\n";
// flush();
?>
<nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom" style='background-image: linear-gradient(-20deg, #2b5876 0%, #4e4376 100%) !important;'>
  <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fa fa-bars"></i></a>
      </li>
    </ul>
   <!-- Right navbar links -->
    <?php if($accessType == 'admin'){ ?>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
        <a class="nav-link" href="#" onclick="logout()">
          <i class="fa fa-power-off"></i>
        </a>
      </li>
      </ul>
    <?php }else { ?>
      <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fa fa-comments" style='color:white'></i>
          <span class="badge badge-danger navbar-badge"><?php//$countAll['msg']?></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <div style='max-height: 300px;overflow: auto;'>
              <?php//GETALLMESSAGES($receiverID)?>
            </div>
           <div class="dropdown-divider"></div>
            <a href="#" onclick="window.location='index.php?access=see-all-msg&msgid=0'" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fa fa-bell" style='color:white'></i>
          <span class="badge badge-danger navbar-badge"><?php//$countAll['notif']?></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <div style='max-height: 200px;overflow: auto;'>
              <?php//GETALLNOTIF($receiverID)?>
            </div>
           <div class="dropdown-divider"></div>
            <!-- <a href="#" onclick="window.location='index.php?page=see-all&id='" class="dropdown-item dropdown-footer">See All Notification</a> -->
        </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fa fa-cogs"  style='color:white'></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <div class="dropdown-divider"></div>
          <ul style="list-style: none;padding:5px;cursor: pointer;">
            <li style='padding: 18px;'>
              
              <ol class="breadcrumb liHover" id="a" style="cursor: pointer;background-color: #ffffff26;margin-top:5%;padding:5px;">
                <li class="breadcrumb-item">
                <?php if(!empty($users['user_image'])){?>
                  <img src="assets/images/<?php echo $users['user_image'];?>" class="img-circle" style="height: 40px; width: 40px; border-radius: 50%; object-fit: cover;" alt="User Image">
                <?php } else { ?>
                  <img src="assets/images/avatar.png" class="img-circle" style="height: 40px; width: 40px; border-radius: 50%; object-fit: cover;" alt="User Image">
                <?php } ?>
                </li>
                &nbsp;&nbsp;
                  <li class="liHover" onclick="userProfile()"><span class="fa fa-user" style="color: #6c757d"></span> <h6 style="color: #6c757d;display: inline;">Profile</h6>  <h6 style="font-size: 10px;color: #6c757d;">View your profile</h6></li>
              </ol>
            </li>
            <div class="dropdown-divider"></div>
            <?php if($accessType == 'clinic'){ ?>
            <!-- <li style='padding: 18px;<?=$display?>' onclick="add_user()">
              <span class="fa fa-plus-circle"></span> Secretary
            </li>
            <div class="dropdown-divider"></div>
            <li style='padding: 18px;' onclick="time_table()">
              <span class="fa fa-clock-o"></span> Time Table
            </li> -->
            <?php } ?>
            <li style='padding: 18px;' onclick="logout()">
              <span class="fa fa-sign-out"></span> Logout
            </li>
          </ul >
      </li>
    </ul>
    <?php } ?>
  </nav>