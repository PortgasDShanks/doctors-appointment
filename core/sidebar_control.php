<?php
if($access == 'dashboard'){
  $dash = 'active';
}else if($access == 'categories'){
  $cat = 'active';
}else if($access == 'clinics'){
  $doc = 'active';
}else if($access == 'appointment'){
  $app = 'active';
}else if($access == 'patient-records'){
  $rec = 'active';
}else if($access == 'patient-appointments'){
  $pa = 'active';
}else if($access == 'calendar'){
  $calen = 'active';
}else if($access == 'blood-lists'){
  $bd = 'active';
}else if($access == 'reports'){
  $rep = 'active';
}else{
  $dash = '';
  $cat = '';
  $doc = '';
  $app = '';
  $rec = '';
  $pa = '';
  $calen = '';
  $bd = '';
  $rep = '';
}
?>