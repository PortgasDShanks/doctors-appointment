<?php
require 'sidebar_control.php';
?>
<aside class="main-sidebar sidebar-light-secondary elevation-4" style='background-image: linear-gradient(-20deg, #2b5876 0%, #4e4376 100%) !important;'>
   <a href="#" class="brand-link">
      <!-- <img src="assets/images/icon.png" alt="No Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8"> -->
      <span class="brand-text" style='color:white'><strong>D A M S</strong></span>
    </a>
    <div class="sidebar">
      <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex" onclick="userProfile()">
      
        <div class="image" >
        <?php if(!empty($users['user_image'])){?>
          <img src="assets/images/<?php echo $users['user_image'];?>" class="img-circle" style="height: 50px; width: 50px; border-radius: 50%; object-fit: cover;" alt="User Image">
        <?php } else { ?>
          <img src="assets/images/avatar.png" class="img-circle" style="height: 50px; width: 50px; border-radius: 50%; object-fit: cover;" alt="User Image">
        <?php } ?>
         
        </div>
        <div class="info" style="padding: 13px;">
          <a href="#" class="d-block"><?php //echo getUser($user_id)?></a>
        </div>
      </div> -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" style="margin-top: 5px;">
          <li class="nav-item has-treeview" style='text-align: center;background-color: #c3c3c3;border-radius: 10px;'>
              <p style="font-size: 16px;padding-top: 15px;">
                &mdash; Navigation Panel &mdash; 
              </p>
            
          </li>
          <li class="nav-item has-treeview" style='padding-top: 10px;'>
            <a href="index.php?access=dashboard" class="nav-link <?=($access == 'dashboard')?"active":"";?>">
              <i class="nav-icon fa fa-dashboard" style="font-size: 15px;color:white"></i>
              <p style="font-size: 16px;color:white">
                Dashboard
              </p>
            </a>
          </li>
          <?php if($accessType == 'clinic'){ ?>
            <li class="nav-item has-treeview">
              <a href="index.php?access=time-schedules" class="nav-link <?=($access == 'time-schedules')?"active":"";?>">
                <i class="nav-icon fa fa-clock-o" style="font-size: 15px;color:white"></i>
                <p style="font-size: 16px;color:white">
                  Time Schedules
                </p>
              </a>
            </li>

            <li class="nav-item has-treeview">
              <a href="index.php?access=calendar" class="nav-link <?=($access == 'calendar')?"active":"";?>">
                <i class="nav-icon fa fa-calendar" style="font-size: 15px;color:white"></i>
                <p style="font-size: 16px;color:white">
                  Calendar
                </p>
              </a>
            </li>

            <li class="nav-item has-treeview">
              <a href="index.php?access=patient-appointments" class="nav-link <?=($access == 'patient-appointments')?"active":""?>">
                <i class="nav-icon fa fa-list" style="font-size: 15px;color:white"></i>
                <p style="font-size: 16px;color:white">
                  Patient Appointments
                </p>
              </a>
            </li>

          <!-- <li class="nav-item has-treeview">
            <a href="index.php?page=blood-lists" class="nav-link <?php echo $bd; ?>">
              <i class="nav-icon fa fa-gift" style="font-size: 15px;color:white"></i>
              <p style="font-size: 16px;color:white">
                Blood Donors
              </p>
            </a>
          </li> -->

          
           <!-- <li class="nav-item has-treeview">
            <a href="index.php?access=reports" class="nav-link <?php echo $reports; ?>">
              <i class="nav-icon fa fa-print" style="font-size: 15px;color:white"></i>
              <p style="font-size: 16px;color:white">
                Reports
              </p>
            </a>
          </li> -->
          <?php } ?>
          <?php if($accessType == 'admin'){ ?>
          <li class="nav-item has-treeview">
            <a href="index.php?access=clinics" class="nav-link <?php echo $doc; ?>">
              <i class="nav-icon fa fa-hospital-o" style="font-size: 15px;color:white"></i>
              <p style="font-size: 16px;color:white">
                Clinics
              </p>
            </a>
          </li>
          <?php } ?>
          <?php if($accessType == 'patient') { ?>
            <li class="nav-item has-treeview">
              <a href="index.php?access=clinic-directory" class="nav-link <?=($access == 'clinic-directory')?"active":""; ?>">
                <i class="nav-icon fa fa-folder-open" style="font-size: 15px;color:white"></i>
                <p style="font-size: 16px;color:white">
                 Clinic Directories
                </p>
              </a>
            </li>
            <li class="nav-item has-treeview">
              <a href="index.php?access=appointment" class="nav-link <?=($access == 'appointment')?"active":"";?>">
                <i class="nav-icon fa fa-calendar" style="font-size: 15px;color:white"></i>
                <p style="font-size: 16px;color:white">
                 My Appointments
                </p>
              </a>
            </li>
            <!-- <li class="nav-item has-treeview">
              <a href="index.php?page=patient-records" class="nav-link <?php echo $rec; ?>">
                <i class="nav-icon fa fa-history" style="font-size: 15px;"></i>
                <p style="font-size: 16px;">
                  Records
                </p>
              </a>
            </li> -->
          <?php } ?>
        </ul>
      </nav>
    </div>
  </aside>