
<?php 
$clinic_id = $_GET['cid'];
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item" onclick="window.location='index.php?access=clinic-directory'"><a href="#" style="font-size: 20px">Clinic Directories</a></li>
          <li class="breadcrumb-item active" style="font-size: 20px">Set Appointments</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
      <div class="container-fluid">
      <input type='hidden' id='userID' value="<?php echo $user_id; ?>">
      <input type='hidden' id='clinicID' value="<?php echo $clinic_id; ?>">
        <div class="row">
          <!-- <div class="col-lg-6">
            <div class="card" style='height: 150px;'>
              <div class="card-body">
              </div>
            </div>
          </div> -->

          <div class="col-lg-6">
            <div class="card" style='height: 150px;'>
                <div class="card-body">
                    <div class='col-md-12'>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><strong>Date:</strong></span>
                            </div>
                            <input type='text'  id='datepicker_1' onchange='dateCheck()' class='form-control'>
                        </div>
                    </div>
                    <div id='alertCont'></div>
                </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="card" style='height: 200px;'>
              <div class="card-body">
                <div class='col-md-12' id='timeCont'>
                  
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-12">
            <div class="card" style='height: 200px;'>
              <div class="card-body">
                 <div class='col-md-12'>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>What are you feeling right now? </strong></span>
                      </div>
                      <textarea style='resize: none' rows='5' placeholder='Description here...' class='form-control' id='appDesc'></textarea>
                    </div>
                    <div id='desc_cont'></div>
                </div>
              </div>
            </div>
          </div>
          

          <div class='col-md-12'>
            <div class='col-md-12' style='margin-top: 10px'><button class='btn btn-sm btn-primary pull-right' id='set_appointment' onclick='setAppointment()'><span class='fa fa-check-circle'></span> Continue</button></div>
          </div>
        </div>
      </div>
    </div>
<?php require 'modals/announcement_modal.php'; ?>
<script>
function doctor(){
    var docID = $("#docID").val();
    if(docID != ''){
        $("#datepicker").prop("disabled", false);
    }else{
        $("#datepicker").prop("disabled", true);
    }
    getDoctorDesc(docID);
}
function getDoctorDesc(doctorID){
  $.post("ajax/getDoctorDesc.php", {
    doctorID: doctorID
  }, function(data){
    $("#desc_cont").html(data);
  });
}
function showDetails(eventID){
   $("#announcement").modal('show');
   $.post("ajax/getAnnouncement_text.php", {
    eventID: eventID
   }, function(data){
    
    $("#cont").html(data);
   });
}
function dateCheck(){
    var docID = $("#docID").val();
    var app_date = $("#datepicker_1").val();
 
    $.post("ajax/checkDateAvailability.php", {
        docID: docID,
        app_date: app_date
    }, function(data){
        if(data > 0){
            $("#alertCont").html("<h6 style='color:red;margin-top:10px;'>Selected date is not available <a href='#' onclick='showDetails("+data+")'>(See Details)</a></h6>");
            $("#timeCont").css("display","none");
        }else{
            $("#alertCont").html("<h6 style='color:green;margin-top:10px;text-align:right'>Selected date is available</h6>");
            getTime(app_date);
            $("#timeCont").css("display","block");
        }
    });
}
function getTime(appointmentDate){
  var clinicID = $("#clinicID").val();
   $("#timeCont").html("<span class='fa fa-spin fa-spinner'></span>");
    $.post("ajax/getDoctor_time.php", {
        clinicID: clinicID,
        appointmentDate: appointmentDate
    }, function(data){
        $("#timeCont").html(data);
    });
}
function setAppointment(){
    var clinicID = $("#clinicID").val();
    var app_date = $("#datepicker_1").val();
    var userID = $("#userID").val();
    var time_id = $("input[id='time']:checked").val();
    var appDesc = $("#appDesc").val();
    if(app_date == '' || appDesc == '' || time_id == ''){
        alertWarning();
    }else{
       swal({
            title: "Finish Setting up your appointment?",
            text: "",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Continue",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false
            },
            function(isConfirm) {
            if (isConfirm) {
                SET_APPOINTMENT(clinicID,app_date,userID,time_id,appDesc);
            } else {
                location.reload();
            }
        }); 
    }
    
}
function SET_APPOINTMENT(clinicID,app_date,userID,time_id,appDesc){
    
    $("#set_appointment").prop("checked", true);
    $("#set_appointment").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
    $.post("ajax/set_patient_appointment.php", {
        clinicID: clinicID,
        app_date: app_date,
        userID: userID,
        time_id: time_id,
        appDesc: appDesc
    }, function(data){
     // alert(data);
        if(data > 0){
            swal({
                title: "All Good!",
                text: "Appointment was successfully finished, Please wait for the approval.",
                type: "success"
            }, function(){
                window.location='index.php?access=appointment';
            });
        }else{
            failedAlert();
        }
    });
}
</script>
