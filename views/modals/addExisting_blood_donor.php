<div class="modal fade" id="addExistdonor" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				 <h6 class="modal-title">Add Existing Blood Donor</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                  <input type="hidden" id='cat'>
              </div>
			<div class="modal-body">
				<div class="row">
                    <div class='col-md-12'>
                        <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Name:</strong></span>
                        </div>
                        <select class='form-control' id='exist_donor_name' onchange="checkDonor()">
                        <option value=''>&mdash; Please Choose &mdash;</option>
                        <?php 
                            $query = mysql_query("SELECT * FROM tbl_blood_donors GROUP BY blood_donor_name");
                            while($row = mysql_fetch_array($query)){
                                echo "<option value='".$row['blood_donor_name']."'>".$row['blood_donor_name']."</option>";
                            }
                        ?>
                        </select>
                        </div>
                    </div>
                    <div class='col-md-12' style='margin-top:10px;'>
                        <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Address:</strong></span>
                        </div>
                        <textarea style='resize:none' class='form-control' row='5' id='exist_donor_address' readonly></textarea>
                        </div>
                    </div>
                    <div class='col-md-12' style='margin-top:10px;'>
                        <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Contact No.:</strong></span>
                        </div>
                        <input class='form-control' type='text' id='existdonor_contact' readonly>
                        </div>
                    </div>
                    <div class='col-md-12' id='bloodType' style='margin-top:10px'>
                        <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Blood Type:</strong></span>
                        </div>
                        <input type='text' class='form-control' id='type_name' readonly>
                        <input type='hidden' class='form-control' id='type_id'>
                        </div>
                    </div>
                    <div class='col-md-12' style='margin-top:10px;'>
                        <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Quantity(Bags):</strong></span>
                        </div>
                        <input class='form-control' type='number' id='exist_quantity'>
                        </div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_ebd" onclick="add_existbloodDonor('exist','add')"><span class="fa fa-check"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
			</div>
		</div>
	</div>
</div>