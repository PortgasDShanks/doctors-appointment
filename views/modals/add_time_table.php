<div class="modal fade" id="addtt" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				 <h6 class="modal-title">Add Time Table</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
               
              </div>
			<div class="modal-body">
				<div class="row">
                    <div class='col-md-12'>
                        <div class="input-group" >
                            <div class="input-group-prepend">
                              <span class="input-group-text"><strong>Day:</strong></span>
                            </div>
                             <select class='form-control' id="time_day">
                                  <?php
                                    foreach ($array as $day => $num) {
                                      echo "<option value='$num'>$day</option>";
                                    }
                                  ?>
                              </select>
                          </div>
                    </div>
                    <br>
					<div class='col-md-6' style='margin-top: 10px'>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><strong>From:</strong></span>
                            </div>
                            <input type="time" class="form-control" id="timepicker1" name="timepicker1">
                        </div>
                    </div>
                    <div class='col-md-6' style='margin-top: 10px'>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><strong>To:</strong></span>
                            </div>
                            <input type="time" class="form-control" id="timepicker2" name="timepicker2">
                        </div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_cat" onclick="add_tt('add')"><span class="fa fa-check"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
			</div>
		</div>
	</div>
</div>