<div class="modal fade" id="followups" tabindex="-1" style='z-index: 10000' data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				 <h6 class="modal-title">Follow Up Checkups</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
              </div>
			<div class="modal-body">
			 <div class='col-md-12'  >
                  <table id='reports_followup' class="table table-bordered table-hover" style='width:100%;'>
                      <thead style='background-color: #343940;color: white;'>
                          <tr>
                              <th>APPOINTMENT DATE</th>
                              <th>PATIENT NAME</th>
                              <th>DIAGNOSIS</th>
                              <th>REMARKS</th>
                          </tr>
                      </thead>
                      <tbody></tbody>
                  </table>
               </div>
			</div>
			<div class="modal-footer">
				<span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_cat" onclick="add_announcement('add')"><span class="fa fa-check"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
			</div>
		</div>
	</div>
</div>