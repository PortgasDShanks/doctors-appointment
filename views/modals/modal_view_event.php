<div class="modal fade" id="viewEvent" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				 <h6 class="modal-title">View Event</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
    </div>
		<div class="modal-body">
			<div class="container">
                <div id='event_cont' style='display:none'>
                    <div class="input-group">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><strong> Title:</strong></span>
                        </div>
                        <textarea style='resize:none' class='form-control' row='5' id='eventName' readonly></textarea>
                        
                    </div>
                    <br>
                    <div class="input-group">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><strong> Date:</strong></span>
                        </div>
                        <textarea style='resize:none' class='form-control' row='5' id='eventDate' readonly></textarea>
                    </div>
                    <br>
                    <div class="input-group">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><strong> Time:</strong></span>
                        </div>
                        <input type="time" class="form-control" id="event_time" readonly>
                    </div>
                </div>
                <div id='appointment_cont' style='display:none'>
                    <div class="input-group">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><strong> Patient Name:</strong></span>
                        </div>
                        <input type="text" class="form-control" id="patient_name" readonly>
                    </div>
                    <br>
                    <div class="input-group">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><strong> Appointment Time:</strong></span>
                        </div>
                        <input type="text" class="form-control" id="appointment_time" readonly>
                    </div>
                    <br>
                    <div class="input-group">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><strong> Rescheduled Appointment? :</strong></span>
                        </div>
                        <input type="text" class="form-control" id="recheduled" readonly>
                    </div>
                    <br>
                    <div class="input-group">
                        <div class="input-group-prepend">
                        <span class="input-group-text"><strong> Appointment Date :</strong></span>
                        </div>
                        <input type="text" class="form-control" id="appointment_date" readonly>
                    </div>
                </div>
                
            </div>
		</div>
        <div class="modal-footer">
            <span class="btn-group">
                <button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
            </span>
        </div>
		</div>
	</div>
</div>