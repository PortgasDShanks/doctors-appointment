<div class="modal fade" id="followup" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				 <h6 class="modal-title">Follow up Appointment</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
               
              </div>
			<div class="modal-body">
                    <div class='col-md-12'>
                        <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Date:</strong></span>
                        </div>
                        <input type="date" onchange="followup()" class="form-control" id="followupCheck" name="followupCheck">
                        </div>
                    </div>
					<div class='col-md-12' id='time_cont' style='margin-top: 10px;'></div>
                    <input type="hidden" class="form-control" id="doctorID" name="doctorID">
                    <input type="hidden" class="form-control" id="appID" name="appID">
				
			</div>
			<div class="modal-footer">
				<span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_addFUC" onclick="addFollowupCheck()"><span class="fa fa-check"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
			</div>
		</div>
	</div>
</div>