<div class="modal fade" id="editCat" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				 <h6 class="modal-title">Edit Specialization</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
               
              </div>
			<div class="modal-body">
				<div class="container">
					<div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Specialization:</strong></span>
                      </div>
                      <input type="text" class="form-control" id="Cname_edit" name="Cname_edit">
                      <input type="hidden" id="Cid" name="Cid">
					  <input type="hidden" id="Caction" name="Caction">
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_edit_cat" onclick="editCat()"><span class="fa fa-check"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
			</div>
		</div>
	</div>
</div>