<div class="modal fade" id="bloodLists" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				 <h6 class="modal-title"><span class='fa fa-th-list'></span> Blood Lists</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
               
              </div>
			<div class="modal-body">
				<div class="row">
                    <div class='col-md-12'><h6>Legend:</h6></div>
                    <div class='col-md-12'> 
                    <span style="color: rgba(0, 128, 0, 0.52);font-size: 15px;"> <i class="fa fa-square"></i></span> <span style="font-weight:bolder;"></span> <span> Used</span> ::
		            <span style="color: rgba(255, 17, 0, 0.55);font-size: 15px;"> <i class="fa fa-square"></i></span> <span style="font-weight:bolder;"></span> <span> Expired</span> 
                    </div>
                   
					<div class='col-md-12'>
                        <table id='blood_list_details' class="table table-bordered table-hover" style='width: 100%;'>
                        <thead style='background-color: #343940;color: white;'>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>DATE ADDED</th>
                                <th>QUANTITY</th>
                                <th>QUANTITY TO USE</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        </table>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<span class="btn-group">
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
			</div>
		</div>
	</div>
</div>