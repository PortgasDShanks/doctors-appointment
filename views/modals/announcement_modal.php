<div class="modal fade" id="announcement" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				 <h6 class="modal-title">Announcement</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
               
              </div>
			<div class="modal-body">
                <div class="card card-primary card-outline">
                <div class="card-body">
                    <div class='col-md-12' id='cont'>
                        
                    </div>
                </div>
                </div><!-- /.card -->
			</div>
			<div class="modal-footer">
				<span class="btn-group">
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
			</div>
		</div>
	</div>
</div>