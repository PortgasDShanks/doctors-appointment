<div class="modal fade" id="edittt" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				 <h6 class="modal-title">Edit Time Table</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
               
              </div>
			<div class="modal-body">
				<div class="row">
					<div class='col-md-6'>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><strong>From:</strong></span>
                            </div>
                            <input type="time" class="form-control" id="timepicker1_e" name="timepicker1_e">
                        </div>
                    </div>
                    <div class='col-md-6'>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><strong>To:</strong></span>
                            </div>
                            <input type="time" class="form-control" id="timepicker2_e" name="timepicker2_e">
                            <input type="hidden" class="form-control" id="timeID" name="timeID">
                        </div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_edit_tt" onclick="edit_tt('edit')"><span class="fa fa-check"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
			</div>
		</div>
	</div>
</div>