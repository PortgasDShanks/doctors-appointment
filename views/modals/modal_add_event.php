<div class="modal fade" id="addEvent" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				 <h6 class="modal-title">Add Event</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
      </div>
			<div class="modal-body">
				<div class="container">
          <form id="event_form" method="POST">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><strong> Title:</strong></span>
            </div>
            <input type="text" class="form-control" id="event_title" name="addEvent_title" required>
            
          </div>
          <br>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><strong> Date:</strong></span>
            </div>
            <input type="text" class="form-control" id="daterange" name="addEvent_date" required>
          </div>
          <br>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><strong> Time:</strong></span>
            </div>
            <input type="time" class="form-control" id="addEvent_time" name="addEvent_time" required>
          </div>
        </div>
			</div>
			<div class="modal-footer">
				<span class="btn-group">
					<button onclick="addEvents()" class="btn btn-primary btn-sm" id="btn_add_event" type="button"><span class="fa fa-check"></span> Continue</button>
        </form>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
			</div>
		</div>
	</div>
</div>