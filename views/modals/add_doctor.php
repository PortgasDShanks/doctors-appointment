<div class="modal fade" id="addDoc" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				 <h6 class="modal-title">Add Doctor</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
               
              </div>
			<div class="modal-body">
				<div class="container">
					<div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Firstname:</strong></span>
                      </div>
                      <input type="text" class="form-control" id="d_fname" name="d_fname">
                    </div>
					<div class="input-group" style='margin-top:10px;'>
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Middlename:</strong></span>
                      </div>
                      <input type="text" class="form-control" id="d_mname" name="d_mname">
                    </div>
					<div class="input-group" style='margin-top:10px;'>
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Lastname:</strong></span>
                      </div>
                      <input type="text" class="form-control" id="d_lname" name="d_lname">
                    </div>
					<div class="input-group" style='margin-top:10px;'>
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Address:</strong></span>
                      </div>
                      <textarea style='resize:none' class='form-control' row='5' id='d_address' ></textarea>
                    </div>
					<div class="input-group" style='margin-top:10px;'>
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Contact #:</strong></span>
                      </div>
                      <input type="number" class="form-control" id="d_connum" name="d_connum">
                    </div>
					<div class="input-group" style='margin-top:10px;'>
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Category:</strong></span>
                      </div>
                      <select class='form-control' id='d_cat'>
					  	<?php echo GETDOCTORCATEGORIES()?>
					  </select>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_doc" onclick="add_doctor('add')"><span class="fa fa-check"></span> Save </button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
			</div>
		</div>
	</div>
</div>