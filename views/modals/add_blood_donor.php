<div class="modal fade" id="adddonor" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				 <h6 class="modal-title">Add New Blood Donor</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                  <input type="hidden" id='cat'>
              </div>
			<div class="modal-body">
				<div class="row">
					<div class='col-md-12'>
                        <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Name:</strong></span>
                        </div>
                        <input class='form-control' type='text' id='donor_name'>
                        </div>
                    </div>
                    <div class='col-md-12' style='margin-top:10px;'>
                        <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Address:</strong></span>
                        </div>
                        <textarea style='resize:none' class='form-control' row='5' id='donor_address' ></textarea>
                        </div>
                    </div>
                    <div class='col-md-12' style='margin-top:10px;'>
                        <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Contact No.:</strong></span>
                        </div>
                        <input class='form-control' type='text' id='donor_contact'>
                        </div>
                    </div>
                    <div class='col-md-12' id='bloodType' style='margin-top:10px'>
                        <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Blood Type:</strong></span>
                        </div>
                        <select class='form-control' id='blood_type'>
                        <option value=''>&mdash; Please Choose &mdash;</option>
                        <?php 
                            $query = mysql_query("SELECT * FROM tbl_blood_types");
                            while($row = mysql_fetch_array($query)){
                                echo "<option value='".$row['blood_type_id']."'>".$row['type_name']."</option>";
                            }
                        ?>
                        </select>
                        </div>
                    </div>
                    <div class='col-md-12' style='margin-top:10px;'>
                        <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Quantity(Bags):</strong></span>
                        </div>
                        <input class='form-control' type='number' id='blood_quantity'>
                        </div>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_b" onclick="add_bloodDonor('new','add')"><span class="fa fa-check"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
			</div>
		</div>
	</div>
</div>