<div class="modal fade" id="addSec" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				 <h6 class="modal-title">Add Secretary</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
               
              </div>
			<div class="modal-body">
				<div class="container">
					<div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Firstname:</strong></span>
                      </div>
                      <input type="text" class="form-control" id="s_fname" name="s_fname">
                    </div>
					<div class="input-group" style='margin-top:10px;'>
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Middlename:</strong></span>
                      </div>
                      <input type="text" class="form-control" id="s_mname" name="s_mname">
                    </div>
					<div class="input-group" style='margin-top:10px;'>
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Lastname:</strong></span>
                      </div>
                      <input type="text" class="form-control" id="s_lname" name="s_lname">
                    </div>
					<div class="input-group" style='margin-top:10px;'>
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Address:</strong></span>
                      </div>
                      <textarea style='resize:none' class='form-control' row='5' id='s_address' ></textarea>
                    </div>
					<div class="input-group" style='margin-top:10px;'>
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Contact #:</strong></span>
                      </div>
                      <input type="number" class="form-control" id="s_connum" name="s_connum">
                      <input type="hidden"  id="userID" name="userID">
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_sec" onclick="add_sec('add')"><span class="fa fa-check"></span> Save </button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
			</div>
		</div>
	</div>
</div>