<div class="modal fade" id="viewfu_result" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				 <h6 class="modal-title">Result of Follow-up Checkup</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
               
              </div>
			<div class="modal-body">
				<div class="container">
					<div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Diagnosis:</strong></span>
                      </div>
                      <textarea name="view_diagnosis_result" id="view_diagnosis_result" rows="4" class='form-control' readonly style='resize: none'></textarea>
                    </div>
                    <div class="input-group" style='margin-top:10px;'>
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Remarks:</strong></span>
                      </div>
                      <textarea name="view_remarks_result" id="view_remarks_result" rows="4" class='form-control' readonly style='resize: none'></textarea>
                    </div>
				</div>
			</div>
			<div class="modal-footer">
				<span class="btn-group">
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
			</div>
		</div>
	</div>
</div>