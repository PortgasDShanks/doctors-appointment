<style>
#card-shadow{
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.34), 0 9px 8px rgba(0, 0, 0, 0.32);
}
.animated {
  animation-duration: 2s;
  animation-fill-mode: both;
}
.dropdown-menu{
  position: absolute;
  transform: translate3d(-84px, 37px, 0px);
  top: 0px;
  left: 0px;
  will-change: transform;
  background-color: #b3b0b0d9;
}
.oval_used {
  height: 15px;
  width: 30px;
  background-color: rgba(0, 128, 0, 0.52);
  /* border-radius: 50%; */
}
.oval_expired{
  height: 15px;
  width: 30px;
  background-color: rgba(255, 17, 0, 0.55);
  /* border-radius: 50%; */
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#" style="font-size: 20px">Home</a></li>
          <li class="breadcrumb-item active" style="font-size: 20px">Blood Lists</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class='col-lg-12'>
        <div class="card">
            <div class="card-body">
              <div class='col-md-12'>
                  <div class="btn-group pull-right">
                    <button type="button" class="btn btn-default btn-sm "><span class='fa fa-plus-circle'></span> Add</button>
                    <button type="button" class="btn btn-default dropdown-toggle dropdown-hover dropdown-icon" data-toggle="dropdown">
                      <span class="sr-only">Toggle Dropdown</span>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="#" onclick="addDonor('new')"><span class='fa fa-plus-circle'></span>  New</a>
                        <a class="dropdown-item" href="#" onclick="addDonor('exist')"><span class='fa fa-plus-circle'></span> Existing</a>
                      </div>
                    </button>
                  </div>
              </div> 
              <div class='col-md-12' style='margin-top:50px;width:100%;overflow-y: auto;' >
                  <table id='blood_table' class="table table-bordered table-hover">
                      <thead style='background-color: #343940;color: white;'>
                          <tr>
                              <th></th>
                              <th></th>
                              <th>NAME</th>
                              <th>ADDRESS</th>
                              <th>CONTACT</th>
                              <th>BLOOD TYPE</th>
                              <th>TOTAL(BAGS)</th>
                          </tr>
                      </thead>
                      <tbody></tbody>
                  </table>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php require 'modals/add_blood_donor.php';
      require 'modals/addExisting_blood_donor.php';
      require 'modals/view_blood_lists.php';
?>
<script>
$(document).ready(function(){
  blood_lists();
});
function viewDetails(donor_name){
  $("#bloodLists").modal('show');
  blood_lists_detailed(donor_name);
}
function addDonor(cat){
  $("#cat").val(cat);
  switch(cat){
    case 'new':
      $("#adddonor").modal('show');
    break;
    case 'exist':
      $("#addExistdonor").modal('show');
    break;
    default:
    break;
  }
}
function checkDonor(){
  var name = $("#exist_donor_name").val();
  $.post("ajax/getDonorDetails.php", {
    name: name
  }, function(data){
    var a = JSON.parse(data);
    $("#existdonor_contact").val(a.contact);
    $("#exist_donor_address").val(a.address);
    $("#type_name").val(a.btypeName);
    $("#type_id").val(a.btypeid);
  });
}
function blood_type(){
  var donor = $("#exist_donor_name").val();
  $.post("ajax/getBloodTypes.php", {
    donor: donor
  }, function(data){
    $("#blood_type").html(data);
  });  
}
function add_bloodDonor(cat,type){
  var dName = $("#donor_name").val();
  var dAddress = $("#donor_address").val();
  var dContact = $("#donor_contact").val();
  var dType = $("#blood_type").val();
  var quantity = $("#blood_quantity").val();
  if(dName == '' || dAddress == '' || dContact == '' || dType == '' || quantity == ''){
    alertWarning();
  }else{
    $("#btn_add_b").prop("disabled", true);
    $("#btn_add_b").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    $.post("ajax/CRUD_blood.php", {
      dName: dName,
      dAddress: dAddress,
      dContact: dContact,
      dType: dType,
      quantity: quantity,
      cat: cat,
      type: type
    }, function(data){
      if(data > 0){
        swal({
            title: "All Good!",
            text: "Blood Donor was successfully added.",
            type: "success"
        }, function(){
            location.reload();
        }); 
      }else{
        failedAlert();
      }
    });
  }
}
function add_existbloodDonor(cat,type){
  var name = $("#exist_donor_name").val();
  var e_quantity = $("#exist_quantity").val();
  if(name == '' || e_quantity == ''){
    alertWarning();
  }else{
    $("#btn_add_ebd").prop("disabled", true);
    $("#btn_add_ebd").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
    $.post("ajax/CRUD_blood.php", {
      name: name,
      e_quantity: e_quantity,
      type: type,
      cat: cat
    }, function(data){
      if(data > 0){
        swal({
            title: "All Good!",
            text: "Blood Donor was successfully added.",
            type: "success"
        }, function(){
            location.reload();
        }); 
      }else{
        failedAlert();
      }
    });
  }
}
function quantityChecker(bags , bloodID){
  var quantity = $("#quantity"+bloodID).val();
  if(quantity > bags){
    swal({
        title: "Aw Snap!",
        text: "Exceed to the Current remaining bags.",
        type: "warning"
    }, function(){
      $("#quantity"+bloodID).val("");
    }); 
  }
}
function useBlood(dname , bloodID){
  var quantity = $("#quantity"+bloodID).val();
  var type = "use";
  if(quantity == ''){
    alertWarning();
  }else{
    $("#blood"+bloodID).prop("disabled", true);
    $("#blood"+bloodID).html("<span class='fa fa-spin fa-spinner'></span>");
    $.post("ajax/CRUD_blood.php", {
      bloodID: bloodID,
      quantity: quantity,
      type: type
    }, function(data){
      if(data > 0){
        swal({
            title: "All Good!",
            text: "Blood was successfully used.",
            type: "success"
        }, function(){
            blood_lists_detailed(dname);
            blood_lists();
        });
      }else{
        failedAlert();
      }
    });
  }
}
function blood_lists_detailed(donor_name){
  $("#blood_list_details").DataTable().destroy();
    $('#blood_list_details').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/blood_lists_detailed.php",
        "dataSrc":"data",
        "data":{
          donor_name: donor_name
        },
				"type": "POST"
    },
    "columns":[
        {
            "data":"buttonQ"
        },
        {
            "data":"count"
        },
        {
            "data":"date_added"
        },
        {
            "data":"bags"
        },
        {
            "data":"inputQ"
        }
        
    ],
    "createdRow": function( row, data, dataIndex ) {
				if ( data.is_expired_used == "e" ) {
					  $(row).css("background-color","rgba(255, 17, 0, 0.55)");
				}else if(data.is_expired_used == "u"){
            $(row).css("background-color","rgba(0, 128, 0, 0.52)");
        }else{
			    	$(row).css("background-color","");
			    }
			  }   
    });
}
function blood_lists(){
    $("#blood_table").DataTable().destroy();
    $('#blood_table').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/blood_lists.php",
        "dataSrc":"data"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"name"
        },
        {
            "data":"address"
        },
        {
            "data":"contact"
        },
        {
            "data":"blood_type"
        },
        {
            "data":"totalBags"
        }
        
    ]   
    });
}
</script>
