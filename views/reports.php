<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#" style="font-size: 20px">Home</a></li>
          <li class="breadcrumb-item active" style="font-size: 20px">Reports</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
      <div class="container-fluid">
        <div class="row">
            
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class='col-md-4'>
                      <div class="input-group">
                      <div class="input-group-prepend">
                          <span class="input-group-text"><strong>Start Date:</strong></span>
                      </div>
                      <input type="date" class="form-control" id="sdate" name="sdate">
                      </div>
                  </div>
                  <div class='col-md-4'>
                      <div class="input-group">
                      <div class="input-group-prepend">
                          <span class="input-group-text"><strong>End Date:</strong></span>
                      </div>
                      <input type="date" class="form-control" id="edate" name="edate">
                      </div>
                  </div>
                  <div class='col-md-4'>
                      <div class="btn-group">
                         <input type="hidden" class="form-control" id="userid" value="<?php echo $user_id ?>">
                        <button type='button' class='btn btn-primary' id='gen' onclick='genReport()'><span class='fa fa-refresh'></span> Generate </button>
                      </div>
                  </div>
                  <div class='col-md-12' style='margin-top:50px;width:100%;overflow-y: auto;' >
                      <table id='reports' class="table table-bordered table-hover">
                          <thead style='background-color: #343940;color: white;'>
                              <tr>
                                  <th>APPOINTMENT DATE</th>
                                  <th>PATIENT NAME</th>
                                  <th>DIAGNOSIS</th>
                                  <th>REMARKS</th>
                                  <th>ACTION</th>
                              </tr>
                          </thead>
                          <tbody></tbody>
                      </table>
                   </div>
                   </div>
                </div>
              </div>
            </div>
          </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
</div>
<?php require 'modals/modal_followcheckups.php'; ?>
<script>
  $(document).ready( function(){

  });
  function genReport(){
    var sdate = $("#sdate").val();
    var edate = $("#edate").val();
    var userid = $("#userid").val();
    if(sdate == '' || edate == ''){
      swal("Please Fill the dates");
    }else{
      reports(sdate, edate, userid);
    }
    
  }
  function viewFC(appointmnt){
    $("#followups").modal('show');
    var sdate = $("#sdate").val();
    var edate = $("#edate").val();
    var userid = $("#userid").val();
    reports_followup(appointmnt, sdate, edate, userid)
  }
   function reports_followup(appointmnt, sdate,edate,userid){
  $("#reports_followup").DataTable().destroy();
    $('#reports_followup').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/appointment_followup_reports.php",
        "dataSrc":"data",
        "data":{
          appointmnt: appointmnt,
          sdate: sdate,
          edate: edate,
          userid: userid
        },
        "type": "POST"
    },
    "columns":[
        {
            "data":"appDate"
        },
        {
            "data":"name"
        },
        {
            "data":"diagnosis"
        },
        {
            "data":"remarks"
        }
        
    ]
    });
}
 function reports(sdate,edate,userid){
  $("#reports").DataTable().destroy();
    $('#reports').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/appointment_reports.php",
        "dataSrc":"data",
        "data":{
          sdate: sdate,
          edate: edate,
          userid: userid
        },
        "type": "POST"
    },
    "columns":[
        {
            "data":"appDate"
        },
        {
            "data":"name"
        },
        {
            "data":"diagnosis"
        },
        {
            "data":"remarks"
        },
        {
            "data":"action"
        }
        
    ]
    });
}
</script>
