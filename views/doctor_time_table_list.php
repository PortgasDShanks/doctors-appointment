<?php 
// switch($accessType){
//   case 'S':
//     $getDoc_sec = mysql_fetch_array(mysql_query("SELECT * FROM tbl_users WHERE user_id = '$user_id'"));
//     $sessionID = $getDoc_sec['doctor_id'];
//   break;
//   case 'D' || 'P':
//     $sessionID = $user_id;
// }
$array = array("Sunday" => 0, "Monday" => 1,"Tuesday" => 2, "Wednesday" => 3, "Thursday" => 4, "Friday" => 5, "Saturday" => 6);
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#" style="font-size: 20px">Home</a></li>
          <li class="breadcrumb-item active" style="font-size: 20px">Time Table List</li>
        </ol>
      </div>
    </div>
  </div><br /><b>
</div>
<!-- Main content -->
<div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <input type='hidden' value='<?php echo $_SESSION['user_id']?>' id='userID'>
            <div class="card card-inverse card-outline">
              <div class="card-body">
                <div class='col-md-12'>
                      <button class='btn btn-sm btn-default pull-right' id="" onclick="modal_addtt()"><span class='fa fa-plus-circle'></span> Add</button>
                </div>
                <div class='col-md-6' style='margin-top:50px;'>
                  <div class="input-group" >
                    <div class="input-group-prepend">
                      <span class="input-group-text"><strong>Day:</strong></span>
                    </div>
                     <select class='form-control' style='width:50%' id="day" onchange="getTimePerDay()">
                          <?php
                            foreach ($array as $day => $num) {
                              echo "<option value='$num'>$day</option>";
                            }
                          ?>
                      </select>
                  </div>
                </div>
                <div class='col-md-12' style="margin-top: 4%;overflow-y: auto;">
                    <table id='time_table' class="table table-bordered table-hover">
                        <thead style='background-color: #343940;color: white;'>
                            <tr>
                                <th>#</th>
                                <th>ACTION</th>
                                <th style='width: 55%;'>TIME</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>  
                    </table>
                </div>
              
              </div>
            </div><!-- /.card -->

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <?php   require 'modals/add_time_table.php';
        require 'modals/edit_time_table.php';
    ?>
<script>
$(document).ready(function(){
    getTimePerDay();
    // $("#timepicker1").timepicker({
    //     timeFormat: 'h:mm p',
    //     interval: 60,
    //     minTime: '10',
    //     maxTime: '6:00pm',
    //     defaultTime: '11',
    //     startTime: '10:00',
    //     dynamic: false,
    //     dropdown: true,
    //     scrollbar: true
    // });
});
function  getTimePerDay(){
  var day = $("#day").val();
  category_data(day);
}
function deleteDoctorTime(timeID){
  var userid = $("#userID").val();
  var type = "delete";
  var day = $("#day").val();
    $("#deletetime"+timeID).prop("disabled", true);
    $("#deletetime"+timeID).html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    $.post("ajax/CRUD_timeTable.php", {
      timeID: timeID,
      type: type,
      userid: userid
    }, function(data){
      if(data > 0){
        swal({
            title: "All Good!",
            text: "Time table was successfully deleted.",
            type: "success"
        }, function(){
            category_data(day);
        }); 
      }else{
        failedAlert();
      }
    });
}
function modal_addtt(){
  $("#addtt").modal('show');
}
function updateDoctorTime(timeID){
  var tFrom = $("#timeFrom"+timeID).val();
  var tTo = $("#timeTo"+timeID).val();
  var userid = $("#userID").val();
  var day = $("#day").val();
  var type = "edit";
  $("#updatetime"+timeID).prop('disabled', true);
  $("#updatetime"+timeID).html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
  $.post("ajax/CRUD_timeTable.php", {
    tFrom: tFrom,
    tTo: tTo,
    userid: userid,
    type: type,
    timeID: timeID,
    day: day
  }, function(data){
    if(data == 1){
      swal({
            title: "All Good!",
            text: "Time table was successfully changed.",
            type: "success"
        }, function(){
            category_data(day);
        });
    }else{
      failedAlert();
    }
    $("#updatetime"+timeID).prop('disabled', false);
    $("#updatetime"+timeID).html("<span class='fa fa-check'></span> Continue ");
  });
}
function viewRecord(tID){
  var userID = $("#userID").val();
  $("#edittt").modal('show');
  $.post("ajax/getTimeTable.php", {
    userID: userID,
    tID: tID
  }, function(data){
    var tt = JSON.parse(data);
    $("#timepicker1_e").val(tt.time_from);
    $("#timepicker2_e").val(tt.time_to);
    $("#timeID").val(tt.time_id);
  });
}
function add_tt(type){
  var userid = $("#userID").val();
  var day = $("#time_day").val();
  var t_from = $("#timepicker1").val();
  var t_to = $("#timepicker2").val();
  $.post("ajax/CRUD_timeTable.php", {
    userid: userid,
    day: day,
    t_from: t_from,
    t_to: t_to,
    type: type
  }, function(data){
    if(data == 1){
      $("#addtt").modal('hide');
        swal({
            title: "All Good!",
            text: "Time table was successfully added.",
            type: "success"
        }, function(){
            category_data(day);
            
        });
    }else{
        failedAlert();
    }
    $("#timepicker1").val("");
    $("#timepicker2").val("");
  });
}
function category_data(day){
    var userID = $("#userID").val();
  $("#time_table").DataTable().destroy();
  $('#time_table').dataTable({
  "processing":true,
  "ajax":{
      "url":"ajax/datatables/time_table_data.php",
      "dataSrc":"data",
    "data":{
        userID: userID,
        day:day
    },
    "type": "POST"
  },
  "columns":[
      {
          "data":"count"
      },
      {
          "data":"action"
      },
      {
          "data":"time_table"
      }
      
  ]   
  });
}
</script>