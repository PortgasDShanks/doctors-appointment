<style>
#card-shadow{
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.34), 0 9px 8px rgba(0, 0, 0, 0.32);
}
.animated {
  animation-duration: 2s;
  animation-fill-mode: both;
}
</style>
<?php 
// switch($accessType){
//   case 'S':
//     $getDoc_sec = mysql_fetch_array(mysql_query("SELECT * FROM tbl_users WHERE user_id = '$user_id'"));
//     $sessionID = $getDoc_sec['doctor_id'];
//   break;
//   case 'D' || 'P':
//     $sessionID = $user_id;
// }
// $curDate = date("Y-m-d", strtotime(getCurrentDate()));
// $queu = mysql_query("SELECT a.appointment_id as app_id, a.appointment_date as appDate, '0' as fu_app_id, a.appointment_id as main_id,  a.patient_id as patient, a.reschedule_date as resched, a.appointment_time_id as timeID, a.`status` as stat,'main_check' as cat FROM tbl_appointments as a , tbl_queuing as q  WHERE a.appointment_id = q.appointment_id AND doctor_id = '$sessionID' AND (a.appointment_date = '$curDate' OR reschedule_date = '$curDate')
// UNION
// SELECT fa.appointment_main_id as app_id, fa.appointment_date as appDate,  fa.fu_appointment_id as fu_app_id,  '0' as main_id,fa.patient_id as patient, fa.reschedule_date as reschedule,  fa.appointment_time_id as timeID,fa.status as stat, 'followup_check' as cat  FROM tbl_followup_appointments as fa , tbl_appointments as a , tbl_queuing as q WHERE fa.fu_appointment_id = q.appointment_id AND fa.doctor_id = '$sessionID'  AND fa.appointment_main_id = a.appointment_id AND (fa.appointment_date = '$curDate' OR fa.reschedule_date = '$curDate')");
// $count = mysql_num_rows($queu);
// $fetch = mysql_fetch_array($queu);
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#" style="font-size: 20px">Home</a></li>
          <li class="breadcrumb-item active" style="font-size: 20px">Dashboard</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <?php if($accessType == 'patient'){ ?>
            <div class='col-lg-12'>
              <div class='card'>
                <div class='card-body'>
                   <div id="calendar"></div>
                </div>
              </div>
            </div>
          <?php }else if($accessType == 'admin'){ ?>
            <div class='col-lg-12'>
              <div class='card'>
                <div class='card-body'>
                  <div class='row'>
                    <div class="col-lg-4 col-6">
                      <!-- small box -->
                      <div class="small-box bg-info">
                        <div class="inner">
                          <h3><?=GETALL_CLINICS_STATUS(0)?></h3>

                          <p>Pending Requests</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-paper-plane"></i>
                        </div>
                        <a href="index.php?access=clinics"  class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <div class="col-lg-4 col-6">
                      <!-- small box -->
                      <div class="small-box bg-success">
                        <div class="inner">
                          <h3><?=GETALL_CLINICS_STATUS(1)?></h3>

                          <p>Active Clinics</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-paper-plane"></i>
                        </div>
                        <a href="index.php?access=clinics"  class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <div class="col-lg-4 col-6">
                      <!-- small box -->
                      <div class="small-box bg-warning">
                        <div class="inner">
                          <h3><?=GETALL_CLINICS_STATUS(3)?></h3>

                          <p>Inactive Clinics</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-paper-plane"></i>
                        </div>
                        <a href="index.php?access=clinics"  class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php }else { ?>
            <div class="col-lg-12">
              <div class="card">
                <div class="card-body">
                  <div class='col-md-12 '>
                    <div class="card" id='card-shadow'>
                      <div class="card-body">
                      <div class='row'>
                        <div class="col-lg-4 col-6">
                          <!-- small box -->
                          <div class="small-box bg-info">
                            <div class="inner">
                              <h3><?=GETALL_PENDING_STATUS($user_id)?></h3>

                              <p>Pending Appointments</p>
                            </div>
                            <div class="icon">
                              <i class="fa fa-paper-plane"></i>
                            </div>
                            <a href="index.php?access=patient-appointments" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                          </div>
                        </div>
                        <div class="col-lg-4 col-6">
                          <!-- small box -->
                          <div class="small-box bg-success">
                            <div class="inner">
                              <h3><?=GETALL_TODAYS_APPOINTMENT($user_id)?></h3>

                              <p>Today's Appointment</p>
                            </div>
                            <div class="icon">
                              <i class="fa fa-paper-plane"></i>
                            </div>
                            <a href="index.php?access=patient-appointments" onclick="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                          </div>
                        </div>
                        <div class="col-lg-4 col-6">
                          <!-- small box -->
                          <div class="small-box bg-warning">
                            <div class="inner">
                              <h3><?=getAllpatientsAllTime($user_id)?></h3>

                              <p>Total Number of Patients (All Time)</p>
                            </div>
                            <div class="icon">
                              <i class="fa fa-paper-plane"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                          </div>
                        </div>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <?php require 'modals/modal_view_event.php';?>
    <script>
    // function gotoCLinic(){
    //   window.location = 'index.php?access=doctors';
    // }
    <?php 
            $query_getEvents = mysql_query("SELECT 
                                appointment_id as `id`,
                                CONCAT(firstname,' ',lastname) as title, 
                                'main_check' as cat, 
                                appointment_date as 
                                s_date, appointment_date as e_date, 
                                'm_date' as date_type, 
                                t.time_from as s_time, 
                                t.time_to as e_time 
                                FROM tbl_appointments as a,tbl_users as u, tbl_doctor_time_table as t 
                                WHERE t.time_id = a.appointment_time_id 
                                AND a.patient_id = u.user_id 
                                AND a.patient_id = '$user_id'
                                AND reschedule_date = '0000-00-00'
                                UNION
                                SELECT 
                                fu_appointment_id as `id`, 
                                CONCAT(firstname,' ',lastname) as title, 
                                'fu_check' as cat, 
                                appointment_date as s_date, 
                                appointment_date as e_date,  
                                'm_date' as date_type,
                                t.time_from as s_time, 
                                t.time_to as e_time 
                                FROM tbl_followup_appointments as a,tbl_users as u, tbl_doctor_time_table as t 
                                WHERE t.time_id = a.appointment_time_id 
                                AND a.patient_id = u.user_id 
                                AND a.patient_id = '$user_id'
                                AND reschedule_date = '0000-00-00'
                                UNION
                                SELECT 
                                appointment_id as `id`, 
                                CONCAT(firstname,' ',lastname) as title, 
                                'main_check' as cat, 
                                reschedule_date as s_date, 
                                reschedule_date as e_date,  
                                're_date' as date_type, 
                                t.time_from as s_time, 
                                t.time_to as e_time 
                                FROM tbl_appointments as a,tbl_users as u, tbl_doctor_time_table as t 
                                WHERE t.time_id = a.appointment_time_id 
                                AND a.patient_id = u.user_id 
                                AND a.patient_id = '$user_id' 
                                AND reschedule_date != '0000-00-00'
                                UNION
                                SELECT 
                                fu_appointment_id as `id`, 
                                CONCAT(firstname,' ',lastname) as title, 
                                'fu_check' as cat, 
                                reschedule_date as s_date, 
                                reschedule_date as e_date,  
                                're_date' as date_type, 
                                t.time_from as s_time, 
                                t.time_to as e_time 
                                FROM tbl_followup_appointments as a,tbl_users as u, tbl_doctor_time_table as t 
                                WHERE t.time_id = a.appointment_time_id 
                                AND a.patient_id = u.user_id 
                                AND a.patient_id = '$user_id' 
                                AND reschedule_date != '0000-00-00'");
            $count_events = mysql_num_rows($query_getEvents);
      ?>
   
       $(document).ready( function(){
        calendarEvents();
        });

        function calendarEvents(){
    $('#calendar').fullCalendar({
      
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
      },
      
      defaultDate: '<?php echo date('Y-m-d'); ?>',
      editable: true,
      eventLimit: true,
      droppable: true,
      events: [
        <?php 
          
        $ctrEvent=1;
        while($fetch_events = mysql_fetch_array($query_getEvents)){
          $event_title = $fetch_events['title'];
          $start_date = $fetch_events['s_date'];
          $end_date = date("Y-m-d", strtotime('+1 day', strtotime($fetch_events['e_date'])));
          $event_time = date("g:ia", strtotime($fetch_events['s_time']));
          $e_time = date("g:ia", strtotime($fetch_events['e_time']));
          $datetype = ($fetch_events['date_type'] == 'm_date')?"NO":"YES";
        ?>
        {
          title: '<?php echo $event_title." "."-"." ".$event_time; ?>',
          start: '<?php echo $start_date; ?>',
          end: '<?php echo $end_date?>',
          color: '#3498db',
          id: '<?php echo $fetch_events['id']; ?>',
          cat: '<?php echo $fetch_events['cat']; ?>',
          event_date: '<?php echo date("F d, Y", strtotime($fetch_events['s_date']))." - ".date("F d,Y", strtotime($fetch_events['e_date'])); ?>',
          event_time: '<?php echo $fetch_events['s_time']; ?>',
          event_title: '<?php echo $fetch_events['title']; ?>',
          appointment_time: '<?php echo date("h:i A", strtotime($fetch_events['s_time'])).' - '.date("h:i A", strtotime($fetch_events['e_time']))?>',
          appointment_date: '<?php echo date("F d, Y", strtotime($start_date)) ?>',
          date_type: '<?php echo $datetype; ?>'
        }<?php 
          if($ctrEvent < $count_events){ 
            echo ",";
          }
          $ctrEvent++;
        }
        ?>
        
      ],
      eventClick: function(callEvent , jsEvent, view){
        $("#viewEvent").modal();

        $("#eventNameV").val(callEvent.event_title);
        $("#eventDateV").val(callEvent.event_date);
        $("#event_timeV").val(callEvent.event_time);

      },
            eventConstraint: {
                start: moment().format('YYYY-MM-DD'),
                end: '2100-01-01'
            }
    });
  }

    </script>
