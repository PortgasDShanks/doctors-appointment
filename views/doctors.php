<style>
  .card-primary:not(.card-outline) .card-header, .card-primary:not(.card-outline) .card-header a {
    color: #000000;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#" style="font-size: 20px"> Home </a></li>
          <li class="breadcrumb-item active" style="font-size: 20px">Clinics</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-inverse card-outline">
              <div class="card-body">
              <div class="col-12 col-sm-12">
                <div class="card card-primary card-tabs">
                  <div class="card-header p-0 pt-1" style='background-image: linear-gradient(-20deg, #2b5876 0%, #4e4376 100%) !important;'>
                    <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                      <li class="pt-2 px-3"><h3 class="card-title" style='color:white'></h3></li>
                      <li class="nav-item">
                        <a class="nav-link active" id="custom-tabs-two-home-tab" data-toggle="pill" href="#custom-tabs-two-home" role="tab" aria-controls="custom-tabs-two-home" aria-selected="false">Pending Requests</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-two-profile-tab" data-toggle="pill" href="#custom-tabs-two-profile" role="tab" aria-controls="custom-tabs-two-profile" aria-selected="false">Active Clinics</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " id="custom-tabs-two-messages-tab" data-toggle="pill" href="#custom-tabs-two-messages" role="tab" aria-controls="custom-tabs-two-messages" aria-selected="true">Cancelled Requests</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-two-settings-tab" data-toggle="pill" href="#custom-tabs-two-settings" role="tab" aria-controls="custom-tabs-two-settings" aria-selected="false">Inactive Clinics</a>
                      </li>
                    </ul>
                  </div>
                  <div class="card-body">
                    <div class="tab-content" id="custom-tabs-two-tabContent">
                      <div class="tab-pane fade active show" id="custom-tabs-two-home" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
                          <table class="mb-0 table table-hover" id='pending' style='margin-top:20px;'>
                              <thead>
                              <tr>
                                  <th>#</th>
                                  <th>Clinic Name</th>
                                  <th>Doctor Name</th>
                                  <th>Email Address</th>
                                  <th>Action</th>
                              </tr>
                              </thead>
                              <tbody>
                              </tbody>
                          </table>
                      </div>
                      <div class="tab-pane fade" id="custom-tabs-two-profile" role="tabpanel" aria-labelledby="custom-tabs-two-profile-tab">
                          <table class="mb-0 table table-hover" id='approved' style='margin-top:20px;width:100%'>
                              <thead>
                              <tr>
                                  <th>#</th>
                                  <th>Clinic Name</th>
                                  <th>Doctor Name</th>
                                  <th>Email Address</th>
                                  <th>Action</th>
                              </tr>
                              </thead>
                              <tbody>
                              </tbody>
                          </table>
                      </div>
                      <div class="tab-pane fade " id="custom-tabs-two-messages" role="tabpanel" aria-labelledby="custom-tabs-two-messages-tab">
                          <table class="mb-0 table table-hover" id='cancelled' style='margin-top:20px;width:100%'>
                              <thead>
                              <tr>
                                  <th>#</th>
                                  <th>Clinic Name</th>
                                  <th>Doctor Name</th>
                                  <th>Email Address</th>
                              </tr>
                              </thead>
                              <tbody>
                              </tbody>
                          </table>
                      </div>
                      <div class="tab-pane fade" id="custom-tabs-two-settings" role="tabpanel" aria-labelledby="custom-tabs-two-settings-tab">
                          <table class="mb-0 table table-hover" id='inactive' style='margin-top:20px;width:100%'>
                              <thead>
                              <tr>
                                  <th>#</th>
                                  <th>Clinic Name</th>
                                  <th>Doctor Name</th>
                                  <th>Email Address</th>
                                  <th>Action</th>
                              </tr>
                              </thead>
                              <tbody>
                              </tbody>
                          </table>
                      </div>
                    </div>
                  </div>
                  <!-- /.card -->
                </div>
              </div>
              
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<script>
$(document).ready(function(){
  pending_requests();
  approved_requests();
  inactive_requests();
  cancelled_requests();
});

function request_response(response,id){
    $.post("ajax/request_response.php", {
        response: response,
        id: id
    }, function(data){
        alert("Clinic Updated Successfully");

        pending_requests();
        approved_requests();
        inactive_requests();
        cancelled_requests();
    });
}
function cancelled_requests(){
  $("#cancelled").DataTable().destroy();
  $('#cancelled').dataTable({
  "processing":true,
  "ajax":{
      "url":"ajax/datatables/cancelled_request_list.php",
      "dataSrc":"data"
  },
  "columns":[
      {
          "data":"count"
      },
      {
          "data":"name"
      },
      {
          "data":"desc"
      },
      {
          "data":"email"
      }
      
  ]   
  });

}
function inactive_requests(){
  $("#inactive").DataTable().destroy();
  $('#inactive').dataTable({
  "processing":true,
  "ajax":{
      "url":"ajax/datatables/inactive_request_list.php",
      "dataSrc":"data"
  },
  "columns":[
      {
          "data":"count"
      },
      {
          "data":"name"
      },
      {
          "data":"desc"
      },
      {
          "data":"email"
      },
      {
          "data":"action"
      }
      
  ]   
  });

}
function approved_requests(){
  $("#approved").DataTable().destroy();
  $('#approved').dataTable({
  "processing":true,
  "ajax":{
      "url":"ajax/datatables/approved_request_list.php",
      "dataSrc":"data"
  },
  "columns":[
      {
          "data":"count"
      },
      {
          "data":"name"
      },
      {
          "data":"desc"
      },
      {
          "data":"email"
      },
      {
          "data":"action"
      }
      
  ]   
  });

}
function pending_requests(){
  $("#pending").DataTable().destroy();
  $('#pending').dataTable({
  "processing":true,
  "ajax":{
      "url":"ajax/datatables/pending_request_list.php",
      "dataSrc":"data"
  },
  "columns":[
      {
          "data":"count"
      },
      {
          "data":"name"
      },
      {
          "data":"desc"
      },
      {
          "data":"email"
      },
      {
          "data":"action"
      }
      
  ]   
  });

}
</script>