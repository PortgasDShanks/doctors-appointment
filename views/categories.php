<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#" style="font-size: 20px">Home</a></li>
          <li class="breadcrumb-item active">Specialization</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">

            <div class="card card-inverse card-outline">
                <div class="card-body">
                    <div class='col-md-12'>
                        <button class='btn btn-sm btn-default pull-right' id="btn_del_cat" onclick="deleteCat('delete')"><span class='fa fa-trash'></span> Delete</button>
                        <button class='btn btn-sm btn-default pull-right' id="" onclick="modal_addcat()"><span class='fa fa-plus-circle'></span> Add</button>
                    </div>
                    <div class='col-md-12' style="margin-top: 4%;overflow-y: auto;">
                        <table id='cat_table' class="table table-bordered table-hover">
                            <thead style='background-color: #343940;color: white;'>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th>SPECIALIZATION</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                 </div>
            </div><!-- /.card -->

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<?php   require 'modals/add_category.php';
        require 'modals/edit_category.php';
    ?>
<script>
$(document).ready( function(){
    category_data();
});
function modal_addcat(){
    $("#addCat").modal('show');
}
function viewRecord(id){
    
    $("#editCat").modal('show');
    $.post("ajax/getCategory.php", {
        id: id
    }, function(data){
        var cat = JSON.parse(data);
        $("#Cname_edit").val(cat.category);
        $("#Cid").val(cat.category_id);
        $("#Caction").val("edit");
    });
}
function deleteCat(act){
    var count_cat_checked = $('input[name="checkbox_cat"]:checked').map(function() {
									return this.value;
							 	}).get();
    cId = [];
    $("#btn_del_cat").prop("disabled", true);
    $("#btn_del_cat").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    $.post("ajax/CRUD_category.php", {
        cId: count_cat_checked,
        act: act
    }, function(data){
        if(data > 0){
            swal({
                title: "All Good!",
                text: "Category was successfully deleted.",
                type: "success"
            }, function(){
                category_data();
            }); 
        }else{
            failedAlert();
        }
        $("#btn_del_cat").prop("disabled", false);
        $("#btn_del_cat").html("<span class='fa fa-trash'></span> Delete");
    });
}
function editCat(){
    var cname = $("#Cname_edit").val();
    var cId = $("#Cid").val();
    var act = $("#Caction").val();
    $("#btn_edit_cat").prop("disabled", true);
    $("#btn_edit_cat").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
    $.post("ajax/CRUD_category.php", {
        cname: cname,
        cId: cId,
        act: act
    }, function(data){
        if(data > 0){
            swal({
                title: "All Good!",
                text: "Category was successfully updated.",
                type: "success"
            }, function(){
                category_data();
            });
        }else{
            failedAlert();
        }
        $("#editCat").modal('hide');
        $("#btn_edit_cat").prop("disabled", false);
        $("#btn_edit_cat").html("<span class='fa fa-check'></span> Continue");
    });
}
function add_category(act){
    var catname = $("#Cname").val();
    
    if(catname == ''){
        alertWarning()
    }else{
        $("#btn_add_cat").prop("disabled", true);
        $("#btn_add_cat").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
        $.post("ajax/CRUD_category.php", {
            catname: catname,
            act: act
        }, function(data){
            if(data == 1){
                swal({
                    title: "All Good!",
                    text: "Category was successfully inserted.",
                    type: "success"
                }, function(){
                    category_data();
                });
            }else{
                failedAlert();
            }
            $("#Cname").val("");
            $("#addCat").modal('hide');
            $("#btn_add_cat").prop("disabled", false);
            $("#btn_add_cat").html("<span class='fa fa-check'></span> Continue");
        });
    }
}
function category_data(){
    $("#cat_table").DataTable().destroy();
    $('#cat_table').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/category_list.php",
        "dataSrc":"data"
    },
    "columns":[
        {
            "mRender": function(data,type,row){
            return "<input type='checkbox' name='checkbox_cat' value='"+row.id+"'>";    
            
        }
        },
        {
            "data":"action"
        },
        {
            "data":"Cname"
        }
        
    ]   
    });
}
</script>