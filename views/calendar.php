<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#" style="font-size: 20px">Home</a></li>
          <li class="breadcrumb-item active" style="font-size: 20px">Calendar</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<input type='hidden' id='useriD' value='<?php echo $user_id; ?>'>
<div class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-md-3">
          
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Add Manually</h4>
              </div>
              <div class="card-body">
                <div class="btn-group col-md-12">
                    <button id="add-new-event" type="button" data-target="#addEvent" data-toggle="modal" class="btn btn-primary btn-block btn-sm"><span class="fa fa-plus-circle"></span> Add</button>
                  </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Events Trash</h4>
              </div>
              <div class="card-body" id="calendarTrash">
                <div>
                <center><span class="fa fa-trash-o" style="font-size: 5em;"></span></center>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-9">
            <div class="card">
              <div class="card-body">
                <div id="calendar"></div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<?php 
require 'modals/modal_add_event.php';
require 'modals/modal_view_event.php';
require 'modals/resched_modal.php';
$query_getEvents = mysql_query("SELECT 
                                event_id as `id`, 
                                event_name as title, 
                                'events' as cat, 
                                event_start_date as s_date,      
                                event_end_date as e_date, 
                                '' as date_type, 
                                event_time as s_time, 
                                '' as e_time  
                                FROM tbl_events
                                UNION
                                SELECT 
                                appointment_id as `id`,
                                CONCAT(firstname,' ',lastname) as title, 
                                'main_check' as cat, 
                                appointment_date as 
                                s_date, appointment_date as e_date, 
                                'm_date' as date_type, 
                                t.time_from as s_time, 
                                t.time_to as e_time 
                                FROM tbl_appointments as a,tbl_users as u, tbl_doctor_time_table as t 
                                WHERE t.time_id = a.appointment_time_id 
                                AND a.patient_id = u.user_id 
                                AND a.doctor_id = '$user_id'
                                AND reschedule_date = '0000-00-00'
                                UNION
                                SELECT 
                                fu_appointment_id as `id`, 
                                CONCAT(firstname,' ',lastname) as title, 
                                'fu_check' as cat, 
                                appointment_date as s_date, 
                                appointment_date as e_date,  
                                'm_date' as date_type,
                                t.time_from as s_time, 
                                t.time_to as e_time 
                                FROM tbl_followup_appointments as a,tbl_users as u, tbl_doctor_time_table as t 
                                WHERE t.time_id = a.appointment_time_id 
                                AND a.patient_id = u.user_id 
                                AND a.doctor_id = '$user_id'
                                AND reschedule_date = '0000-00-00'
                                UNION
                                SELECT 
                                appointment_id as `id`, 
                                CONCAT(firstname,' ',lastname) as title, 
                                'main_check' as cat, 
                                reschedule_date as s_date, 
                                reschedule_date as e_date,  
                                're_date' as date_type, 
                                t.time_from as s_time, 
                                t.time_to as e_time 
                                FROM tbl_appointments as a,tbl_users as u, tbl_doctor_time_table as t 
                                WHERE t.time_id = a.appointment_time_id 
                                AND a.patient_id = u.user_id 
                                AND a.doctor_id = '$user_id' 
                                AND reschedule_date != '0000-00-00'
                                UNION
                                SELECT 
                                fu_appointment_id as `id`, 
                                CONCAT(firstname,' ',lastname) as title, 
                                'fu_check' as cat, 
                                reschedule_date as s_date, 
                                reschedule_date as e_date,  
                                're_date' as date_type, 
                                t.time_from as s_time, 
                                t.time_to as e_time 
                                FROM tbl_followup_appointments as a,tbl_users as u, tbl_doctor_time_table as t 
                                WHERE t.time_id = a.appointment_time_id 
                                AND a.patient_id = u.user_id 
                                AND a.doctor_id = '$user_id' 
                                AND reschedule_date != '0000-00-00'
                                "); 
$count_events = mysql_num_rows($query_getEvents);
?>
<script>
  // function addDraggable(){
  //   var draggable_event = $("#DraggableEvent").val();
  //   var userID = $("#userID").val();
  //   $.post("ajax/addDraggableEvent.php", {
  //     draggable_event:draggable_event,
  //     userID: userID
  //   }, function(data){
  //     if(data == 1){
  //       insertAlert();
  //     }else{
  //       failedAlert();
  //     }
  //   });
  // }
  function addEvents(){
    var userID = $("#useriD").val();
    var daterange = $("#daterange").val();
    var eventTime = $("#addEvent_time").val();
    var type = "add";
    var eventName = $("#event_title").val();
    $("#btn_add_event").prop("disabled", true);
    $("#btn_add_event").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
    $.post("ajax/CRUD_events.php",{
      userID: userID,
      daterange: daterange,
      eventTime: eventTime,
      type: type,
      eventName: eventName
    }, function(data){
      if(data > 0){
        swal({
            title: "All Good!",
            text: "Event was successfully deleted.",
            type: "success"
        }, function(){
            location.reload();
        }); 
      }else{
        failedAlert();
      }
    });
  }
  function updateResizedEvent(new_date, event_id){
    var userID = $("#useriD").val();
    var type = "update_resize";
    $.post("ajax/CRUD_events.php", {
      new_date: new_date,
      event_id: event_id,
      userID: userID,
      type: type
    }, function(data){
      if(data > 0){
        swal({
            title: "All Good!",
            text: "Event was successfully changed.",
            type: "success"
        }, function(){
            location.reload();
        }); 
      }else{
        failedAlert();
      }
    });
  }
  function updateDroppedEvent(new_start, new_end, event_id){
    var userID = $("#useriD").val();
    var type = "update_drop";
    $.post("ajax/CRUD_events.php", {
      userID: userID,
      type: type,
      new_start: new_start,
      new_end: new_end,
      event_id: event_id
    }, function(data){
      if(data > 0){
        swal({
            title: "All Good!",
            text: "Event was successfully changed.",
            type: "success"
        }, function(){
            location.reload();
        }); 
      }else{
        failedAlert();
      }
    });
  }
  function resched(){
        var appoint = $("#appID").val();
        var doctor = $("#useriD").val();
        var resched_date = $("#resched_date").val();
        $.post("ajax/getTime_followup.php",{
          docID: doctor,
          appointmentDate: resched_date
        },function(data){
         // alert(data);
          $("#time_cont").html(data);
        });
      }
  function appointmentResched(){
    var appoint = $("#appID").val();
    var resched_date = $("#resched_date").val();
    var type = 'resched';
    var cat = $("#categ").val();
    $("#btn_resched").prop("disabled", true);
    $("#btn_resched").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
    $.post("ajax/CRUD_appointment.php",{
      appoint: appoint,
      resched_date: resched_date,
      type: type,
      cat: cat
    }, function(data){
      if(data > 0){
        swal({
            title: "All Good!",
            text: "Successfully Rescheduled Patient Appointment.",
            type: "success"
        }, function(){
          location.reload();
        });
      }else{
        failedAlert();
      }
    });
  }
  function deleteEvent(event_id){
    var userID = $("#useriD").val();
    var type = "delete";
    $.post("ajax/CRUD_events.php",{
      userID: userID,
      type: type,
      event_id: event_id
    }, function(data){
      if(data > 0){
        swal({
            title: "All Good!",
            text: "Event was successfully deleted.",
            type: "success"
        }, function(){
            location.reload();
        }); 
      }else{
        failedAlert();
      }
    });
  }
  $(document).ready( function(){
    calendarEvents();
  });
  function calendarEvents(){
    $('#calendar').fullCalendar({
      
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
      },
      
      defaultDate: '<?php echo date('Y-m-d'); ?>',
      editable: true,
      eventLimit: true,
      droppable: true,
      events: [
        <?php 
          
        $ctrEvent=1;
        while($fetch_events = mysql_fetch_array($query_getEvents)){
          $event_title = $fetch_events['title'];
          $start_date = $fetch_events['s_date'];
          $end_date = date("Y-m-d", strtotime('+1 day', strtotime($fetch_events['e_date'])));
          $event_time = date("g:ia", strtotime($fetch_events['s_time']));
          $e_time = date("g:ia", strtotime($fetch_events['e_time']));
          $datetype = ($fetch_events['date_type'] == 'm_date')?"NO":"YES";
        ?>
        {
          title: '<?php echo $event_title." "."-"." ".$event_time; ?>',
          start: '<?php echo $start_date; ?>',
          end: '<?php echo $end_date?>',
          color: '#3498db',
          id: '<?php echo $fetch_events['id']; ?>',
          cat: '<?php echo $fetch_events['cat']; ?>',
          event_date: '<?php echo date("F d, Y", strtotime($fetch_events['s_date']))." - ".date("F d,Y", strtotime($fetch_events['e_date'])); ?>',
          event_time: '<?php echo $fetch_events['s_time']; ?>',
          event_title: '<?php echo $fetch_events['title']; ?>',
          appointment_time: '<?php echo date("h:i A", strtotime($fetch_events['s_time'])).' - '.date("h:i A", strtotime($fetch_events['e_time']))?>',
          appointment_date: '<?php echo date("F d, Y", strtotime($start_date)) ?>',
          date_type: '<?php echo $datetype; ?>'
        }<?php 
          if($ctrEvent < $count_events){ 
            echo ",";
          }
          $ctrEvent++;
        }
        ?>
        
      ],
      eventDragStop: function(event,jsEvent) {
        var event_id = event.id;
        var cat = event.cat
        if(cat == 'main_check' || cat == 'fu_check'){
          swal({
            title: "Aw Snap!",
            text: "Action Not Applied, This is a Patient Appointment",
            type: "error"
          }, function(){
              location.reload();
          }); 
        }else{
          var trashEl = jQuery('#calendarTrash');
          var ofs = trashEl.offset();

          var x1 = ofs.left;
          var x2 = ofs.left + trashEl.outerWidth(true);
          var y1 = ofs.top;
          var y2 = ofs.top + trashEl.outerHeight(true);

          if (jsEvent.pageX >= x1 && jsEvent.pageX <= x2 &&
              jsEvent.pageY >= y1 && jsEvent.pageY <= y2) {
              swal({
                title: "Are you sure?",
                text: "Once you drag it here , This event will be deleted!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Delete!",
                cancelButtonText: "Cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
              },
              function(isConfirm) {
                if (isConfirm) {
                  deleteEvent(event_id);
                } else {
                  swal("Aw Snap!", "Event Delete Cancel", "error");
                }
              });
          }
        }
      },
      eventDrop: function(event , delta , revertfunc){
        var title = event.title;
        var change_date = event.start.format();
        var end_change_date = event.end.format();
        var id = event.id;
        var cat = event.cat;
        swal({
              title: "Are you sure?",
              text: "Once you move it here , The date of this event will update!",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-primary",
              confirmButtonText: "Update!",
              cancelButtonText: "Cancel!",
              closeOnConfirm: false,
              closeOnCancel: false
            },
            function(isConfirm) {
              if (isConfirm) {
                if(cat == 'main_check' || cat == 'fu_check'){
                  swal.close();
                  $("#resched").modal('show');
                  $("#appID").val(id);
                  $("#doctorID").val(doctor);
                  $("#categ").val(cat);
                }else{
                  updateDroppedEvent(change_date,end_change_date,id);
                }
              } else {
                swal("Aw Snap!", "Event change date cancelled", "error");
              }
            });
      },
      eventClick: function(callEvent , jsEvent, view){
        var cat = callEvent.cat;
        $("#viewEvent").modal();
       if(cat == 'events'){
        $("#event_cont").css("display","block");
        $("#appointment_cont").css("display","none");

        $("#eventName").val(callEvent.event_title);
        $("#eventDate").val(callEvent.event_date);
        $("#event_time").val(callEvent.event_time);
       }else{
        $("#event_cont").css("display","none");
        $("#appointment_cont").css("display","block");

        $("#patient_name").val(callEvent.event_title);
        $("#appointment_time").val(callEvent.appointment_time);
        $("#recheduled").val(callEvent.date_type);
        $("#appointment_date").val(callEvent.appointment_date);
       }
      },
      drop: function(){
        if ($('#drop-remove').is(':checked')) {
          $(this).remove();
        }
      },
      eventReceive: function(event){
       var val = $("#de").val();
       var eventval = $('#external-events .fc-event').each(function() {
                 $(this).data('event', {
		            title: $.trim($(this).text()), // use the element's text as the event title
		            stick: true, // maintain when user navigates (see docs on the renderEvent method)
		            id: $(this).attr('id')
		        });
              });
        var date_dropped = event.start.format();
        
        alert(date_dropped + " - " + id);
      },
      eventResize: function(event , delta , revertfunc){
        var resized_date = event.end.format();
        var event_t = event.title;
        var event_id = event.id;
        var cat = event.cat;
        if(cat == 'main_check' || cat == 'fu_check'){
          swal({
            title: "Aw Snap!",
            text: "Action Not Applied, This is a Patient Appointment",
            type: "error"
          }, function(){
              location.reload();
          }); 
        }else{
          swal({
            title: "Are you sure?",
            text: "Once you resize this , The date range of this event will update!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Update!",
            cancelButtonText: "Cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
          },
          function(isConfirm) {
            if (isConfirm) {
              updateResizedEvent(resized_date,event_id);
            } else {
              swal.close();
            }
          });
        }
      },
            eventConstraint: {
                start: moment().format('YYYY-MM-DD'),
                end: '2100-01-01'
            }
    });
  }
</script>
