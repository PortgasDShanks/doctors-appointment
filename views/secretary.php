<?php 
$sec = mysql_query("SELECT * FROM tbl_users WHERE doctor_id = '$user_id' AND access = 'S'");
$countSec = mysql_num_rows($sec);
$fetch_sec = mysql_fetch_array($sec);
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#" style="font-size: 20px">Home</a></li>
          <li class="breadcrumb-item active" style="font-size: 20px">Secretary</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h5 class="m-0">My Secretary</h5>
              </div>
              <div class="card-body">
                <?php if($countSec != 0){ ?>
                    <div class='row'>
                        <div class='col-md-4'>
                            <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><strong>Firstname:</strong></span>
                            </div>
                            <input type="text" class="form-control" id="s_fname" name="s_fname" value="<?php echo $fetch_sec['user_firstname']?>">
                            </div>
                        </div>
                        <div class='col-md-4'>
                            <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><strong>Middlename:</strong></span>
                            </div>
                            <input type="text" class="form-control" id="s_mname" name="s_mname" value="<?php echo $fetch_sec['user_middlename']?>">
                            </div>
                        </div>
                        <div class='col-md-4'>
                            <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><strong>Lastname:</strong></span>
                            </div>
                            <input type="text" class="form-control" id="s_lname" name="s_lname" value="<?php echo $fetch_sec['user_lastname']?>">
                            </div>
                        </div>
                        <div class='col-md-8'>
                            <div class="input-group" style='margin-top:10px;'>
                            <div class="input-group-prepend">
                                <span class="input-group-text"><strong>Address:</strong></span>
                            </div>
                            <textarea style='resize:none' class='form-control'  id='s_address' value="<?php echo $fetch_sec['user_address']?>"><?php echo $fetch_sec['user_address']?></textarea>
                            </div>
                        </div>
                        <div class='col-md-4'>
                            <div class="input-group" style='margin-top:10px;'>
                            <div class="input-group-prepend">
                                <span class="input-group-text"><strong>Contact #:</strong></span>
                            </div>
                            <input type="number" class="form-control" id="s_connum" name="s_connum" value="<?php echo $fetch_sec['user_conNum']?>">
                            
                            </div>
                        </div>
                        <div class='col-md-12' style='margin-top:20px;'>
                            <button class='btn btn-danger btn-sm pull-right' id='deletebtn' onclick="removeSec('<?php echo $fetch_sec['user_id']?>' , 'delete')"><span class='fa fa-trash'></span> Remove</button>
                        </div>
                    </div>
                <?php } else { ?> 
                    <center><h4 style='color: #8c8b8bbd'>!NO SECRETARY FOUND</h4><br><button class='btn btn-default btn-sm' onclick="addsecModal('<?php echo $user_id?>')"><span class='fa fa-plus-circle'></span> Add</button></center>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php   require 'modals/add_secretary.php';
       
    ?>
<script>
function addsecModal(userID){
    $("#userID").val(userID);
    $("#addSec").modal('show');
}
function deleteSec(secID,type){
    $("#deletebtn").prop("disabled", true);
    $("#deletebtn").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
    $.post("ajax/CRUD_secretary.php", {
        secID: secID,
        type: type
    }, function(data){
        if(data == 1){
            swal({
                title: "All Good!",
                text: "Secretary was successfully removed.",
                type: "success",
                timer: 2000
            }, function(){
                location.reload();
            });
        }else{
            failedAlert();
        }

        //$("#addDoc").modal('hide');
        $("#deletebtn").prop("disabled", false);
        $("#deletebtn").html("<span class='fa fa-trash'></span> Remove");
    });
}
function removeSec(secID,type){
    swal({
    title: "Are you sure?",
    text: "",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Remove",
    cancelButtonText: "Cancel",
    closeOnConfirm: false,
    closeOnCancel: false
    },
    function(isConfirm) {
    if (isConfirm) {
        deleteSec(secID,type);
    } else {
        location.reload();
    }
    });
}
function add_sec(type){
    var s_fname = $("#s_fname").val();
    var s_mname = $("#s_mname").val();
    var s_lname = $("#s_lname").val();
    var s_address = $("#s_address").val();
    var s_num = $("#s_connum").val();
    var userID = $("#userID").val();
    $("#btn_add_sec").prop("disabled", true);
    $("#btn_add_sec").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
    if(s_fname == '' || s_mname == '' || s_lname == '' || s_address == '' || s_num == ''){
        alertWarning();
    }else{
        $.post("ajax/CRUD_secretary.php", {
        type: type,
        s_fname: s_fname,
        s_mname: s_mname,
        s_lname: s_lname,
        s_address: s_address,
        s_num: s_num,
        userID: userID
        }, function(data){
        //alert(data);
        if(data == 1){
            swal({
                title: "All Good!",
                text: "Secretary was successfully inserted.",
                type: "success"
            }, function(){
                location.reload();
            });
        }else{
            failedAlert();
        }
        
        $("#addSec").modal('hide');
        $("#btn_add_sec").prop("disabled", false);
        $("#btn_add_sec").html("<span class='fa fa-check'></span> Save");
        })
    }
}
</script>