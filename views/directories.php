<style>
.badge {
    display: inline-block;
    padding: .25em .4em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    text-align: center;
    vertical-align: baseline;
    border-radius: .25rem;
    text-align: -webkit-left;
    white-space: break-spaces;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#" style="font-size: 20px">Home</a></li>
          <li class="breadcrumb-item active" style="font-size: 20px">Calendar</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<input type='hidden' id='useriD' value='<?php echo $user_id; ?>'>
<div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class='row'>
                  
                  <div class="col-sm-12">
                    <form method='POST' id="searchbar">
                    <div class="input-group">
                      <input type="text" name="search_value" id="search_value" class="form-control" placeholder="Search (E.g Clinic Name , Clinic Specialization)">
                      <div class="input-group-prepend">
                        <button class="btn btn-sm btn-info" id="search_btn" onclick='search_clinic()'><span class="fa fa-search"></span></button>
                      </div>
                    </div>
                     </form>
                  </div>
                 
                  <div class="col-sm-12 row" id="content_response" style='margin-top:20px;'>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<script>
  $(document).ready( function(){

    search_clinic();
    $("#searchbar").on("submit", function(e){
      e.preventDefault();
      var url = "ajax/searched_clinic.php";
      var data = $(this).serialize();
      $.post(url, data, function(data){
        $("#content_response").html(data);
      });
    })
  });
function setappointment(id){
  window.location = "index.php?access=set-appointment&cid="+id;
}
function search_clinic(){
  var search_value = $("#search_value").val();
  
     $.post("ajax/searched_clinic.php", {
      search_value: search_value
    }, function(data){
      $("#content_response").html(data);
    })
    
  
}
</script>
