<?php 
$receiverID = $_GET['id'];
?>
<style>
#card-shadow{
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.34), 0 9px 8px rgba(0, 0, 0, 0.32);
}
.animated {
  animation-duration: 2s;
  animation-fill-mode: both;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#" style="font-size: 20px">Home</a></li>
          <li class="breadcrumb-item active" style="font-size: 20px">Notification</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
              <div class="card">
                <div class="card-body">
                <input type='hidden' value='<?php echo $receiverID?>' id='notifReciever'>
                  <div class='col-md-12 ' style="overflow-y: auto;">
                        <table id='notif_table' class="table table-bordered table-hover">
                            <thead style='background-color: #343940;color: white;'>
                                <tr>
                                    <th></th>
                                    <th>FROM</th>
                                    <th>DATE</th>
                                    <th>NOTIFICATION</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
    <script>
       $(document).ready( function(){
        allNotif();
        });
        function deleteNotif(notifID,receiverID){
            $("#delete_btn"+notifID).prop("disabled", true);
            $("#delete_btn"+notifID).html("<span class='fa fa-spin fa-spinner'></span> Loading...")
            $.post("ajax/deleteNotif.php", {
                notifID: notifID
            }, function(data){
                if(data > 0){
                    swal({
                        title: "All Good!",
                        text: "Notification was successfully deleted.",
                        type: "success"
                    }, function(){
                        allNotif(receiverID);
                    });
                }else{
                    failedAlert();
                }
            });
        }
        function viewNotification(locationLink,notifID){
            $("#view_btn"+notifID).prop("disabled", true);
            $("#view_btn"+notifID).html("<span class='fa fa-spin fa-spinner'></span> Loading...");
            $.post("ajax/updateNotif.php",{
                notifID: notifID
            }, function(data){
                
                if(data > 0){
                    window.location = 'index.php?page='+locationLink;
                }
            });
        }
        function allNotif(){
            var notifReciever = $("#notifReciever").val();
            $("#notif_table").DataTable().destroy();
            $('#notif_table').dataTable({
            "processing":true,
            "ajax":{
                "url":"ajax/datatables/notif_list.php",
                "dataSrc":"data",
                "data":{
                    notifReciever: notifReciever
                },
                "type":"POST"
            },
            "columns":[
                {
                    "data":"count"
                },
                {
                    "data":"sender"
                },
                {
                    "data":"notifDate"
                },
                {
                    "data":"notification"
                },
                {
                    "data":"action"
                }
                
            ]   
            });
        }
    </script>
