<?php 
  $patient_id = $_GET['id'];

  $patient_data = mysql_fetch_array(mysql_query("SELECT * FROM tbl_users WHERE user_id = '$patient_id'"));
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#" style="font-size: 20px">Home</a></li>
          <li class="breadcrumb-item active" style="font-size: 20px">Patient Profile</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
      <input type='hidden' id='userID' name='userID' value='<?php echo $user_id; ?>'>
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Patient Profile</h5>
                <hr>
                <form id="editProfile" method="POST" action="" enctype="multipart/form-data">
                    
                    <div class='row'>
                      <div class='col-md-6'><h4>Profile Picture<h4></div>
                      <div class="col-md-6">
                        <div class="col-md-12" style="">
                            <input type="hidden" name="user_iD" value="<?php echo $user_id; ?>">
                            <?php if(!empty($patient_data['user_image'])){?>
                            <img id="img_wrap" class="previewImage01 image-wrap" src="assets/images/<?php echo $patient_data['user_image'];?>" width="150" height="150" style="object-fit: cover;border-radius: 50%;border:3px solid #b7b1b1">
                            <?php } else { ?>
                            <img id="img_wrap" class="previewImage01 image-wrap" src="assets/images/avatar.png" width="150" height="150" style="object-fit: cover;border-radius: 50%;border:3px solid #b7b1b1">
                            <?php } ?>
                        </div>
                      </div>

                      <div class='col-md-12' style='margin: 10px 0px 10px 0px;border: 1px solid #e5e5e5;'></div>
                      <div class='col-md-6'><h4>Personal Information</h4></div>
                      <div class='col-md-6'>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Firstname:</strong></span>
                          </div>
                          <input type="text" class="form-control" id="fname" name="fname" value='<?php echo $patient_data['user_firstname']?>'>
                        </div>

                        <div class="input-group" style='margin-top: 10px;'>
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Middlename:</strong></span>
                          </div>
                          <input type="text" class="form-control" id="mname" name="mname" value='<?php echo $patient_data['user_middlename']?>'>
                        </div>

                        <div class="input-group" style='margin-top: 10px;'>
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Lastname:</strong></span>
                          </div>
                          <input type="text" class="form-control" id="lname" name="lname" value='<?php echo $patient_data['user_lastname']?>'>
                        </div>

                        <div class="input-group" style='margin-top: 10px;'>
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Age:</strong></span>
                          </div>
                          <input type="number" class="form-control" id="ageP" name="ageP" value='<?php echo $patient_data['age']?>'>
                        </div>

                        <div class="input-group" style='margin-top:10px;'>
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Address:</strong></span>
                          </div>
                          <textarea style='resize:none' class='form-control' row='5' id='address' name='address'><?php echo $patient_data['user_address']?></textarea>
                        </div>

                        <div class="input-group" style='margin-top:10px;'>
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Contact #:</strong></span>
                          </div>
                          <input type="number" class="form-control" id="connum" name="connum" value='<?php echo $patient_data['user_conNum']?>'>
                        </div>
                      </div>
                      <div class='col-md-12' style='margin: 10px 0px 10px 0px;border: 1px solid #e5e5e5;'></div>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<script>
</script>
