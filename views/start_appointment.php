<?php 
$appointmentID = $_GET['appID'];


$patient = SELECT_DATA("*","tbl_users as u , tbl_appointments as a , tbl_doctor_time_table as tt","u.user_id = a.patient_id AND a.appointment_time_id = tt.time_id AND appointment_id = '$appointmentID'");

$status = ($patient['status'] == 0)?"<span class='badge badge-warning' style='font-size: 15px;'>Pending</span>":(($patient['status'] == 1)?"<span class='badge badge-info' style='font-size: 15px;'>Approved</span>":(($patient['status'] == 2)?"<span class='badge badge-success' style='font-size: 15px;'>Finished</span>":(($patient['status'] == 3)?"<span class='badge badge-primary' style='font-size: 15px;'>Rescheduled</span>":"<span class='badge badge-danger' style='font-size: 15px;'>Cancelled</span>")));

$disabled = ($patient['status'] != 3)?"disabled":"";
$doctor = $patient['doctor_id'];
$is_readonly = ($patient['status'] == 3)?"readonly":"";


$textarea = ($patient['status'] == 2 || $patient['status'] == 4)?"readonly":"";
$button = ($patient['status'] == 2 || $patient['status'] == 4)?"display:none":"";
$fuch = ($patient['status'] == 2 || $patient['status'] == 4)?"display:block":"display:none";
?>
<style>
#card-shadow{
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.34), 0 9px 8px rgba(0, 0, 0, 0.32);
}
</style>
<input type="hidden" name="" id="appointmentID" value='<?=$appointmentID?>'>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#" style="font-size: 20px">Home</a></li>
          <li class="breadcrumb-item active" style="font-size: 20px">Appointment Ongoing</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class='row'>
                    <div class='col-md-4'>
                        <div class='card' id='card-shadow'>
                            <div class='card-body'>
                                <h4> Appointment Status: <?=$status?></h4>
                            </div>
                        </div>
                    </div>
                  <div class='col-md-12'>
                    <div class='card' id='card-shadow'>
                      <div class="card-body">
                        <h5 class='card-title'>Patient Information</h5>
                          <div class='row'>
                            <div class='col-md-7' style='margin-top:10px;'>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><strong>Fullname:</strong></span>
                                </div>
                                <input type="text" class="form-control" id="patient_fname" name="patient_fname" value='<?=$patient['lastname'].", ".$patient['firstname']." ".$patient['middlename']?>' readonly>
                              </div>
                            </div>
                            <div class='col-md-5'>
                              <div class="input-group" style='margin-top:10px;'>
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><strong>Contact #:</strong></span>
                                </div>
                                <input type="number" class="form-control" id="patient_num" name="patient_num" value='<?php echo $patient['contact_no']; ?>' readonly>
                              </div>
                            </div>

                            <div class='col-md-12' style='margin-top:10px;'>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><strong>Address:</strong></span>
                                </div>
                                <textarea style='resize:none' class='form-control' row='5' id='patient_address' value='<?php echo $patient['home_address']; ?>' readonly><?php echo $patient['home_address']; ?></textarea>
                              </div>
                            </div>

                          </div>
                      </div>
                    </div>

                    <div class='card' id='card-shadow'>
                      <div class="card-body">
                        <h5 class='card-title'>Appointment Details</h5>
                          <div class='row'>
                            <div class='col-md-4' style='margin-top:10px;'>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><strong>Appointment Date:</strong></span>
                                </div>
                                <input type="text" class="form-control" id="appointement_date" name="patient_fname" value='<?php echo date("F d, Y", strtotime($patient['appointment_date'])); ?>' readonly>
                              </div>
                            </div>

                            <div class='col-md-4' style='margin-top:10px;'>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><strong>Reschedule Date:</strong></span>
                                </div>
                                <input type="text" style='<?php echo ($patient['reschedule_date'] == '0000-00-00')?"color:red":"";?>' class="form-control" id="resched_date" name="resched_date" value='<?php echo ($patient['reschedule_date'] == '0000-00-00')?"N/A":date("F d, Y", strtotime($patient['reschedule_date'])); ?>' readonly>
                              </div>
                            </div>

                            <div class='col-md-4' style='margin-top:10px;'>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><strong>Appointment Time:</strong></span>
                                </div>
                                <input type="text" class="form-control" id="appointment_time" name="appointment_time" value='<?php echo date("H:i A", strtotime($patient['time_from']))." - ".date("H:i A", strtotime($patient['time_to'])); ?>' readonly>
                              </div>
                            </div>
                            <div class='col-md-12' style='margin-top:10px'><h5 class='card-title'>Appointment Result</h5></div>

                            <div class='col-md-6' style='margin-top:10px;'>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><strong>Diagnosis:</strong></span>
                                </div>
                                <textarea <?=$textarea;?> style='resize:none' class='form-control' row='10' id='app_diagnosis' placeholder="Type Diagnosis Here..."><?php echo $patient['diagnosis']?></textarea>
                              </div>
                            </div>

                            <div class='col-md-6' style='margin-top:10px;'>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><strong>Remarks:</strong></span>
                                </div>
                                <textarea <?=$textarea;?> style='resize:none' class='form-control' row='10' id='app_remarks' placeholder="Type Remarks Here..."><?php echo $patient['remarks']?></textarea>
                              </div>
                            </div>

                            <div class='col-md-12' style='margin-top:10px;padding:10px;'>
                              <button id='btnFinish' style='<?=$button?>' class='btn btn-sm btn-primary pull-right' onclick="finish_appointment('<?php echo $appointmentID; ?>','<?php echo $doctor; ?>')"><span class='fa fa-check-circle'></span> Finish Appointment</button>

                              <!-- <button class='btn btn-sm btn-success pull-right' style='margin-right:10px'><span class='fa fa-plus-circle'></span> Set Follow up Check up</button> -->
                            </div>
                          </div>
                      </div>
                    </div>

                    <div class='card' id='card-shadow' style='<?=$fuch?>'>
                      <div class="card-body">
                        <h5 class='card-title'>Follow-up Checkups</h5>
                          <div class='row'>
                          <div class='col-sm-12' style='margin-top:10px;'>
                            <table id='followupcheckup' class="table table-bordered table-hover">
                                    <thead style='background-color: #343940;color: white;'>
                                        <tr>
                                            <th></th>
                                            <th>DOCTOR</th>
                                            <th>APPOINTMENT DATE</th>
                                            <th>RESCHEDULE DATE</th>
                                            <th>APPOINTMENT TIME</th>
                                            <th>STATUS</th>
                                            <th>ACTION</th>
                                            <th>RESULT</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    
                                    </tbody>
                                </table>
                           </div>
                          </div>
                      </div>
                    </div>


                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<?php require 'modals/resched_modal.php'; ?>
<?php require 'modals/add_followup_checkup.php';?>
<?php require 'modals/add_result_fu_modal.php';?>
<?php require 'modals/view_followup_result.php';?>
<script>
$(document).ready(function(){
  follow_up_appointments();
});
function follow_up_appointments(){
    var appointmentID = $("#appointmentID").val();
    $("#followupcheckup").DataTable().destroy();
    $('#followupcheckup').dataTable({
        "processing"    :true,
        responsive      :true,
        scrollCollapse  :true,
        paging          :true,
        searching       :true,
        bSort           :true,
        header          :true,
        "ajax":{
        "url":"ajax/datatables/followup_appointments_doc_view.php",
        "dataSrc":"data",
        "data":{
            appointmentID: appointmentID
        },
        "type": "POST"
        },
        "columns":[
            {
                "data":"counter"
            },
            {
                "data":"doctor"
            },
            {
                "data":"appointment_date"
            },
            {
                "data":"resched_date"
            },
            {
                "data":"appointed_time"
            },
            {
                "data":"status"
            },
            {
                "data":"action"
            },
            {
                "data":"result"
            }
        ]  
    });
  }
  function showResult(fu_id){
    $.post("ajax/view_followup_result.php", {
      fu_id: fu_id
    }, function(data){
      var a = JSON.parse(data);
      $("#view_diagnosis_result").val(a.diagnosis);
      $("#view_remarks_result").val(a.remarks);

      $("#viewfu_result").modal();
    })
  }
  function addResult(fu_id){
    $("#fuID").val(fu_id);
    $("#fu_result").modal();
  }
  function addResultFU(){
    var diagnosis_result = $("#diagnosis_result").val();
    var remarks_result = $("#remarks_result").val();
    var fuID = $("#fuID").val();
    if(diagnosis_result == '' || remarks_result == ''){
      alert("Please fill all boxes provided!");
    }else{
      $.post("ajax/result_of_followup.php", {
        diagnosis_result: diagnosis_result,
        remarks_result: remarks_result,
        fuID: fuID
      }, function(data){
          if(data > 0){
            swal({
                title: "All Good!",
                text: "Follow Up Appointment was successfully Finished.",
                type: "success"
            }, function(){
              follow_up_appointments();
              $("#fu_result").modal("hide");
            });
          }else{
            failed_query();
          }
      })
    }
  }
function finish_appointment(appointmentid,doctor){
  var app_diagnosis = $("#app_diagnosis").val();
  var app_remarks = $("#app_remarks").val();
  $("#btnFinish").prop("disabled", false);
  $("#btnFinish").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
  $.post("ajax/finishAppointment.php",{
    app_diagnosis:app_diagnosis,
    app_remarks:app_remarks,
    appointmentid:appointmentid
    },function(data){
      if(data > 0){
        followup_confirm(doctor,appointmentid);
      }else{
        failedAlert();
      }
    });
}
function followup_confirm(doctor,appointmentid){
    swal({
      title: "Would you like to add follow up check-up?",
      text: "",
      type: "info",
      showCancelButton: true,
      confirmButtonClass: "btn-info",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
      },
      function(isConfirm) {
      if (isConfirm) {
          swal.close();
          $("#appID").val(appointmentid);
          $("#doctorID").val(doctor);
          $("#followup").modal('show');
      } else {
          location.reload();
      }
    });
}
function followup(){
  var followupCheckDate = $("#followupCheck").val();
  var doctorID = $("#doctorID").val();
  $.post("ajax/getTime_followup.php",{
    docID: doctorID,
    appointmentDate: followupCheckDate
  },function(data){
    $("#time_cont").html(data);
  });
}
function addFollowupCheck(){
  var followupCheckDate = $("#followupCheck").val();
  var appID = $("#appID").val();
  var time_id = $("input[id='time']:checked").val();
  $("#btn_addFUC").prop("disabled", true);
  $("#btn_addFUC").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
  $.post("ajax/addFollowUpCheck.php",{
    followupCheckDate: followupCheckDate,
    appID: appID,
    time_id:time_id
  },function(data){
    if(data == 1){
      swal({
          title: "All Good!",
          text: "Follow Up Appointment was successfully added.",
          type: "success"
      }, function(){
          window.location='index.php?access=start-appointment';
      });
    }else{
      failed_alert();
    }
  });
}
function cancelAppointment(transID){
       
       swal({
         title: "Are you sure to cancel this appointment?",
         text: "",
         type: "info",
         showCancelButton: true,
         confirmButtonClass: "btn-primary",
         confirmButtonText: "Continue",
         cancelButtonText: "Cancel",
         closeOnConfirm: false,
         closeOnCancel: false
         },
         function(isConfirm) {
         if (isConfirm) {
           CANCEL_APPOINTMENT(transID);
         } else {
             location.reload();
         }
       });
     }
     function CANCEL_APPOINTMENT(transID){
        var type = 'cancel';
        $("#cancel_btn").prop("disabled", true);
        $("#cancel_btn").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
        $.post("ajax/CRUD_appointmentfollowup.php",{
          transID: transID,
          type: type
        }, function(data){
          if(data > 0){
            swal({
                title: "All Good!",
                text: "Successfully Cancelled Patient Appointment.",
                type: "success"
            }, function(){
              location.reload();
            });
          }else{
            failedAlert();
          }
            
        });
      }
      function reschedAppointment(transID,doctor){
        //alert(cat+'::'+doctor);
        $("#app_ID").val(transID);
        $("#doctor_ID").val(doctor);
        $("#resched").modal('show');
      }
      function resched(){
        var appoint = $("#app_ID").val();
        var doctor = $("#doctor_ID").val();
        var resched_date = $("#resched_date_").val();
        $.post("ajax/getTime_followup.php",{
          docID: doctor,
          appointmentDate: resched_date
        },function(data){
        
          $("#re_time_cont").html(data);
        });
      }
      function appointmentResched(){
        var appoint = $("#app_ID").val();
        var resched_date = $("#resched_date_").val();
        var type = 'resched';
        var cat = $("#categ").val();
        var time = $("input[name='time']:checked").val();
        $("#btn_resched").prop("disabled", true);
        $("#btn_resched").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
        $.post("ajax/CRUD_appointmentfollowup.php",{
          appoint: appoint,
          resched_date: resched_date,
          type: type,
          cat: cat,
          time: time
        }, function(data){
          if(data > 0){
            swal({
                title: "All Good!",
                text: "Successfully Rescheduled Patient Appointment.",
                type: "success"
            }, function(){
              location.reload();
            });
          }else{
            failedAlert();
          }
        });
      }
</script>
