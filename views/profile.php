<?php 
  $clinic_user = SELECT_DATA("*","tbl_users as u, tbl_clinic as c","u.user_id = c.user_id AND u.user_id = '$user_id'");
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#" style="font-size: 20px">Home</a></li>
          <li class="breadcrumb-item active" style="font-size: 20px">Profile</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
      <input type='hidden' id='userID' name='userID' value='<?php echo $user_id; ?>'>
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <?php if($accessType == 'clinic') { ?>
                <form id="editProfile" method="POST" action="" enctype="multipart/form-data">
                    <h5 class="card-title">Clinic Profile</h5>
                    <hr>
                    <div class='row'>
                      <div class='col-md-6'><h4>Clinic Information<h4></div>
                      <div class='col-md-6'>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Clinic Name:</strong></span>
                          </div>
                          <input type="text" class="form-control" id="cname" name="cname" value='<?php echo $clinic_user['clinic_name']?>'>
                        </div>

                        <div class="input-group" style='margin-top:10px;'>
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Clinic Description:</strong></span>
                          </div>
                          <textarea style='resize:none' class='form-control' row='5' id='cdesc' name='cdesc'><?php echo $clinic_user['clinic_desc']?></textarea>
                        </div>

                        <div class="input-group" style='margin-top:10px;'>
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Address:</strong></span>
                          </div>
                          <textarea style='resize:none' class='form-control' row='5' id='caddress' name='caddress'><?php echo $clinic_user['clinic_location']?></textarea>
                        </div>

                        <div class="input-group" style='margin-top: 10px;'>
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Field:</strong></span>
                          </div>
                          <input type="text" class="form-control" id="field" name="field" value='<?php echo $clinic_user['specialization']?>'>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <h5 class="card-title">User Profile</h5>
                    <hr>
                    <div class='row'>
                      <div class='col-md-6'><h4>Profile Picture<h4></div>
                      <div class="col-md-6">
                        <div class="col-md-12" style="">
                            <input type="hidden" name="user_iD" value="<?php echo $user_id; ?>">
                            <?php if(!empty($clinic_user['user_image'])){?>
                            <img id="img_wrap" class="previewImage01 image-wrap" src="assets/images/<?php echo $clinic_user['user_image'];?>" width="150" height="150" style="object-fit: cover;border-radius: 50%;border:3px solid #b7b1b1">
                            <?php } else { ?>
                            <img id="img_wrap" class="previewImage01 image-wrap" src="assets/images/avatar.png" width="150" height="150" style="object-fit: cover;border-radius: 50%;border:3px solid #b7b1b1">
                            <?php } ?>
                            <div class="image-upload" style="margin-top: 5px;margin-left: 30px;">
                            <input type="file" name="avatar" id="files" class="btn-inputfile share" />
                              <label for="files" class="btn default" style="font-size: 16px;"><i class="fa fa-file-image-o"></i> Change</label>
                          </div>
                        </div>
                      </div>

                      <div class='col-md-12' style='margin: 10px 0px 10px 0px;border: 1px solid #e5e5e5;'></div>
                      <div class='col-md-6'><h4>Personal Information</h4></div>
                      <div class='col-md-6'>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Firstname:</strong></span>
                          </div>
                          <input type="text" class="form-control" id="fname" name="fname" value='<?php echo $clinic_user['firstname']?>'>
                        </div>

                        <div class="input-group" style='margin-top: 10px;'>
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Middlename:</strong></span>
                          </div>
                          <input type="text" class="form-control" id="mname" name="mname" value='<?php echo $clinic_user['middlename']?>'>
                        </div>

                        <div class="input-group" style='margin-top: 10px;'>
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Lastname:</strong></span>
                          </div>
                          <input type="text" class="form-control" id="lname" name="lname" value='<?php echo $clinic_user['lastname']?>'>
                        </div>

                        <div class="input-group" style='margin-top:10px;'>
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Address:</strong></span>
                          </div>
                          <textarea style='resize:none' class='form-control' row='5' id='address' name='address'><?php echo $clinic_user['home_address']?></textarea>
                        </div>

                        <div class="input-group" style='margin-top:10px;'>
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Contact #:</strong></span>
                          </div>
                          <input type="number" class="form-control" id="connum" name="connum" value='<?php echo $clinic_user['contact_no']?>'>
                        </div>
                      </div>
                      <div class='col-md-12' style='margin: 10px 0px 10px 0px;border: 1px solid #e5e5e5;'></div>
                      <div class='col-md-6'><h4>Account Credentials</h4></div>
                      <div class='col-md-6'>
                        <label class="checkbox-inline" style='margin-top: 10px;'><input type="checkbox" name="credentials_check" id="credentials_check" onchange="showCred()"> Change Credentials? </label>
                        
                        <div id='cred_cont' style='display: none'>
                          <div class="input-group" style='margin-top: 10px;'>
                            <div class="input-group-prepend">
                              <span class="input-group-text"><strong>Old Password:</strong></span>
                            </div>
                            <input type="password" onkeyup='oldPassCheck()' class="form-control" id="old_pass" name="old_pass">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><strong><span id='icon'></span></strong></span>
                            </div>
                          </div>

                          <div class="input-group" style='margin-top: 10px;'>
                            <div class="input-group-prepend">
                              <span class="input-group-text"><strong>New Password:</strong></span>
                            </div>
                            <input type="password" readonly class="form-control" id="new_pass" name="new_pass">
                          </div>

                          <div class="input-group" style='margin-top: 10px;'>
                            <div class="input-group-prepend">
                              <span class="input-group-text"><strong>Confirm Password:</strong></span>
                            </div>
                            <input type="password" readonly onkeyup='confirmPassword()' class="form-control" id="conf_pass" name="conf_pass">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><strong><span id='iconConf'></span></strong></span>
                            </div>
                          </div>

                        </div> 
                      </div>
                      <div class='col-md-12' style='margin: 10px 0px 10px 0px;border: 1px solid #e5e5e5;'></div>
                    </div>

                  <button type="submit" id="btn-edit" class="btn btn-primary pull-right btn-sm"><span class="fa fa-check"></span> Save Changes</button>
                </form>
                <?php } ?>
                <?php if($accessType == 'patient') {  ?>
                  <form id="editProfile_patient" method="POST" action="" enctype="multipart/form-data">
                    <hr>
                    <h5 class="card-title">User Profile</h5>
                    <hr>
                    <div class='row'>
                      <div class='col-md-6'><h4>Profile Picture<h4></div>
                      <div class="col-md-6">
                        <div class="col-md-12" style="">
                            <input type="hidden" name="user_iD" value="<?php echo $user_id; ?>">
                            <?php if(!empty($users['user_image'])){?>
                            <img id="img_wrap" class="previewImage01 image-wrap" src="assets/images/<?php echo $users['user_image'];?>" width="150" height="150" style="object-fit: cover;border-radius: 50%;border:3px solid #b7b1b1">
                            <?php } else { ?>
                            <img id="img_wrap" class="previewImage01 image-wrap" src="assets/images/avatar.png" width="150" height="150" style="object-fit: cover;border-radius: 50%;border:3px solid #b7b1b1">
                            <?php } ?>
                            <div class="image-upload" style="margin-top: 5px;margin-left: 30px;">
                            <input type="file" name="avatar" id="files" class="btn-inputfile share" />
                              <label for="files" class="btn default" style="font-size: 16px;"><i class="fa fa-file-image-o"></i> Change</label>
                          </div>
                        </div>
                      </div>

                      <div class='col-md-12' style='margin: 10px 0px 10px 0px;border: 1px solid #e5e5e5;'></div>
                      <div class='col-md-6'><h4>Personal Information</h4></div>
                      <div class='col-md-6'>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Firstname:</strong></span>
                          </div>
                          <input type="text" class="form-control" id="fname" name="fname" value='<?php echo $users['firstname']?>'>
                        </div>

                        <div class="input-group" style='margin-top: 10px;'>
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Middlename:</strong></span>
                          </div>
                          <input type="text" class="form-control" id="mname" name="mname" value='<?php echo $users['middlename']?>'>
                        </div>

                        <div class="input-group" style='margin-top: 10px;'>
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Lastname:</strong></span>
                          </div>
                          <input type="text" class="form-control" id="lname" name="lname" value='<?php echo $users['lastname']?>'>
                        </div>

                        <div class="input-group" style='margin-top:10px;'>
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Address:</strong></span>
                          </div>
                          <textarea style='resize:none' class='form-control' row='5' id='address' name='address'><?php echo $users['home_address']?></textarea>
                        </div>

                        <div class="input-group" style='margin-top:10px;'>
                          <div class="input-group-prepend">
                            <span class="input-group-text"><strong>Contact #:</strong></span>
                          </div>
                          <input type="number" class="form-control" id="connum" name="connum" value='<?php echo $users['contact_no']?>'>
                        </div>
                      </div>
                      <div class='col-md-12' style='margin: 10px 0px 10px 0px;border: 1px solid #e5e5e5;'></div>
                      <div class='col-md-6'><h4>Account Credentials</h4></div>
                      <div class='col-md-6'>
                        <label class="checkbox-inline" style='margin-top: 10px;'><input type="checkbox" name="credentials_check" id="credentials_check" onchange="showCred()"> Change Credentials? </label>
                        
                        <div id='cred_cont' style='display: none'>
                          <div class="input-group" style='margin-top: 10px;'>
                            <div class="input-group-prepend">
                              <span class="input-group-text"><strong>Old Password:</strong></span>
                            </div>
                            <input type="password" onkeyup='oldPassCheck()' class="form-control" id="old_pass" name="old_pass">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><strong><span id='icon'></span></strong></span>
                            </div>
                          </div>

                          <div class="input-group" style='margin-top: 10px;'>
                            <div class="input-group-prepend">
                              <span class="input-group-text"><strong>New Password:</strong></span>
                            </div>
                            <input type="password" readonly class="form-control" id="new_pass" name="new_pass">
                          </div>

                          <div class="input-group" style='margin-top: 10px;'>
                            <div class="input-group-prepend">
                              <span class="input-group-text"><strong>Confirm Password:</strong></span>
                            </div>
                            <input type="password" readonly onkeyup='confirmPassword()' class="form-control" id="conf_pass" name="conf_pass">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><strong><span id='iconConf'></span></strong></span>
                            </div>
                          </div>

                        </div> 
                      </div>
                      <div class='col-md-12' style='margin: 10px 0px 10px 0px;border: 1px solid #e5e5e5;'></div>
                    </div>

                  <button type="submit" id="btn-edit" class="btn btn-primary pull-right btn-sm"><span class="fa fa-check"></span> Save Changes</button>
                </form>

                <?php } ?>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<script>
$(document).ready( function(){
  $("#editProfile").on('submit',(function(e) {
    e.preventDefault();
        $.ajax({
         url:"ajax/update_profile.php",
         type: "POST",
         data:  new FormData(this),
         beforeSend: function(){},
         contentType: false,
         cache: false,
         processData:false,
         success: function(data)
            {
              updateAlert();
          
            },
             error: function() 
         {
        failedAlert();
         }           
       });
        
    }));

    $("#editProfile_patient").on('submit',(function(e) {
    e.preventDefault();
        $.ajax({
         url:"ajax/update_profile_patient.php",
         type: "POST",
         data:  new FormData(this),
         beforeSend: function(){},
         contentType: false,
         cache: false,
         processData:false,
         success: function(data)
            {
              updateAlert();
          
            },
             error: function() 
         {
        failedAlert();
         }           
       });
        
    }));
});
$(".btn-inputfile").change(function () {
      $("#btn-edit").prop("disabled", false);
      var input = document.getElementById('files');
      previewFile(input);
  });

  function previewFile(input) {
    var file = input.files[0];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $('.previewImage01').attr('src', reader.result);

    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }

  
  function confirmPassword(){
    var newPass = $("#new_pass").val();
    var conf = $("#conf_pass").val();
    if(conf != newPass){
      $("#iconConf").html("<span class='fa fa-close' style='color:red'></span> Did Not Match");
      $("#btn-edit").attr('disabled', true);
    }else{
      $("#iconConf").html("<span class='fa fa-check-circle' style='color:green'></span> Matched");
      $("#btn-edit").attr('disabled', false);
    }
  }
  function oldPassCheck(){
    var old = $("#old_pass").val();
    var userID = $("#userID").val();
    $.post("ajax/oldPassChecker.php", {
      old: old,
      userID: userID
    }, function(data){
      if(data > 0){
        $("#icon").html("<span class='fa fa-check-circle' style='color:green'></span>");
        $("#new_pass").attr("readonly", false);
        $("#conf_pass").attr("readonly", false);
      }else{
        $("#icon").html("<span class='fa fa-close' style='color:red'></span>");
        $("#new_pass").attr("readonly", true);
        $("#conf_pass").attr("readonly", true);
      }
    });
  }
  function showCred(){
    if($("#credentials_check").prop('checked')){
      $("#cred_cont").slideToggle();
      $("#btn-edit").attr('disabled', true);
    }else{
      $("#cred_cont").slideToggle();
      $("#old_pass").val("");
      $("#new_pass").val("");
      $("#conf_pass").val("");
      $("#new_pass").attr("readonly", true);
      $("#conf_pass").attr("readonly", true);
      $("#btn-edit").attr('disabled', false);
      $("#icon").html("");
      $("#iconConf").html("");
    }
  }
</script>
