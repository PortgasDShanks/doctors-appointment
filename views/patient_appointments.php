<?php 

?>
<style>
#appointment_table_wrapper{
  margin-top:15px;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#" style="font-size: 20px">Home</a></li>
          <li class="breadcrumb-item active" style="font-size: 20px">Patient Appointments</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class='col-md-12' style='overflow:auto'>
                    <!-- <div class="input-group" style='width:40%'>
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Date:</strong></span>
                      </div>
                      <input type="text" class="form-control" onchange='datechange()' value='<?php echo date("Y-m-d", strtotime(getCurrentDate()))?>' id="datepicker" name="datepicker_today" >
                    </div> -->
                    <table id='appointment_table' class="table table-bordered table-hover" style='margin-top:10px;width:100%'>
                        <thead style='background-color: #343940;color: white;'>
                            <tr>
                                <th style='display:none'></th>
                                <th></th>
                                <th>PATIENT</th>
                                <th>APPOINTMENT DATE</th>
                                <th>RESCHEDULE DATE</th>
                                <th>APPOINTMENT TIME</th>
                                <th>DESCRIPTION</th>
                                <th>STATUS</th>
                                <th style='width:130px'>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<?php require 'modals/resched_modal.php'; ?>
<?php require 'modals/show_appointment_result.php'; ?>
<script>
    $(document).ready(function(){
      patient_appointment();
    });
    function toggle_followup(mainID){
      if($("#spanDiv"+mainID).hasClass('fa fa-eye')){
        $("#spanDiv"+mainID).removeClass('fa fa-eye');
        $("#spanDiv"+mainID).addClass('fa fa-eye-slash');
        $("#btnShow"+mainID).removeClass('btn-success');
        $("#btnShow"+mainID).addClass('btn-danger');
      }else{
        $("#spanDiv"+mainID).removeClass('fa fa-eye-slash');
        $("#spanDiv"+mainID).addClass('fa fa-eye');
        $("#btnShow"+mainID).removeClass('btn-danger');
        $("#btnShow"+mainID).addClass('btn-success');
      }
      $(".content_fu"+mainID).fadeToggle('fast');
    } 
      function reschedAppointment(transID,doctor){
        $("#app_ID").val(transID);
        $("#doctor_ID").val(doctor);
        $("#resched").modal('show');
      }
      function resched(){
        var appoint = $("#app_ID").val();
        var doctor = $("#doctor_ID").val();
        var resched_date = $("#resched_date_").val();
        $.post("ajax/getTime_followup.php",{
          docID: doctor,
          appointmentDate: resched_date
        },function(data){
          $("#re_time_cont").html(data);
        });
      }
      function appointmentResched(){
        var appoint = $("#app_ID").val();
        var resched_date = $("#resched_date_").val();
        var type = 'resched';
        var cat = $("#categ").val();
        var time = $("input[name='time']:checked").val();
        $("#btn_resched").prop("disabled", true);
        $("#btn_resched").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
        $.post("ajax/CRUD_appointment.php",{
          appoint: appoint,
          resched_date: resched_date,
          type: type,
          cat: cat,
          time: time
        }, function(data){
          if(data > 0){
            swal({
                title: "All Good!",
                text: "Successfully Rescheduled Patient Appointment.",
                type: "success"
            }, function(){
              location.reload();
            });
          }else{
            failedAlert();
          }
        });
      }
      function approveAppointment(transID,cat){
        swal({
          title: "Are you sure to approve this appointment?",
          text: "",
          type: "info",
          showCancelButton: true,
          confirmButtonClass: "btn-primary",
          confirmButtonText: "Continue",
          cancelButtonText: "Cancel",
          closeOnConfirm: false,
          closeOnCancel: false
          },
          function(isConfirm) {
          if (isConfirm) {
            APPROVE_APPOINTMENT(transID,cat);
          } else {
              location.reload();
          }
        });
      }
      function startAppointment(appID){
        //alert(appID);
        $("#start_btn"+appID).prop("disabled", true);
        $("#start_btn"+appID).html("<span class='fa fa-spin fa-spinner'></span> Please Wait");
        $.post("ajax/queuing.php", {
          appID: appID
        }, function(data){
          //alert(data)
          if(data == 1 || data == 2){
            window.location = 'index.php?access=start-appointment&appID='+appID;
          }else{
            failedAlert();
          }
        });
      }
      function showResults(transID,cat){
        var userID = $("#userID").val();
        $("#result").modal('show');
        $.post("ajax/getResult.php",{
          transID: transID,
          cat: cat,
          userID: userID
        }, function(data){
          $("#cont").html(data);
        });
      }
      function APPROVE_APPOINTMENT(transID){
        var type = 'approve';
        $("#approve_btn").prop("disabled", true);
        $("#approve_btn").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
        $.post("ajax/CRUD_appointment.php", {
          transID: transID,
          type: type
        }, function(data){
         // alert(data)
          if(data > 0){
            swal({
                title: "All Good!",
                text: "Successfully Apporove Patient Appointment.",
                type: "success"
            }, function(){
              location.reload();
            });
          }else{
            failedAlert();
          }
        })
      }

      function cancelAppointment(transID){
       
        swal({
          title: "Are you sure to cancel this appointment?",
          text: "",
          type: "info",
          showCancelButton: true,
          confirmButtonClass: "btn-primary",
          confirmButtonText: "Continue",
          cancelButtonText: "Cancel",
          closeOnConfirm: false,
          closeOnCancel: false
          },
          function(isConfirm) {
          if (isConfirm) {
            CANCEL_APPOINTMENT(transID);
          } else {
              location.reload();
          }
        });
      }
      function CANCEL_APPOINTMENT(transID){
        var type = 'cancel';
        $("#cancel_btn").prop("disabled", true);
        $("#cancel_btn").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
        $.post("ajax/CRUD_appointment.php",{
          transID: transID,
          type: type
        }, function(data){
          if(data > 0){
            swal({
                title: "All Good!",
                text: "Successfully Cancelled Patient Appointment.",
                type: "success"
            }, function(){
              location.reload();
            });
          }else{
            failedAlert();
          }
            
        });
      }
    function patient_appointment(){
        $("#appointment_table").DataTable().destroy();
        $('#appointment_table').dataTable({
        "processing":true,
        responsive:     true,
        scrollCollapse: true,
        paging:         true,
        searching:      true,
        bSort:          true,
        header: true,
        "ajax":{
        "url":"ajax/datatables/patient_appointments.php",
        "dataSrc":"data"
        },
        "columns":[
            {
                "data":"counter"
            },
            {
                "data":"patient"
            },
            {
                "data":"app_date"
            },
            {
                "data":"reschedule_date"
            },
            {
                "data":"app_time"
            },
            {
                "data":"description"
            },
            {
                "data":"status"
            },
            {
                "data":"action"
            }
        ] 
        });
    }
</script>
