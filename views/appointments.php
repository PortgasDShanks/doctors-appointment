<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#" style="font-size: 20px">Home</a></li>
          <li class="breadcrumb-item active" style="font-size: 20px">Appointments</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
      <div class="container-fluid">
      <input type='hidden' id='userID' value="<?php echo $user_id; ?>">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class='row'>
                  <div class='col-md-12' >
                    <!-- <button class='btn btn-default pull-right' id="" onclick="window.location='index.php?page=set-appointment'" style="font-size: 18px;"><span class='fa fa-hand-o-right'></span> Set Appointment</button> -->
                  </div>
                  <div class='col-md-12' style='margin-top: 20px;overflow: auto;'>
                    <table id='appointment_table' class="table table-bordered table-hover">
                        <thead style='background-color: #343940;color: white;'>
                            <tr>
                                <th></th>
                                <th>DOCTOR</th>
                                <th>APPOINTMENT DATE</th>
                                <th>RESCHEDULE DATE</th>
                                <th>APPOINTMENT TIME</th>
                                <th>DESCRIPTION</th>
                                <th>STATUS</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                         
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php require 'modals/show_appointment_result.php'; ?>  
<script>
$(document).ready( function(){
  appointments();
});
 function gotoPatientData(id){
      window.location = 'index.php?page=patient-data&id='+id;
    }
function showResults(transID,cat){
  var userID = $("#userID").val();
  $("#result").modal('show');
  $.post("ajax/getResult.php",{
    transID: transID,
    cat: cat,
    userID: userID
  }, function(data){
    $("#cont").html(data);
  });
}
function view_appointment_details(id){
  window.location = "index.php?access=appointment-details&id="+id;
}
function toggle_followup(mainID){
      if($("#spanDiv"+mainID).hasClass('fa fa-eye')){
        $("#spanDiv"+mainID).removeClass('fa fa-eye');
        $("#spanDiv"+mainID).addClass('fa fa-eye-slash');
        $("#btnShow"+mainID).removeClass('btn-success');
        $("#btnShow"+mainID).addClass('btn-danger');
      }else{
        $("#spanDiv"+mainID).removeClass('fa fa-eye-slash');
        $("#spanDiv"+mainID).addClass('fa fa-eye');
        $("#btnShow"+mainID).removeClass('btn-danger');
        $("#btnShow"+mainID).addClass('btn-success');
      }
      $(".content_fu"+mainID).fadeToggle('fast');
    } 
  function appointments(){
    var userID = $("#userID").val();
    $("#appointment_table").DataTable().destroy();
    $('#appointment_table').dataTable({
        "processing"    :true,
        responsive      :true,
        scrollCollapse  :true,
        paging          :true,
        searching       :true,
        bSort           :true,
        header          :true,
        "ajax":{
        "url":"ajax/datatables/appointments.php",
        "dataSrc":"data",
        "data":{
            userID: userID
        },
        "type": "POST"
        },
        "columns":[
            {
                "data":"counter"
            },
            {
                "data":"doctor"
            },
            {
                "data":"appointment_date"
            },
            {
                "data":"resched_date"
            },
            {
                "data":"appointed_time"
            },
            {
                "data":"description"
            },
            {
                "data":"status"
            },
            {
                "data":"action"
            }
        ]  
    });
  }
</script>
