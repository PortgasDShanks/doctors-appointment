<?php 
switch($accessType){
  case 'S':
    $getDoc_sec = mysql_fetch_array(mysql_query("SELECT * FROM tbl_users WHERE user_id = '$user_id'"));
    $sessionID = $getDoc_sec['doctor_id'];
  break;
  case 'D' || 'P':
    $sessionID = $user_id;
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Announcements</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <input type='hidden' value='<?php echo $user_id?>' id='userID'>
            <div class="card card-inverse card-outline">
              <div class="card-body">
                <div class='col-md-6'>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><strong>Date:</strong></span>
                      </div>
                      <input type="text" class="form-control" onchange='datechange()' value='<?php echo date("Y-m-d", strtotime(getCurrentDate()))?>' id="datepicker" name="datepicker">
                    </div>
                </div>
                <div class='col-md-12' style="margin-top: 2%;" id='announcement_cont'>
                    
                </div>
              
              </div>
            </div><!-- /.card -->

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <?php   
        require 'modals/add_announcement.php';
    ?>
<script>
$(document).ready(function(){
    datechange();
});
function datechange(){
    var datepick = $("#datepicker").val();
    var userID = $("#userID").val();
    $.post("ajax/getAnnouncement.php", {
        datepick: datepick,
        userID: userID
    }, function(data){
        $("#announcement_cont").html(data);
    });
}
function addAnnouncement(){
    $("#addannouncement").modal('show');
}
function removeAnnounce(announcementID){
    var type = 'delete';
   $.post("ajax/CRUD_announcement.php", {
    announcementID: announcementID,
    type: type
   }, function(data){
    if(data == 1){
        deleteAlert();
    }else{
        failedAlert();
    }
   });
}
function add_announcement(type){
    var desc = $("#announcement").val();
    var date_a = $("#a_date").val();
    var docID = $("#docID").val();

    if(desc == ''){
        alertWarning();
    }else{
        $.post("ajax/CRUD_announcement.php", {
            desc: desc,
            date_a: date_a,
            docID: docID,
            type: type
        }, function(data){
            if(data == 1){
                insertAlert();
            }else{
                failedAlert();
            }
        });
    }
}
</script>