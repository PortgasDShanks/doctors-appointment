<style>
  .box {
  position: relative;
  max-width: 600px;
  width: 90%;
  height: 300px;
  background: #fff;
  box-shadow: 0 0 15px rgba(0,0,0,.1);
}
/* common */
.ribbon {
  width: 120px;
  height: 120px;
  overflow: hidden;
  position: absolute;
}
.ribbon::before,
.ribbon::after {
  position: absolute;
  z-index: -1;
  content: '';
  display: block;
  border: 5px solid #2980b9;
}
.ribbon span {
  position: absolute;
  display: block;
  width: 210px;
  padding: 5px 0;
  /* background-color: #3498db; */
  box-shadow: 0 5px 10px rgba(0,0,0,.1);
  color: #fff;
  font: 700 15px/1 'Lato', sans-serif;
  text-shadow: 0 1px 1px rgba(0,0,0,.2);
  text-transform: uppercase;
  text-align: center;
}

/* top right*/
.ribbon-top-right {
  top: -2x;
  right: -2px;
}
.ribbon-top-right::before,
.ribbon-top-right::after {
  border-top-color: transparent;
  border-right-color: transparent;
}
.ribbon-top-right::before {
  top: 0;
  left: 0;
}
.ribbon-top-right::after {
  bottom: 0;
  right: 0;
}
.ribbon-top-right span {
  left: -30px;
  top: 30px;
  transform: rotate(45deg);
}
#card-shadow{
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.34), 0 9px 8px rgba(0, 0, 0, 0.32);
}





</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#" style="font-size: 20px">Home</a></li>
          <li class="breadcrumb-item active" style="font-size: 20px">Patient Records</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
      <input type='hidden' id='userID' value="<?php echo $user_id; ?>">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            
          </div>
        </div>
      </div>
    </div>
  <?php require 'modals/show_appointment_result.php'; ?>
<script>
function showResult(appointmentID){
  var userID = $("#userID").val();
  $("#result").modal('show');
  $.post("ajax/getResult.php", {
    userID: userID,
    appointmentID: appointmentID
  }, function(data){
    $("#cont").html(data);
  });
}
</script>
