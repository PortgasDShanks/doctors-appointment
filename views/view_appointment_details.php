<?php 
$appointmentID = $_GET['id'];

$patient = SELECT_DATA("*","tbl_users as u , tbl_appointments as a , tbl_doctor_time_table as tt","u.user_id = a.patient_id AND a.appointment_time_id = tt.time_id AND appointment_id = '$appointmentID'");
$status = ($patient['status'] == 0)?"<span class='badge badge-warning' style='font-size: 15px;'>Pending</span>":(($patient['status'] == 1)?"<span class='badge badge-info' style='font-size: 15px;'>Approved</span>":(($patient['status'] == 2)?"<span class='badge badge-success' style='font-size: 15px;'>Finished</span>":(($patient['status'] == 3)?"<span class='badge badge-primary' style='font-size: 15px;'>Rescheduled</span>":"<span class='badge badge-danger' style='font-size: 15px;'>Cancelled</span>")));

$disabled = ($patient['status'] != 3)?"disabled":"";
$doctor = $patient['doctor_id'];
$is_readonly = ($patient['status'] == 3)?"readonly":"";
?>
<style>
#card-shadow{
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.34), 0 9px 8px rgba(0, 0, 0, 0.32);
}
</style>
<input type="hidden" name="" id="appointmentID" value='<?=$appointmentID?>'>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#" onclick="window.location='index.php?access=appointment'" style="font-size: 20px">Appointment</a></li>
          <li class="breadcrumb-item active" style="font-size: 20px">Appointment Details</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class='row'>
                    <div class='col-md-4'>
                        <div class='card' id='card-shadow'>
                            <div class='card-body'>
                                <h4> Appointment Status: <?=$status?></h4>
                            </div>
                        </div>
                    </div>
                  <div class='col-md-12'>
                    <div class='card' id='card-shadow'>
                      <div class="card-body">
                        <h5 class='card-title'>Patient Information</h5>
                          <div class='row'>
                            <div class='col-md-7' style='margin-top:10px;'>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><strong>Patient Name:</strong></span>
                                </div>
                                <input type="text" class="form-control" id="patient_fname" name="patient_fname" value='<?=$patient['lastname'].", ".$patient['firstname']." ".$patient['middlename']?>' readonly>
                              </div>
                            </div>
                            <div class='col-md-5'>
                              <div class="input-group" style='margin-top:10px;'>
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><strong>Contact #:</strong></span>
                                </div>
                                <input type="number" class="form-control" id="patient_num" name="patient_num" value='<?=$patient['contact_no']; ?>' readonly>
                              </div>
                            </div>

                            <div class='col-md-12' style='margin-top:10px;'>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><strong>Address:</strong></span>
                                </div>
                                <textarea style='resize:none' class='form-control' row='5' id='patient_address' value='<?=$patient['home_address']; ?>' readonly><?php echo $patient['home_address']; ?></textarea>
                              </div>
                            </div>

                          </div>
                      </div>
                    </div>

                    <div class='card' id='card-shadow'>
                      <div class="card-body">
                        <h5 class='card-title'>Appointment Details</h5>
                          <div class='row'>
                            <div class='col-md-4' style='margin-top:10px;'>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><strong>Appointment Date:</strong></span>
                                </div>
                                <input type="text" class="form-control" id="appointement_date" name="patient_fname" value='<?php echo date("F d, Y", strtotime($patient['appointment_date'])); ?>' readonly>
                              </div>
                            </div>

                            <div class='col-md-4' style='margin-top:10px;'>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><strong>Reschedule Date:</strong></span>
                                </div>
                                <input type="text" style='<?php echo ($patient['reschedule_date'] == '0000-00-00')?"color:red":"";?>' class="form-control" id="resched_date" name="resched_date" value='<?php echo ($patient['reschedule_date'] == '0000-00-00')?"N/A":date("F d, Y", strtotime($patient['reschedule_date'])); ?>' readonly>
                              </div>
                            </div>

                            <div class='col-md-4' style='margin-top:10px;'>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><strong>Appointment Time:</strong></span>
                                </div>
                                <input type="text" class="form-control" id="appointment_time" name="appointment_time" value='<?php echo date("H:i A", strtotime($patient['time_from']))." - ".date("H:i A", strtotime($patient['time_to'])); ?>' readonly>
                              </div>
                            </div>
                            <div class='col-md-12' style='margin-top:10px'><h5 class='card-title'>Appointment Result</h5></div>

                            <div class='col-md-6' style='margin-top:10px;'>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><strong>Diagnosis:</strong></span>
                                </div>
                                <textarea readonly style='resize:none' class='form-control' row='10' id='app_diagnosis'><?php echo $patient['diagnosis']?></textarea>
                              </div>
                            </div>

                            <div class='col-md-6' style='margin-top:10px;'>
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><strong>Remarks:</strong></span>
                                </div>
                                <textarea readonly style='resize:none' class='form-control' row='10' id='app_remarks'><?php echo $patient['remarks']?></textarea>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>

                    <div class='card' id='card-shadow'>
                      <div class="card-body">
                        <h5 class='card-title'>Follow-up Checkups</h5>
                          <div class='row'>
                           <div class='col-sm-12' style='margin-top:10px;'>
                            <table id='followup' class="table table-bordered table-hover">
                                    <thead style='background-color: #343940;color: white;'>
                                        <tr>
                                            <th></th>
                                            <th>DOCTOR</th>
                                            <th>APPOINTMENT DATE</th>
                                            <th>RESCHEDULE DATE</th>
                                            <th>APPOINTMENT TIME</th>
                                            <th>STATUS</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    
                                    </tbody>
                                </table>
                           </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <?php require 'modals/resched_modal.php'; ?>
<?php require 'modals/add_followup_checkup.php';?>
<script>
$(document).ready(function(){
    follow_up_appointments();
});
function cancelAppointment(transID){
       
       swal({
         title: "Are you sure to cancel this appointment?",
         text: "",
         type: "info",
         showCancelButton: true,
         confirmButtonClass: "btn-primary",
         confirmButtonText: "Continue",
         cancelButtonText: "Cancel",
         closeOnConfirm: false,
         closeOnCancel: false
         },
         function(isConfirm) {
         if (isConfirm) {
           CANCEL_APPOINTMENT(transID);
         } else {
             location.reload();
         }
       });
     }
     function CANCEL_APPOINTMENT(transID){
        var type = 'cancel';
        $("#cancel_btn").prop("disabled", true);
        $("#cancel_btn").html("<span class='fa fa-spin fa-spinner'></span> Loading...");
        $.post("ajax/CRUD_appointmentfollowup.php",{
          transID: transID,
          type: type
        }, function(data){
          if(data > 0){
            swal({
                title: "All Good!",
                text: "Successfully Cancelled Patient Appointment.",
                type: "success"
            }, function(){
              location.reload();
            });
          }else{
            failedAlert();
          }
            
        });
      }
      function reschedAppointment(transID,doctor){
        //alert(cat+'::'+doctor);
        $("#app_ID").val(transID);
        $("#doctor_ID").val(doctor);
        $("#resched").modal('show');
      }
      function resched(){
        var appoint = $("#app_ID").val();
        var doctor = $("#doctor_ID").val();
        var resched_date = $("#resched_date_").val();
        $.post("ajax/getTime_followup.php",{
          docID: doctor,
          appointmentDate: resched_date
        },function(data){
        
          $("#re_time_cont").html(data);
        });
      }
      function appointmentResched(){
        var appoint = $("#app_ID").val();
        var resched_date = $("#resched_date_").val();
        var type = 'resched';
        var cat = $("#categ").val();
        var time = $("input[name='time']:checked").val();
        $("#btn_resched").prop("disabled", true);
        $("#btn_resched").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
        $.post("ajax/CRUD_appointmentfollowup.php",{
          appoint: appoint,
          resched_date: resched_date,
          type: type,
          cat: cat,
          time: time
        }, function(data){
          if(data > 0){
            swal({
                title: "All Good!",
                text: "Successfully Rescheduled Patient Appointment.",
                type: "success"
            }, function(){
              location.reload();
            });
          }else{
            failedAlert();
          }
        });
      }
function follow_up_appointments(){
    var appointmentID = $("#appointmentID").val();
    $("#followup").DataTable().destroy();
    $('#followup').dataTable({
        "processing"    :true,
        responsive      :true,
        scrollCollapse  :true,
        paging          :true,
        searching       :true,
        bSort           :true,
        header          :true,
        "ajax":{
        "url":"ajax/datatables/followup_appointments.php",
        "dataSrc":"data",
        "data":{
            appointmentID: appointmentID
        },
        "type": "POST"
        },
        "columns":[
            {
                "data":"counter"
            },
            {
                "data":"doctor"
            },
            {
                "data":"appointment_date"
            },
            {
                "data":"resched_date"
            },
            {
                "data":"appointed_time"
            },
            {
                "data":"status"
            },
            {
                "data":"action"
            }
        ]  
    });
  }
</script>
