<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>404 Error Page</h1>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<div class="content">
   <div class="error-page">
        <h2 class="headline text-warning"> 404</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-warning"></i> Oops! Page not found.</h3>

          <p>
            We could not find the page you were looking for.
            Meanwhile, you may <a href="index.php?page=dashboard">return to dashboard</a>.
          </p>
        </div>
      </div>
</div>
<script type="text/javascript">
</script>