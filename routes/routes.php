<?php
switch($access){
	case 'dashboard':
		require 'views/dashboard.php';
	break;
	case 'clinics';
		require 'views/doctors.php';
	break;
	case 'time-schedules';
		require 'views/doctor_time_table_list.php';
	break;
	case 'calendar':
		require 'views/calendar.php';
	break;
	case 'clinic-directory':
		require 'views/directories.php';
	break;
	case 'appointment':
		require 'views/appointments.php';
	break;
	case 'set-appointment':
		require 'views/set_appointment.php';
	break;
	case 'appointment-details':
		require 'views/view_appointment_details.php';
	break;
	case 'patient-appointments':
		require 'views/patient_appointments.php';
	break;
	case 'start-appointment':
		require 'views/start_appointment.php';
	break;
	case 'profile':
		require 'views/profile.php';
	break;
	case 'see-all-msg':
		require 'views/messages.php';
	break;
} 
// // }
// if($access == 'dashboard'){
// 	require 'views/dashboard.php';
// }else if($access == 'categories'){
// 	require 'views/categories.php';
// }else if($access == 'doctors'){
// 	require 'views/doctors.php';
// }else if($access == 'appointment'){
// 	require 'views/appointments.php';
// }else if($access == 'patient-records'){
// 	require 'views/patient_records.php';
// }else if($access == 'profile'){
// 	require 'views/profile.php';
// }else if($access == 'secretary'){
// 	require 'views/secretary.php';
// }else if($access == 'time-table'){
// 	require 'views/doctor_time_table_list.php';
// }else if($access == 'set-appointment'){
// 	require 'views/set_appointment.php';
// }else if($access == 'announcements'){
// 	require 'views/doctor_announcement.php';
// }else if($access == 'patient-appointments'){
// 	require 'views/patient_appointments.php';
// }else if($access == 'calendar'){
// 	require 'views/calendar.php';
// }else if($access == 'start-appointment'){
// 	require 'views/start_appointment.php';
// }else if($access == 'blood-lists'){
// 	require 'views/blood_lists.php';
// }else if($access == 'see-all'){
// 	require 'views/see_all_notifications.php';
// }else if($access == 'see-all-msg'){
// 	require 'views/messages.php';
// }else if($access == 'reports'){
// 	require 'views/reports.php';
// }else if($access == 'patient-data'){
// 	require 'views/patient_data.php';
// }else{	
// 	require 'views/404.php';
// }


?>