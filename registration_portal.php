<?php
include 'core/config.php'; 
(isset($_SESSION['user_id']))?header("Location: views/index.php"):"";
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Doctor's Appointment Management System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="An Online Appointment System for patients">
    <meta name="msapplication-tap-highlight" content="no">
    <link href="assets/login_assets/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="assets/login_assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/login_assets/js/bootstrap.min.js"></script>
    <!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script> -->
    
    <style>
        .register{
            background: -webkit-linear-gradient(left, #3931af, #00c6ff);
            padding: 3%;
            border-radius: 50px 0px 50px 0px;
        }
        .register-left{
            text-align: center;
            color: #fff;
            margin-top: 4%;
        }
        .register-left input{
            border: none;
            border-radius: 1.5rem;
            padding: 2%;
            width: 60%;
            background: #f8f9fa;
            font-weight: bold;
            color: #383d41;
            margin-top: 30%;
            margin-bottom: 3%;
            cursor: pointer;
        }
        .register-right{
            background: #f8f9fa;
            border-top-left-radius: 10% 50%;
            border-bottom-left-radius: 10% 50%;
        }
        .register-left img{
            margin-top: 15%;
            margin-bottom: 5%;
            width: 25%;
            -webkit-animation: mover 2s infinite  alternate;
            animation: mover 1s infinite  alternate;
        }
        @-webkit-keyframes mover {
            0% { transform: translateY(0); }
            100% { transform: translateY(-20px); }
        }
        @keyframes mover {
            0% { transform: translateY(0); }
            100% { transform: translateY(-20px); }
        }
        .register-left p{
            font-weight: lighter;
            padding: 12%;
            margin-top: -9%;
        }
        .register .register-form{
            padding: 10%;
            margin-top: 10%;
        }
        .btnRegister{
            float: right;
            margin-top: 10%;
            border: none;
            border-radius: 1.5rem;
            padding: 2%;
            background: #0062cc;
            color: #fff;
            font-weight: 600;
            width: 50%;
            cursor: pointer;
        }
        .register .nav-tabs{
            margin-top: 3%;
            border: none;
            background: #0062cc;
            border-radius: 1.5rem;
            width: 28%;
            float: right;
        }
        .register .nav-tabs .nav-link{
            padding: 2%;
            height: 34px;
            font-weight: 600;
            color: #fff;
            border-top-right-radius: 1.5rem;
            border-bottom-right-radius: 1.5rem;
        }
        .register .nav-tabs .nav-link:hover{
            border: none;
        }
        .register .nav-tabs .nav-link.active{
            width: 100px;
            color: #0062cc;
            border: 2px solid #0062cc;
            border-top-left-radius: 1.5rem;
            border-bottom-left-radius: 1.5rem;
        }
        .register-heading{
            text-align: center;
            margin-top: 8%;
            margin-bottom: -15%;
            color: #495057;
        }
                
    </style>
<body>
<div class="app-container app-theme-white body-tabs-shadow" style='background: -webkit-linear-gradient(left, #505354, #00c6ff);'>
        <div class="app-container">
        <div class="container register">
                <div class="row">
                    <div class="col-md-3 register-left">
                        <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
                        <h3>Welcome</h3>
                        <p>Good Choice!</p>
                        <button class='btn btn-sm btn-primary' onclick='window.location="login.php"'> Back to Login </button><br/>
                    </div>
                    <div class="col-md-9 register-right">
                        <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Patient</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Clinic</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <h3 class="register-heading">Register as Patient</h3>
                                <div class="row register-form">
                                    <div class="col-md-12">
                                    <form method='POST' id='register_patient'>
                                        <label> &mdash; Please Fill your information below &mdash; </label>
                                        <div class="form-group">
                                            <input type="text" autocomplete='off' placeholder="Firstname *" class='form-control' id='firstname' name='firstname' required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" autocomplete='off' placeholder="Lastname *" class='form-control' id='lastname' name='lastname' required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" autocomplete='off' placeholder="Contact No. *" class='form-control' id='contact' name='contact' required>
                                        </div>
                                        <div class="form-group">
                                            <textarea class='form-control' autocomplete='off' rows='3' placeholder='Home Address *' style='resize:none' name='home_address' required></textarea>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" autocomplete='off' placeholder="Email Address *" class='form-control' id='email_add' name='email_add' required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" autocomplete='off' placeholder="Username *" class='form-control' id='username' name='username' required>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" minlength='8' class='form-control' placeholder="Password *" id='password' name='password' required>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" minlength='8' class='form-control' placeholder="Confirm Password *" name='conf_password' id='conf_password' required onkeyup='passwordChecker()'>
                                        </div>
                                        <span id='password_response'></span>
                                        <div class="form-group">
                                            <button type='submit' id='register_btn' disabled class='btn btn-primary pull-right' style='float:right'>Register</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <h3  class="register-heading">Register as Clinic</h3>
                                <div class="row register-form">
                                    <div class="col-md-12">
                                    <form method='POST' id='register_clinic'>
                                        <label> &mdash; Clinic Information &mdash; </label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name='clinic_name' placeholder="Clinic Name *" />
                                        </div>
                                        <div class="form-group">
                                            <textarea class='form-control' autocomplete='off' rows='3' placeholder='Clinic Description *' style='resize:none' name='clinic_desc' required></textarea>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name='specialization' placeholder="Specialization *" />
                                        </div>
                                        <div class="form-group">
                                            <textarea class='form-control' autocomplete='off' rows='3' placeholder='Clinic Address *' style='resize:none' name='clinic_address' required></textarea>
                                        </div>
                                        <label> &mdash; Doctor Information &mdash; </label>
                                        <div class="form-group">
                                            <input type="text" autocomplete='off' placeholder="Firstname *" class='form-control' id='doctor_firstname' name='firstname' required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" autocomplete='off' placeholder="Lastname *" class='form-control' id='doctor_lastname' name='lastname' required>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" autocomplete='off' placeholder="Email Address *" class='form-control' id='doctor_email_add' name='email_add' required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" autocomplete='off' placeholder="Username *" class='form-control' id='clinic_username' name='username' required>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" minlength='8' class='form-control' placeholder="Password *" id='clinic_password' name='password' required>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" minlength='8' class='form-control' placeholder="Confirm Password *" name='conf_password' id='clinic_conf_password' required onkeyup='passwordChecker_clinic()'>
                                        </div>
                                        <span id='clnc_password_response'></span>
                                        <div class="form-group">
                                            <button type='submit' id='register_btn_clnc' disabled class='btn btn-primary pull-right' style='float:right'>Register</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
<script>
    $(document).ready( function(){
        $("#register_clinic").on('submit', function(e){
            e.preventDefault();
            $("#register_btn").prop("disabled", true);
            $("#register_btn").html("Saving Data...");
            var url = 'ajax/clinic_registration.php';
            var data = $(this).serialize();
            $.post(url, data, function(data){
                if(data > 0){
                    alert("Registration Successful");
                    window.location = 'index.php';
                }else{
                    alert("Failed");
                    //window.location.reload();
                }
            });
            
        });

        $("#register_patient").on('submit', function(e){
                e.preventDefault();
                $("#register_btn").prop("disabled", true);
			    $("#register_btn").html("Saving Data...");
                var url = 'ajax/patient_registration.php';
                var data = $(this).serialize();
                $.post(url, data, function(data){
                    if(data > 0){
                        alert("Registration Successful");
                        window.location = 'index.php';
                    }else{
                        alert("Failed");
                        window.location.reload();
                    }
                });
                
            })
    });
    function passwordChecker(){
        var password = $("#password").val();
        var conf_password = $("#conf_password").val();

        if(password == conf_password){
            $("#password_response").html("Password Matched").css('color','green');
            $("#register_btn").attr("disabled", false);
        }else{
            $("#password_response").html("Password Doesn't Match").css('color','red');
            $("#register_btn").attr("disabled", true);
        }
    }
    function passwordChecker_clinic(){
        var password = $("#clinic_password").val();
        var conf_password = $("#clinic_conf_password").val();

        if(password == conf_password){
            $("#clnc_password_response").html("Password Matched").css('color','green');
            $("#register_btn_clnc").attr("disabled", false);
        }else{
            $("#clnc_password_response").html("Password Doesn't Match").css('color','red');
            $("#register_btn_clnc").attr("disabled", true);
        }
    }
</script>
</body>
</html>
