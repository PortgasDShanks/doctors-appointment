<?php 
include 'core/config.php';
if(isset($_SESSION['user_id'])){
  header("Location: index.php");
}
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="assets/images/icon.png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="assets/login_assets/fonts/icomoon/style.css">

    <link rel="stylesheet" href="assets/login_assets/css/owl.carousel.min.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/login_assets/css/bootstrap.min.css">
    
    <!-- Style -->
    <link rel="stylesheet" href="assets/login_assets/css/style.css">

    <title>DAMS</title>
  </head>
  <body>
  

  <div class="d-lg-flex half">
    
    <div class="contents order-1 order-md-2">

      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-7">
            <h3>Login to <strong> DOCTOR'S APPOINTMENT MANAGEMENT SYSTEM</strong></h3>
            <p class="mb-4">Don't have an account ? <a href="registration_portal.php"> Register Here...</a> </p>
            <form id='signin-account' method="post">
              <div class="form-group first">
                <label for="username">Username</label>
                <input type="text" autocomplete='off' autofocus class="form-control" placeholder="Username" id="username" name='username'>
              </div>
              <div class="form-group last mb-3">
                <label for="password">Password</label>
                <input type="password" class="form-control" placeholder="Password" id="password" name="password">
              </div>
              
              <div class="d-flex mb-5 align-items-center">
                <span class="ml-auto"><a href="#" class="forgot-pass">Forgot Password</a></span> 
              </div>

              <input type="submit" value="Log In" id='login_btn' class="btn btn-block btn-primary">

            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="bg order-2 order-md-1" style="background-image: url('assets/images/clinicbg.jpg');"></div>
    
  </div>
    
    

    <script src="assets/login_assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/login_assets/js/popper.min.js"></script>
    <script src="assets/login_assets/js/bootstrap.min.js"></script>
    <script src="assets/login_assets/js/main.js"></script>
    <script>
      $(document).ready( function(){
        $("#signin-account").on('submit', function(e){
                e.preventDefault();
                $("#login_btn").prop("disabled", true);
			    $("#login_btn").html("Authenticating...");
                var url = 'ajax/auth.php';
                var data = $(this).serialize();
                $.post(url, data, function(data){
                    if(data > 0){
                        window.location = 'index.php?access=dashboard';
                    }else{
                        alert("Failed");
                        window.location.reload();
                    }
                });
                
            })
    });
    </script>
  </body>
</html>
