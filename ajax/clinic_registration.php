<?php
include '../core/config.php';

$clinic_name = $_POST['clinic_name'];
$clinic_desc = $_POST['clinic_desc'];
$location = $_POST['clinic_address'];
$specialization = $_POST['specialization'];

$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$email_add = $_POST['email_add'];
$username = $_POST['username'];
$password = $_POST['password'];


$md5Password = md5($password);


$doctor_data = array(
        "firstname" => $firstname,
        "lastname" => $lastname,
        "username" => $username,
        "email_address" => $email_add,
        "password" => $md5Password,
        "user_access" => "clinic" 
);

$doctor_reg = PMS_INSERT_DATA("tbl_users", $doctor_data , "Y");
if($doctor_reg > 0){
        $clinic_info = array(
                "user_id" => $doctor_reg,
                "clinic_name" => $clinic_name,
                "clinic_desc" => $clinic_desc,
                "specialization" => $specialization,
                "clinic_location" => $location,
                "date_added" => getCurrentDate(),
                "clinic_status" => 0
        );

        $clinic_reg = PMS_INSERT_DATA("tbl_clinic", $clinic_info , "N");

}

echo $clinic_reg;