<?php
include '../../core/config.php';

$appointmentID = $_POST['appointmentID'];

$query_cat = SELECT_LOOP_DATA("*","tbl_followup_appointments","appointment_main_id = '$appointmentID'");
$count = 1;
$response['data'] = array();
if(is_array($query_cat)){
    foreach ($query_cat as $data) {
        $list = array();
        $cancel_btn = ($data['status'] == 4 || $data['status'] == 2)?"display:none":"";
        $resched_btn = ($data['status'] == 4 || $data['status'] == 2 || $data['status'] == 3)?"display:none":"";
        $apprv_btn = ($data['status'] == 0 )?"":"display:none";
        $list['counter'] = $count++;
        $list['fu_appointment_id'] = $data['fu_appointment_id'];
        $list['doctor'] = getUser($data['doctor_id']);
        $list['appointment_date'] = date("F d, Y", strtotime($data['appointment_date']));
        $list['resched_date'] = ($data['reschedule_date'] != '0000-00-00')?date("F d, Y", strtotime($data['reschedule_date'])):"<span style='color:red'>N/A</span>";
        $list['appointed_time'] = getTime($data['appointment_time_id']);
        $list['status'] = ($data['status'] == 0)?"<span class='badge badge-warning' style='font-size: 15px;'>Pending</span>":(($data['status'] == 1)?"<span class='badge badge-info' style='font-size: 15px;'>Approved</span>":(($data['status'] == 2)?"<span class='badge badge-success' style='font-size: 15px;'>Finished</span>":(($data['status'] == 3)?"<span class='badge badge-primary' style='font-size: 15px;'>Rescheduled</span>":"<span class='badge badge-danger' style='font-size: 15px;'>Cancelled</span>")));

        $list['action'] = "<button style='".$apprv_btn."' class='btn btn-sm btn-info' data-toggle='tooltip' data-placement='bottom' title='Approve Appointment' onclick='approveAppointment(".$data['fu_appointment_id'].")'><span class='fa fa-thumbs-up'></span></button><button data-toggle='tooltip' data-placement='bottom' title='Cancel Appointment' class='btn btn-sm btn-danger' style='".$cancel_btn."'  onclick='cancelAppointment(".$data['fu_appointment_id'].")'><span class='fa fa-ban'></span></button><button data-toggle='tooltip' data-placement='bottom' title='Reshedule Appointment' class='btn btn-sm btn-info' style='".$resched_btn."' onclick='reschedAppointment(".$data['fu_appointment_id'].",".$data['doctor_id'].")'><span class='fa fa-calendar'></span></button>";


        $list['result'] = ($data['status'] == 1 || $data['status'] == 3)?"<button class='btn btn-sm btn-info' onclick='addResult(".$data['fu_appointment_id'].")'><span class='fa fa-plus-circle'></span> Add Result</button>":(($data['status'] == 2)?"<button class='btn btn-sm btn-success' onclick='showResult(".$data['fu_appointment_id'].")'><span class='fa fa-eye'></span> Show Result</button>":"<span class='badge badge-danger' style='font-size: 15px;'>No Result</span>");

        array_push($response['data'],$list);
    }
    
}
echo json_encode($response);

	