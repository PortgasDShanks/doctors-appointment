<?php
include '../../core/config.php';

$query_cat = SELECT_LOOP_DATA("*","tbl_clinic as c, tbl_users as u","c.user_id = u.user_id AND clinic_status = 2 ORDER BY clinic_id DESC");
$count = 1;
$response['data'] = array();

if(is_array($query_cat)){
    foreach($query_cat as $data){

        $list = array();
        
    
        $list['clinic_id'] = $data["clinic_id"];
        $list['count'] = $count++;
        $list['name'] = $data['clinic_name'];
        $list['desc'] = $data['firstname']." ".$data['lastname'];
        $list['email'] = $data['email_address'];

        array_push($response['data'],$list);
    }
}
        echo json_encode($response);
