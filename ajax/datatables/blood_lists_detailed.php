<?php
include '../../core/config.php';
$name = $_POST['donor_name'];
$curdate = date("Y-m-d h:i", strtotime(getCurrentDate()));
$query_cat = mysql_query("SELECT * FROM tbl_blood_donors WHERE blood_donor_name = '$name' ORDER BY date_added DESC")or die(mysql_error());
$count = 1;
$response['data'] = array();

while($fetch_cat = mysql_fetch_array($query_cat)){

    $list = array();
    $bags_remain = $fetch_cat['quantity_bags'] - $fetch_cat['quantity_used'];
    $addedDate = date("Y-m-d h:i", strtotime($fetch_cat['date_added']));
    $expireDate = date("Y-m-d h:i", strtotime("+42 days",strtotime($addedDate)));
    $is_disabled = ($expireDate <= $curdate || $bags_remain < 1)?"disabled":""; 

    $status = ($expireDate <= $curdate)?"e":(($bags_remain < 1)?"u":"");
    

	$list['bloodID'] = $fetch_cat['blood_donor_id'];
	$list['count'] = $count++;
    $list['date_added'] = date("M d, Y h:i A", strtotime($fetch_cat['date_added']));
    $list['bags'] = $bags_remain;
    $list['is_expired_used'] = $status;

    $list['inputQ'] = "<input type='number' ".$is_disabled." class='form-control' onkeyup='quantityChecker(".$bags_remain.",".$fetch_cat['blood_donor_id'].")' max='".$bags_remain."' id='quantity".$fetch_cat['blood_donor_id']."'>";

    $list['buttonQ'] = "<button ".$is_disabled." class='btn btn-sm btn-success' id='blood".$fetch_cat['blood_donor_id']."' onclick='useBlood(\"".$name."\",\"".$fetch_cat['blood_donor_id']."\")'><span class='fa fa-minus-circle'></span></button>";

	array_push($response['data'],$list);
}
	echo json_encode($response);