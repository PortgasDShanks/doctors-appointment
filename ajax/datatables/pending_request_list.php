<?php
include '../../core/config.php';

$query_cat = SELECT_LOOP_DATA("*","tbl_clinic as c, tbl_users as u","c.user_id = u.user_id AND clinic_status = 0 ORDER BY clinic_id DESC");
$count = 1;
$response['data'] = array();

if(is_array($query_cat)){
    foreach($query_cat as $data){

        $list = array();
        
    
        $list['clinic_id'] = $data["clinic_id"];
        $list['count'] = $count++;
        $list['name'] = $data['clinic_name'];
        $list['desc'] = $data['firstname']." ".$data['lastname'];
        $list['email'] = $data['email_address'];
        $list['action'] = "<i onclick='request_response(\"".'approve'."\",\"".$data["clinic_id"]."\")' class='fa fa-thumbs-up' style='font-size: 25px;color:green;cursor:pointer' data-toggle='tooltip' title='Approve Request' data-placement= 'bottom'></i><i onclick='request_response(\"".'approve'."\",\"".$data["clinic_id"]."\")' class='fa fa-ban' style='font-size: 25px;padding-left: 10px;color:red;cursor:pointer' data-toggle='tooltip' title='Cancel Request' data-placement= 'bottom'></i>";


        // <i onclick='request_response(\"".'cancel'."\",\"".$data["clinic_id"]."\")' class='metismenu-icon pe-7s-close-circle icon-gradient bg-mean-fruit' style='font-size:30px;cursor:pointer;padding-left: 10px;' data-toggle='tooltip' title='Cancel Request' data-placement= 'bottom'></i>

        array_push($response['data'],$list);
    }
}
        echo json_encode($response);
