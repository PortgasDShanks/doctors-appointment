<?php
include '../../core/config.php';

$userid = $_POST['userID'];

$query_cat = SELECT_LOOP_DATA("*","tbl_appointments","patient_id = '$userid'");
$count = 1;
$response['data'] = array();
if(is_array($query_cat)){
    foreach ($query_cat as $data) {
        $list = array();
        $cancel_btn = ($data['status'] == 4 || $data['status'] == 2)?"display:none":"";
        $resched_btn = ($data['status'] == 4 || $data['status'] == 2 || $data['status'] == 3)?"display:none":"";
        $list['counter'] = $count++;
        $list['appointment_id'] = $data['appointment_id'];
        $list['doctor'] = getUser($data['doctor_id']);
        $list['appointment_date'] = date("F d, Y", strtotime($data['appointment_date']));
        $list['resched_date'] = ($data['reschedule_date'] != '0000-00-00')?date("F d, Y", strtotime($data['reschedule_date'])):"<span style='color:red'>N/A</span>";
        $list['appointed_time'] = getTime($data['appointment_time_id']);
        $list['description'] = $data['description'];
        $list['status'] = ($data['status'] == 0)?"<span class='badge badge-warning' style='font-size: 15px;'>Pending</span>":(($data['status'] == 1)?"<span class='badge badge-info' style='font-size: 15px;'>Approved</span>":(($data['status'] == 2)?"<span class='badge badge-success' style='font-size: 15px;'>Finished</span>":(($data['status'] == 3)?"<span class='badge badge-primary' style='font-size: 15px;'>Rescheduled</span>":"<span class='badge badge-danger' style='font-size: 15px;'>Cancelled</span>")));

        $list['action'] = "<button class='btn btn-sm btn-success' data-toggle='tooltip' data-placement='bottom' title='View Details' onclick='view_appointment_details(".$data['appointment_id'].")'><span class='fa fa-eye'></span></button><button data-toggle='tooltip' data-placement='bottom' title='Cancel Appointment' class='btn btn-sm btn-danger' style='".$cancel_btn."'><span class='fa fa-ban'></span></button><button data-toggle='tooltip' data-placement='bottom' title='Reshedule Appointment' class='btn btn-sm btn-info' style='".$resched_btn."'><span class='fa fa-calendar'></span></button>";

        array_push($response['data'],$list);
    }
   
}
echo json_encode($response);

	