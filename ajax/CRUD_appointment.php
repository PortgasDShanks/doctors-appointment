<?php 
include '../core/config.php';
use SMSGatewayMe\Client\ApiClient;
use SMSGatewayMe\Client\Configuration;
use SMSGatewayMe\Client\Api\MessageApi;
use SMSGatewayMe\Client\Model\SendMessageRequest;
$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTYyMDk1MzI0MiwiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjg4NTkyLCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.XXMUBRns0_BEKbiROgTYx3-CzfNM-RhX74zWw17dhOA";
$device_id = 124810;

$action = $_POST['type'];
$notif_date = getCurrentDate();


    switch($action){
        case "cancel":
            $transID = $_POST['transID'];

            $transDetails = SELECT_DATA("*","tbl_appointments","appointment_id = '$transID'");

         

            $form_data = array("status" => 4);
            $update = PMS_UPDATE_QUERY("tbl_appointments",$form_data,"WHERE appointment_id = '$transID'");

            $form_data2 = array("notification_date" => $notif_date , "notification_type" => 0 , "notification_is_for" => 0 , "user_id" => $transDetails['doctor_id'] , "status" => 0 , "notification_receiver" => $transDetails['patient_id'] , "module" => "CA" , "action" => "Your appointment was cancelled");

            $crud = PMS_INSERT_DATA("tbl_notification",$form_data2,"N");
        break;
        case "approve":
            $transID = $_POST['transID'];
          
            $transDetails = SELECT_DATA("*","tbl_appointments","appointment_id = '$transID'");
            $form_data = array("status" => 1);
            $update = PMS_UPDATE_QUERY("tbl_appointments",$form_data,"WHERE appointment_id = '$transID'");

            $form_data2 = array("notification_date" => $notif_date , "notification_type" => 0 , "notification_is_for" => 0 , "user_id" => $transDetails['doctor_id'] , "status" => 0 , "notification_receiver" => $transDetails['patient_id'] , "module" => "AA" , "action" => "Your appointment was approved");

            $crud = PMS_INSERT_DATA("tbl_notification",$form_data2,"N");


        break;
        case "resched":
            $transID = $_POST['appoint'];
            $reschedDate = date("Y-m-d", strtotime($_POST['resched_date']));
            $time = $_POST['time'];

            // $query = ($cat == 'main_check')?"SELECT * FROM tbl_appointments WHERE appointment_id = '$transID'":"SELECT * FROM tbl_followup_appointments WHERE fu_appointment_id = '$transID'";

            $transDetails = SELECT_DATA("*","tbl_appointments","appointment_id = '$transID'");
          
            $form_data = array("status" => 3 , "reschedule_date" => $reschedDate, "appointment_time_id" => $time);
            $update = PMS_UPDATE_QUERY("tbl_appointments",$form_data,"appointment_id = '$transID'");

            $form_data2 = array("notification_date" => $notif_date , "notification_type" => 0 , "notification_is_for" => 0 , "user_id" => $transDetails['doctor_id'] , "status" => 0 , "notification_receiver" => $transDetails['patient_id'] , "module" => "CA" , "action" => "Your appointment was rescheduled");

            $crud = PMS_INSERT_DATA("tbl_notification",$form_data2,"N");
           
        break;
    }
    if($crud > 0){
        $patient_id = $transDetails['patient_id'];
        $doctor = $transDetails['doctor_id'];
        $getPatientContact = SELECT_DATA("contact_no","tbl_users","user_id = '$patient_id'");
        $getClinicContact = SELECT_DATA("*","tbl_clinic","user_id = '$doctor'");

        if($action == 'approve'){
            $message = "Hello, This is from ".$getClinicContact['clinic_name'].", I would like to inform you that your appointment was approved. Please come on time on your appointed date. Thank you";
        }else if($action == 'cancel'){
            $message = "Hello, This is from ".$getClinicContact['clinic_name'].", I would like to inform you that your appointment was cancelled. We're very sorry. Thank you";
        }else{
            $message = "Hello, This is from ".$getClinicContact['clinic_name'].", I would like to inform you that your appointment was rescheduled. Your new appointment date is ".date("F d, Y", strtotime($reschedDate)).". Thank you";
        }
        
        $text = array(
            "phone_number" => $getPatientContact['contact_no'],
            "message" => $message,
            "device_id" => $device_id
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://smsgateway.me/api/v4/message/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 50,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "[  " . json_encode($text) ."]",
            CURLOPT_HTTPHEADER => array(
                "authorization: $token",
                "cache-control: no-cache"
            ),
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if($err){
            echo 0;
        }else{
            echo 1;
        }
    }