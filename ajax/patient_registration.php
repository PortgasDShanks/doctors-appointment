<?php
include '../core/config.php';

$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$email_address = $_POST['email_add'];
$home_address = $_POST['home_address'];
$contact = $_POST['contact'];
$username = $_POST['username'];
$password = $_POST['password'];

$md5Password = md5($password);

$patient_data = array(
        "firstname" => $firstname,
        "lastname" => $lastname,
        "home_address" => $home_address,
        "contact_no" => $contact,
        "email_address" => $email_address,
        "username" => $username,
        "password" => $md5Password,
        "user_access" => "patient" 
);

$patient_reg = PMS_INSERT_DATA("tbl_users", $patient_data , "N");

echo $patient_reg;