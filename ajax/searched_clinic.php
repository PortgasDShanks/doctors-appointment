<?php 
include '../core/config.php';

$search = $_POST['search_value'];

if($search == ""){
	$sql = SELECT_LOOP_DATA("*","tbl_clinic as c , tbl_users as u","c.user_id = u.user_id AND c.clinic_status = 1");
}else{
	$sql = SELECT_LOOP_DATA("*","tbl_clinic as c , tbl_users as u","c.user_id = u.user_id AND c.clinic_status = 1 AND (c.clinic_name LIKE '%$search%' OR c.specialization LIKE '%$search%') ");
}

if(is_array($sql)){
	foreach($sql as $data){
		 $avatar = (!empty($data['user_image'])) ? "assets/images/".$data['user_image']."" : "assets/images/avatar.png";

		 echo '<div class="col-md-4">';
		 	echo '<div class="card card-widget widget-user-2">';
		 		echo '<div class="widget-user-header bg-info">';
                    echo '<div class="widget-user-image">';
                    	echo '<img class="img-circle elevation-2" src="'.$avatar.'" alt="User Avatar">';
                    echo '</div>';
                    echo '<h3 class="widget-user-username">'.strtoupper($data['firstname']).' '.strtoupper($data['lastname']).'</h3>';
                    echo '<h5 class="widget-user-desc">'.strtoupper($data['specialization']).'</h5>';
                echo '</div>';
                echo '<div class="card-footer p-0">';
                	echo '<ul class="nav flex-column">';
                		echo '<li class="nav-item">';
                        	echo '<a href="#" class="nav-link">';
                        		echo '<span class="badge bg-info" style="font-size: 16px;font-weight: 400;"">'.$data['clinic_name'].'</span>';
                        	echo '</a>';
                        echo '</li>';
                        echo '<li class="nav-item">';
                        	echo '<a href="#" class="nav-link">';
                        		echo '<div class="badge bg-info" style="font-size: 16px;text-indent: 20px;font-weight: 400;"><p>&mdash; '.$data['clinic_desc'].'</p></div>';
                        	echo '</a>';
                        echo '</li>';
                        echo '<li class="nav-item">';
                        	echo '<a href="#" class="nav-link">';
                        		echo '<span class="badge bg-info" style="font-size: 16px;font-weight: 400">'.$data['clinic_location'].'</span>';
                        	echo '</a>';
                        echo '</li>';
                        echo '<li class="nav-item">';
                        echo '<a href="#" class="nav-link">';
                        echo '<center><span class="badge bg-info" ><button class="btn btn-sm btn-info" onclick="setappointment('.$data['clinic_id'].')"><span class="fa fa-hand-o-right"></span> Set an Appointment</button></span><center>';
                        echo '</a>';
                        echo '</li>';
                	echo '</ul>';
                echo '</div>';
		 	echo "</div>";
		 echo "</div>";

	
	} 
}else{ 
	echo "<div class='col-md-12'>";
      echo "<h2 style='text-align: center;color: red;'>No Clinic Found</h2>";
    echo "</div>";
} 
